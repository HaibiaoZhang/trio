set PATH=%CD%\..\toolchain\make381;%CD%\..\toolchain\RISCVGCC810-win32\bin
set TOOLS_DIR=%CD%\..\toolchain\RISCVGCC810-win32
set NAME=DEMOAPP
set VER=302001
set CODE=1
set POSTYPE=TYPEA50

make -s %1 all

mkimage sign rsa.private 1 0xA7260102 ./output/%NAME%_%VER%_%CODE%.bin ../bin/%NAME%_%VER%_%CODE%.sign.bin

pause

#ifndef _SEC_API_H_
#define _SEC_API_H_

#ifndef	u8
#define u8		unsigned char
#endif
#ifndef	u16
#define u16		unsigned short
#endif
#ifndef	u32
#define u32		unsigned int
#endif


#define SEC_ENC 1
#define SEC_DEC 0

#define SEC_MAX_MKSK 200//所有对称密钥统一索引，通过密钥属性来区分
#define SEC_MAX_DUKPT 10
#define SEC_MAX_RSA 10


#define KCV_MODE_NOCHK   0
#define KCV_MODE_CHK0    1
#define KCV_MODE_CHKFIX  2
#define KCV_MODE_CHKMAC  3

/**
 * 标识 "key usage" 的数据类型. 
 *
 * 这里对 key usage 的命名描述 借鉴 TR-31.
 * 但是, 出于实现考虑, 对具体的 key usage 的 value 的定义, 和 TR-31 不同. 
 *
 * 若某个应用程序尝试使用特定密钥执行其 "key usage" 以外的功能, 譬如尝试使用用来加密 PIN 的 key 执行 MAC 操作,
 */
#define SEC_KU_BDK                        ( (unsigned short) (('B'<<8)|'0') )// 对应 TR-31 中的 "B0". 
                                            // 参见 " ANS X9 TR-31 (2010)", "A.5.1 Key Usage", "Table A-3. Defined Key Usage Values".
#define SEC_KU_DUKPT_INIT_KEY             ( (unsigned short) (('B'<<8)|'1') ) // "B1". DUKPT init key.
#define SEC_KU_CVK                        ( (unsigned short) (('C'<<8)|'0') )// "C0". CVK : Card Verification Key.
#define SEC_KU_DATA_ENCRYPTION            ( (unsigned short) (('D'<<8)|'0') )// "D0". This key can be used to encipher or decipher data, 
#if 0
																	//but it cannot be used in any more specificcryptographic
																	//operations such as PIN block encryption or key encryption.
#define SEC_KU_EMV_IMK_APP_CRYPTOGRAMS    ( (unsigned short) (('E'<<8)|'0') )// "E0". IMK : Issuer Master Key.
#define SEC_KU_EMV_IMK_SECURE_MSG_FOR_CONFIDENTIALITY \
                                          ( (unsigned short) (('E'<<8)|'1') )// "E1". IMK : Issuer Master Key.
#define SEC_KU_EMV_IMK_SECURE_MSG_FOR_INTEFRITY \
        	                             ( (unsigned short) (('E'<<8)|'2') )// "E2"
#define SEC_KU_EMV_IMK_DATA_AUTHENTICATION_CODE \
       		                             ( (unsigned short) (('E'<<8)|'3') )// "E3"
#define SEC_KU_EMV_IMK_DYNAMIC_NUM        ( (unsigned short) (('E'<<8)|'4') )// "E4"
#define SEC_KU_EMV_IMK_CARD_PERSONALIZATION \
                                    		( (unsigned short) (('E'<<8)|'5') )// "E5"
#define SEC_KU_EMV_IMK_OTHER              ( (unsigned short) (('E'<<8)|'6') )// "E6"
#define SEC_KU_IV                         ( (unsigned short) (('I'<<8)|'7') )// "I0". IV : Initializatio Vector.
#endif 

#define SEC_KU_KEY_ENC_OR_WRAP            ( (unsigned short) (('K'<<8)|'0') )// "K0". 包含 MK.
#define SEC_KU_KBPK                       ( (unsigned short) (('K'<<8)|'1') )// "K1". KBPK : TR-31 Key Block Protection Key.
#define SEC_KU_ISO_16609_MAC_ALGORITHM_1  ( (unsigned short) (('M'<<8)|'0') )// "M0"
#define SEC_KU_ISO_9797_1_MAC_ALGORITHM_1 ( (unsigned short) (('M'<<8)|'1') )// "M1"
#define SEC_KU_ISO_9797_1_MAC_ALGORITHM_2 ( (unsigned short) (('M'<<8)|'2') )// "M2"
#define SEC_KU_ISO_9797_1_MAC_ALGORITHM_3 ( (unsigned short) (('M'<<8)|'3') )// "M3". MAC Key.
#define SEC_KU_ISO_9797_1_MAC_ALGORITHM_4 ( (unsigned short) (('M'<<8)|'4') )// "M4"
#define SEC_KU_ISO_9797_1_MAC_ALGORITHM_5 ( (unsigned short) (('M'<<8)|'5') )// "M5"

#define SEC_KU_CUP_SM4_MAC_ALGORITHM_0		( (unsigned short) (('M'<<8)|'y') )// "MY"
#define SEC_KU_MAC_ALL					   ( (unsigned short) (('M'<<8)|'Z') )// "MZ"

#define SEC_KU_PIN_ENCRYPTION             ( (unsigned short) (('P'<<8)|'0') )// "P0". PIN Key, WK.
#define SEC_KU_KPV                        ( (unsigned short) (('V'<<8)|'0') )// "V0". PIN verification, KPV, other algorithm.
#define SEC_KU_PIN_VERIFICATION_IBM_3624  ( (unsigned short) (('V'<<8)|'1') )// "V1"
#define SEC_KU_PIN_VERIFICATION_VISA_PVV  ( (unsigned short) (('V'<<8)|'2') )// "V2"

/**
 * 表征特定 key 数据可以被使用的 算法类型.
 * 这里借鉴 TR-31 中的描述方式来定义, 参见 " ANS X9 TR-31 (2010)", "A.5.2 Algorithm".
 */
#define SEC_KA_AES                        ( (char)('A') )
#define SEC_KA_DEA                        ( (char)('D') )
#define SEC_KA_ELLIPTIC_CURVE             ( (char)('E') )
#define SEC_KA_HMAC_SHA_1                 ( (char)('H') )
#define SEC_KA_RSA                        ( (char)('R') )
#define SEC_KA_DSA                        ( (char)('S') )
#define SEC_KA_TDEA                       ( (char)('T') )      // Triple DEA
#define SEC_KA_SM4						  ( (char)('M') )      // SM2 DEA

/**
 * 标识特定密钥的 "mode of use" 的数据类型. 
 * 这里借鉴 TR-31 中的描述方式, 参见 " ANS X9 TR-31 (2010)", "A.5.3 Mode of Use". 
 */
#define SEC_MOU_ENC_DEC_WRAP_UNWRAP  ( (char)('B') )    // Both Encrypt & Decrypt / Wrap &Unwrap. "KUM" : Key Use Mode.
#define SEC_MOU_GENERATE_AND_VERIFY  ( (char)('C') )    // Both Generate & Verify : 主要针对 MAC : 生成 MAC or 校验 MAC.
#define SEC_MOU_DEC_OR_UNWRAP_ONLY   ( (char)('D') )    // Decrypt / Unwrap Only : 只能用来解密, 比如 MK.
#define SEC_MOU_ENC_OR_WRAP_ONLY     ( (char)('E') )    // Encrypt / Wrap Only : 只能用在加密, 比如 PIN key. 
#define SEC_MOU_GENERATE_ONLY        ( (char)('G') )    // Generate Only : 只能用来生成 MAC.
#define SEC_MOU_NO_RESTRICTIONS      ( (char)('N') )    // No special restrictions (other than restrictions implied by the Key Usage)
#define SEC_MOU_SIGNATURE_ONLY       ( (char)('S') )    // Signature Only
#define SEC_MOU_VERIFY_ONLY          ( (char)('V') )    // Verify Only : 只能用来校验 MAC, 内部计算的 MAC 结果不能导出.
#define SEC_MOU_DERIVE_KEYS          ( (char)('X') )    // Key used to derive other key(s) : 可以是 DUKPT init key 的 属性. 

/**
 * 标识特定密钥的 "key version number" 的数据类型. 
 * 出于实现考虑, 本类型的具体 value 和 TR-31 中定义的形态不同,
 * TR-31 的定义方式, 参见 " ANS X9 TR-31 (2010)", "A.5.4 Key Version Number".
 * 成员定义, 参见 "Table A-6. Key Version Number definition"
 */
#define SEC_KV_NOT_USED          ( (unsigned short)(('0'<<8)|'0') )
// 若 TR-31 格式密文密钥中的 key block header 的 "key version number" field 中的 "First character" 是 'c', 
// 则其携带的信息 "不是" key 实例的本征属性. 

/**
 * 标识特定密钥的 被导出行为配置 的数据类型. 
 * 参见 " ANS X9 TR-31 (2010)", "A.5.5 Exportability", "Table A-7. Defined Values for Exportability Byte".
 */
/** "Exportable under a KEK in a form meeting the requirements of X9.24 Parts 1 or 2." */
#define SEC_KE_EXPORTABLE_UNDER_KEK           ( (char)('E') )
#define SEC_KE_NON_EXPORTABLE                 ( (char)('N') )      // 通常默认. 
/** "Sensitive, Exportable under a KEK in a form not necessarily meeting the requirements of X9.24 Parts 1 or 2.." */
#define SEC_KE_SENSITIVE                      ( (char)('S') )


/****macrodef of optional BLOCK ID**********************************/
#define SEC_OPT_KEY_BLOCK_ID_KSN ((unsigned short)('K'<<8|'S'))//‘KS’ 0x4B53 Key Set Identifier,
#define SEC_ENC_KEY_FMT_TR31_MAC_LEN 8 // TR31- 密文中MAC长度


typedef enum
{
	SEC_MKSK=0,
	SEC_DUKPT,
	SEC_RSA_KEY,
}SEC_KEY_TYPE;//the key_type for the API



typedef struct
{
    u16  key_usage;
    u8   key_alg;
    u8	 key_modeofuse;
    u16  key_version;
    u8	 key_export;
}ST_KEY_Attr;

typedef struct
{
	u16 keylen;//SEC_KEY_TYPE
	u8 *keydata;
	u16	kcvmode;//0=no kcv,1-standard kcv
	u16 kcvlen;
	u8 	*kcv;
	u8  *iksn;
}ST_API_FMT_NORMAL_KEY_Block;

typedef enum{ KEY_FMT_NORMAL=0,KEY_FMT_TR31}KEY_BLOCK_FMT;
typedef struct
{
	u32 version;//fixed as 0x00 
	u16 depend_type;//SEC_KEY_TYPE
	u16 depend_id;
	u16 id;
	KEY_BLOCK_FMT format;//指定key格式:0-FMT_WITHOUT_KCV,1-FMT_WITHKCV， 2-FMT_TR31
	ST_KEY_Attr * keyattr;//format非2-FMT_TR31时需要
}ST_API_Store_Key;

int LvosStoreKey(u32 hWnd,u32 key_type,void * key,void* keydata);


#define DATA_DEC	0
#define DATA_ENC	1

#define DATA_MODE_ECB		0
#define DATA_MODE_CBC		1
#define DATA_MODE_OFB		2
#define DATA_MODE_CFB		3

typedef enum {DUKPT_KEY_PIN=0,
			  DUKPT_KEY_MAC_BOTH,
			  DUKPT_KEY_MAC_RSP,
			  DUKPT_KEY_DATA_BOTH,
			  DUKPT_KEY_DATA_RSP
			  }SEC_DUKPT_KEY_SELECT;

typedef struct 
{
	u32 version;
	u32 keyindex;
	u8 *iv;
	u32 enc_or_dec;
	u32 mode;//CBC/ECB/OFB/CFB....
	u32 dukpt_key_select;//:SEC_DUKPT_KEY_SELECT only for dukpt
}ST_API_DataCalc_Key;
int LvosDataCalc(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);


////for gen_or_verify
#define MAC_GENERATE_MAC       (0)
#define MAC_VERIFY_MAC         (1)

////for mac_alg
//#define MAC_ALG_ISO9797        (1000)         // ISO9797 算法, 标准 MAC 算法. 具体是算法 1 还是 3，根据 mac 相关 API 调用的 key 的用途来决定.
#define MAC_ALG_ISO_9797_1_MAC_ALG1 1001
//#define MAC_ALG_ISO_9797_1_MAC_ALGORITHM_2 1002
#define MAC_ALG_ISO_9797_1_MAC_ALG3 1003
//#define MAC_ALG_ISO_9797_1_MAC_ALG4 1004
//#define MAC_ALG_ISO_9797_1_MAC_ALG5 1005
#define MAC_ALG_ISO_16609_MAC_ALG1 2000 //same to 9797-1-alg3
#define MAC_ALG_FAST_MODE		3000//全部异或再加密
#define MAC_ALG_X9_19			3001//same to 9797-1-alg3
#define MAC_ALG_CBC				3002//TDES/DES cbc encryption and output the last block
#define MAC_ALG_CUP_SM4_MAC_ALGORITHM_0    3002

//for mac_padding
#define MAC_PADDING_MODE_1     (1)      // 填充模式 1
#define MAC_PADDING_MODE_2     (2)      // 填充模式 2



typedef struct 
{
	u32 version;
	u32 keyindex;
	u32 gen_or_verify;//MAC_GENERATE_MAC or MAC_VERIFY_MAC
	u32 mac_alg;//算法选择
	u32 mac_padding;//填充算法
	u8* mac_iv;
	u8  mac_in[8];//only for mac verify;padding 0x00 on the right for masked bytes;
	u32 dukpt_key_select;//:SEC_DUKPT_KEY_SELECT,only for dukpt key
}ST_API_MAC_KEY;
int LvosMacCalc(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);

// for  pinblock_fmt
#define SEC_PIN_BLK_ISO_FMT0     0
#define SEC_PIN_BLK_ISO_FMT1     1
#define SEC_PIN_BLK_ISO_FMT2     2
#define SEC_PIN_BLK_ISO_FMT3     3
#define SEC_PIN_BLK_EPS			 4
#define SEC_PIN_BLK_IBM_3621     5
#define SEC_PIN_BLK_IBM_3624	 6


typedef struct
{
	u32 version;
	u32 keyindex;
	char*len_sets;
	u32 pinblock_fmt;
	u32 timeoutms;
	u8 * datain;
}ST_API_PIN_KEY;
int LvosGetPinBlock(u32 hWnd,u32 key_type,void* key,void *pinblock);

typedef struct
{
     u32  ModLen;  
     u8   Mod[256]; 
     u8   Exp[4];    
     u8   randLen;   /*random data len*/
     u8   rand[8];   /*random data*/
} ST_API_RSA_ICC;

int LvosSecCtrl(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);

#define SEC_RET_OK		                (0)  
#define SEC_RET_ERROR                   (-1)		
#define SEC_ROOTKEY_ERROR		    (-1000)                
#define SEC_ERR_LOCKED              (SEC_ROOTKEY_ERROR-1)  /*Security System locked*/	

#define SEC_ERR_DATA                (SEC_ROOTKEY_ERROR-2)  /*SEC file data FsRead/FsWrite error*/
#define SEC_ERR_KEYINDEX            (SEC_ROOTKEY_ERROR-3)  
#define SEC_ERR_KEYFAIL             (SEC_ROOTKEY_ERROR-4)  /*Ker verify fail*/ 
#define SEC_ERR_NOPIN               (SEC_ROOTKEY_ERROR-5)  /*Not input PIN*/
#define SEC_ERR_INPUT_CANCEL        (SEC_ROOTKEY_ERROR-6)  /*Cancel PIN Enter*/
#define SEC_ERR_PIN_TIMEOUT         (SEC_ROOTKEY_ERROR-7)  /*Input PIN timeout*/
#define SEC_ERR_SMALL_INTERVAL      (SEC_ROOTKEY_ERROR-8)  /*Call Get PIN function is less then small interval time*/
#define SEC_ERR_KCV_MODE            (SEC_ROOTKEY_ERROR-9) 
#define SEC_ERR_KCV_FAIL            (SEC_ROOTKEY_ERROR-10) /*KCV verify fail*/
#define SEC_ERR_KCV_ODD             (SEC_ROOTKEY_ERROR-11) 
#define SEC_ERR_NO_MATCH            (SEC_ROOTKEY_ERROR-12) /*No right to use the key*/
#define SEC_ERR_KEYTPYE             (SEC_ROOTKEY_ERROR-13) 
#define SEC_ERR_KEYLEN		        (SEC_ROOTKEY_ERROR-14) 
#define SEC_ERR_EXPLEN              (SEC_ROOTKEY_ERROR-15) /**Expect len is not match*/
#define SEC_ERR_DSTKEY_INDEX        (SEC_ROOTKEY_ERROR-16) 
#define SEC_ERR_SRCKEY_INDEX        (SEC_ROOTKEY_ERROR-17) 
#define SEC_ERR_SRCKEY_TYPE	        (SEC_ROOTKEY_ERROR-18)

#define SEC_ERR_GROUP_INDEX         (SEC_ROOTKEY_ERROR-31)
#define SEC_ERR_NULL_PTR            (SEC_ROOTKEY_ERROR-32)
#define SEC_ERR_NOKCV               (SEC_ROOTKEY_ERROR-34) 
#define SEC_ERR_DUKPT_OVERFLOW      (SEC_ROOTKEY_ERROR-35)
#define SEC_ERR_DUKPT_KEYTYPE       (SEC_ROOTKEY_ERROR-36)
#define SEC_ERR_NEED_ADD_KSN        (SEC_ROOTKEY_ERROR-37) 


#define SEC_ERR_KEY_USAGE         (SEC_ROOTKEY_ERROR-38)   /**  试图在 key 的 "usage" 之外, 使用该 key.                                            */
#define SEC_ERR_MODE_OF_KEY_USE   (SEC_ROOTKEY_ERROR-39)   /**  对 key 的使用方式错误. 比如用被限定只能用来加密的 key 来解密数据.                  */
#define SEC_ERR_KEY_USAGE_AND_MODE_OF_USE_NOT_MATCH (SEC_ROOTKEY_ERROR-40)   /**  待下载的 key 的 usage 和 mode of use 属性不匹配.                                   */
#define SEC_ERR_NOT_SUPPORT (SEC_ROOTKEY_ERROR-41)
#define SEC_ERR_KEYATTR_NOT_MATCH (SEC_ROOTKEY_ERROR-42)
#define SEC_ERR_NO_AUTH             (SEC_ROOTKEY_ERROR-51)  /*Security System is not authentication*/	

int SecStoreKey(u32 key_type,ST_API_Store_Key * key,void* keydata);
#endif


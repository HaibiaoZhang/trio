#ifndef __LIBPRINTER_H__
#define __LIBPRINTER_H__

#include "type.h"

#define PRINTER_NORMAL        0
#define PRINTER_WIDER         1
#define PRINTER_REVERSE       2

#define PRINTER_RET_BASE          -9000
#define PRINTER_RET_SUCCESS       0
#define PRINTER_RET_FAIL          (PRINTER_RET_BASE)
#define PRINTER_RET_TIMEOUT       (PRINTER_RET_BASE - 1)
#define PRINTER_RET_ABORT         (PRINTER_RET_BASE - 2)
#define PRINTER_RET_PARAM         (PRINTER_RET_BASE - 3)

typedef struct{
	int					Got;			//0--未获取到
	int 				PrintheadWidth; //打印头宽度，单位点
	unsigned char		MaxSpeed;       //可设置的最大速度
	unsigned char 		MaxDensity;		//可设置的最大浓度
	unsigned char		Reserved[8];	//保留
}PRN_DEVICE_INFO;

typedef struct{
	int					Got;			//0--未获取到
	char 				CurrentTemp;    //打印头当前温度，单位摄氏度
	char 				CurrentDensity; //当前设置的浓度
	char 				CurrentSpeed;   //当前设置的速度
	char 				IsOverHeated;   //1--打印头过热
	char 				IsNoPaper;      //1--缺纸
	unsigned char		Reserved1[3];	//保留
	
	int 				CurrentVoltage; //打印电压，电池电压压降后的值
	int 				BufLeftLines;   //API内部使用，剩余打印空间
	int 				ErrorNo;        //错误码，0--无错误
	unsigned char		Reserved2[4];	//保留
}PRN_DEVICE_STATUS;

int Print_Devinfo(PRN_DEVICE_INFO *info);
int Print_status(PRN_DEVICE_STATUS *status);
int Print_density(int  density);
int Print_GErrno (char *errno_desc, int  buf_size);
int Print_Clear (void);
int Print_SetLineHeight(int kerning);
int Print_QRCode(char *data,int align);
int Print_BMP(char *file_name, int align);
int Print_Rawdata(u8 *raw_data, int width, int height ,int align);
int Print_Str(u8 * str,int fontsize,int align,int attr);
int Print_feed(int length);
int Print_Start(void);
#endif


#ifndef __BITMAP_H__
#define __BITMAP_H__

#define BMP_DISPLAY_WIDTH	128

#define ERR_BMP_BASE		 (-400)
#define ERR_BMP_TYPE		(ERR_BMP_BASE -1)	/*非WINDOWS BMP*/
#define ERR_BMP_PLANE		(ERR_BMP_BASE -2)	/*PLANE必须总为1*/
#define ERR_BMP_COLOR		(ERR_BMP_BASE -3)	/*非单色图片*/
#define ERR_BMP_COMPRESS	(ERR_BMP_BASE -4)	/*不支持压缩算法*/
#define ERR_BMP_WIDTH		(ERR_BMP_BASE -5)	/*太宽*/
#define ERR_BMP_HEIGHT		(ERR_BMP_BASE -6)	/*太高*/
#define ERR_BMP_OPEN		(ERR_BMP_BASE -7)	/*打开图片失败*/
#define ERR_BMP_READ		(ERR_BMP_BASE -8)	/*读取图片失败*/

/*位图文件头*/
typedef struct __TBMPFILEHEADER {
	unsigned char szType[2];
	unsigned char szSize[4];
	unsigned char szReserved[4];
	unsigned char szOffBits[4];
	unsigned char szHeadSize[4];
} TBMPFILEHEADER; 
#define BMPHEADSIZE 18////结构体可能会字节4字节对齐

typedef struct __TBMPINFOHEADER { /* bmih */
	unsigned char  szWidth[4];
	unsigned char  szHeight[4];
	unsigned char  szPlanes[2];/*该值总是为01H*/
	unsigned char  szBitCount[2];/*1 - 单色位图4 - 16 色位图8 - 256 色位图16 - 16bit 高彩色位图24 - 24bit 真彩色位图32 - 32bit 增强型真彩色位图 */
	unsigned char  szCompression[4];
	unsigned char  szSizeImage[4];/*用字节数表示的位图数据的大小。该数必须是4的倍数 */
	unsigned char  szXPelsPerMeter[4];
	unsigned char  szYPelsPerMeter[4];
	unsigned char  szClrUsed[4];
	unsigned char  szClrImportant[4];
} TBMPINFOHEADER; 
#define BMPINFOSIZE 36////结构体可能会字节4字节对齐

typedef struct __TRGBQUAD { /* rgbq */
	unsigned char rgbBlue;
	unsigned char rgbGreen;
	unsigned char rgbRed;
	unsigned char rgbReserved;
} TRGBQUAD;

/*位图信息头*/
typedef struct __TBMPINFO { /* bmi */
	TBMPINFOHEADER bmiHeader;
	TRGBQUAD bmiColors[1];/*可能存在一些颜色数据*/
} TBMPINFO; 


#define BYTE2USHORT(a) ((unsigned short)(a[1]<<8)+a[0])
#define BYTE2UINT(a) ((unsigned int)(a[3]<<24)+(a[2]<<16)+(a[1]<<8)+a[0])
#define USHORT2BYTE(a,b) {b[0]=(unsigned char)((a&0xFF00)>>8);b[1]=(unsigned char)(a&0xFF);}

#endif



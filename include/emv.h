#ifndef __DEV_H__
#define __DEV_H__

#include <EmvL2.h>

#include "type.h"

#define UP_PIN_FREE	 
#ifdef	UP_PIN_FREE

#define	UP_DEBITAID			"A000000333010101"
#define	UP_CREDITAID		"A000000333010102"
#define	UP_QUASICREDITAID	"A000000333010103"
#define	UP_EC				"A000000333010106"

#define	ONLINEPIN_CVM		0x80
#define	SIGNATURE_CVM		0x40
#define	CD_CVM				0x80
#define	NO_CVM				0x00

enum{
	UP_DEBITAPP,
	UP_CREDITAPP,
	UP_QUASICREDITAPP,
	UP_ECAPP,
	UP_UNKNOWAPP
};

enum{ 
	ONLINEPIN_REQUIR ,
	SIGNATURE_REQUIR,
	ONLINEPIN_FREE,
	ONLINEPIN_UNKNOW
};

#endif

#define EMV_CASH          0x01     
#define EMV_GOODS	      0x02       
#define EMV_SERVICE       0x04      
#define EMV_CASHBACK      0x08     

#define BLACK_FILE		"EMVBLACK"  
#define AID_PARAM_FILE 	"EMVAID"	 
#define CAPK_PARAM_FILE	"EMVCAPK"	
#define DEFAULT_FILE    "EMVDEFAULT" 

#define EMV_RET_OK          0
#define EMV_RET_BASE        -2000
#define EMV_RET_FAIL        (EMV_RET_BASE-0)
#define EMV_RET_PARAM       (EMV_RET_BASE-1)
#define EMV_RET_ERR_AID     (EMV_RET_BASE-2)
#define EMV_RET_ERR_DEFAUT  (EMV_RET_BASE-3)
#define EMV_RET_ERR_CAPK    (EMV_RET_BASE-4)
#define EMV_RET_FALLBACK    (EMV_RET_BASE-5)
#define EMV_RET_ONLINE_APPROVE (EMV_RET_BASE-6)
#define EMV_RET_ONLINE_DENIAL  (EMV_RET_BASE-7)
#define EMV_RET_USER_ABORT     (EMV_RET_BASE-8)
#define EMV_RET_ONLINE_ABORT   (EMV_RET_BASE-9)

// for emvcompletion
#ifndef EMV_ONLINE_OUTCOME
typedef enum {
	ONLINE_APPROVE,
	ONLINE_DENIAL,	
	ONLINE_FAILED,
	ONLINE_DENIAL_05
}EMV_ONLINE_OUTCOME;
#endif

#if 0
typedef enum 
{
 STEP_PREPROC=1, 
 STEP_APPBUILDLIST ,  
 STEP_FINALSEL, 
 STEP_INITAPP,
 STEP_READAPP,
 STEP_DATAAUTH,
 STEP_PROCRESTRIC, 
 STEP_TERMRISK, 
 STEP_CVMPROC, 
 STEP_TAA, 
 STEP_COMPLETION,
 STEP_PAYPASS_WHOLE,

}EMV_STEP;
#endif

typedef enum {
  EMV_RS_SUCCESS = 0,      // Success
  EMV_RS_APPROVE,          // Offline Approve
  EMV_RS_DECLIENED,        // Offline Declined
  EMV_RS_ONLINE_APPROVE,   // Online Approve
  EMV_RS_ONLINE_DECLIENED, // Online Declined
  EMV_RS_ABORT,            // Abort by Cardholder
  EMV_RS_TRY_AGAIN,        // Try Again
  EMV_RS_OTHER_INTERFACE,  // Use Other Interface
  EMV_RS_OTHER_CARD,       // Use Other Card
  EMV_RS_ONLINE_REQUEST,   // Online Request
  EMV_RS_ERROR,            // Error
}E_EMVRESULT;

/*AID*/
typedef struct {
	// other
	byte OnlinePin[1]; 
	// to ST_AIDLIST
	byte AIDLen[1];  
	byte AID[16];	  
	byte SelFlag[1]; 
	// to ST_AIDPARAM
	byte TargetPer[1]; 	  
	byte MaxTargetPer[1];	  
	byte FloorLimitCheck[1]; 
	byte FloorLimit[4];	  
	byte Threshold[4]; 	  
	byte TACDenial[5]; 	  
	byte TACOnline[5]; 	  
	byte TACDefault[5];	  
	byte dDOLLen[1];		
	byte dDOL[32]; 		  
	byte Version[2];		
	// TO ST_PREPARAM
	byte IsExistCVMLmt[1]; 				
	byte CVMLmt[6];						
	byte IsExistTermClssLmt[1];		
	byte TermClssLmt[6];					
	byte IsExistTermClssOfflineFloorLmt[1]; 
	byte TermClssOfflineFloorLmt[6];		
	byte IsExistTermOfflineFloorLmt[1];	 
	byte TermOfflineFloorLmt[6];
}ST_AID;

/**
 * 终端默认参数结构体
 **/
typedef struct
{
  byte Version[4];       // 0
  byte MerchCateCode[2]; //  9F15
  byte MerchId[16];      // 9F16
  byte MerchName[128];   // 9F4E
  byte CountryCode[2];   // 9F1A
  byte TerminalType[1];  // 9F35
  byte TermId[8];        //  9F1C
  byte Capability[3];    // 9F33
  byte AddCapability[5]; // 9F40
  byte RMDLen[1];        // 
  byte RiskManData[8];   // 9F1D
  byte tDOLLen[1];       // 
  byte tDOL[32];         // 97
  byte TermAIP[2];       // 
  byte IFDSn[8];         // 9F1E
  byte AcquierId[6];     // 9F01
  byte TransCurrCode[2]; // 5F2A
  byte TransCurrExp[1];  // 5F36
  byte ReferCurrCode[2]; // 9F3C
  byte ReferCurrExp[1];  // 9F3D
  byte ReferCurrCon[4];  // 9F3A 
  byte TTQ[4];           // 9F66
  byte ECTTLVal[6];		 // 9F7B
  byte Category[2];      // 9F53
  byte TSN[4];           // HEX TAG_9F41_TS_SEQ_COUNTER
} ST_DEFAULT;

/*交易动态参数*/
typedef struct
{
  byte Type[1];            // TAG_9C_TS_TYPE
  byte Category[2];        // 9F53
  byte TSN[4];             // HEX TAG_9F41_TS_SEQ_COUNTER
  byte Date[3];            // BCD YYHHMM TAG_9A_TS_DATE
  byte Time[3];            // BCD HHMMSS TAG_9F21_TS_TIME
  byte Amt[6];             // BCD TAG_9F02_AMT_N
  byte OtherAmt[6];        //  9F03
  byte TTQ[4];             // Terminal Transaction Qualifiers 9F66
  byte TransCurrCode[2];   // 5F2A
  byte TransCurrExp[1];    // 5F36
  byte ReferCurrCode[2];   // TAG_9F3C_TS_REF_CURR_CODE,
  byte ReferCurrExp[1];    // TAG_9F3D_TS_REF_CURR_EXP,
  byte ReferCurrCon[4];    // 9F3A (交易货币对参考货币的汇率*1000)
  byte IsReadLogInCard[1]; // FF900B
  //other
  byte ForceOnline[1];  //  FF9005
  byte TrackEncrypt[1]; //  FF9008
  byte Reversal[10];    // 
} ST_TRANS;

typedef struct
{
  byte RID[5];
  byte Expire[4];
  byte Index;
} ST_CAPK_COND;

typedef struct
{
  byte AIDLen;  //AID
  byte AID[16]; 
} ST_AID_COND;

void EMV_SetDebug(int isDebug);
int EMV_GetDebug();
	
void EMV_SetIsInputPIN(int isInput);

int EMV_GetCurKernelType();

int EMV_ClearAID();
int EMV_AddAID(ST_AID * pxAid);
	
int EMV_ClearCAPK();
int EMV_AddCAPK(ST_CAPK * pxCAPK);
	
int EMV_SetDefault(ST_DEFAULT * pxDEF);

int EMV_SetKernalData(ushort usTag, byte *val, int valLen);

int EMV_GetKernalData(ushort usTag, byte * val, uint *len);

int EMV_GetKernalTLVs(const ushort * tagList, uint num, byte * data, uint * dataLen);

int EMV_GetCardData(ushort usTag, byte *val, int *valLen);


/*
CVM function:
ONLINEPIN_FREE - 
ONLINEPIN_REQUIR - 
SIGNATURE_REQUIR -
ONLINEPIN_UNKNOW -
*/
int EMV_CheckCVMType();


int EMV_GetCurKernelType();

/**
power on call once
**/
int EMV_InitCore();

/**
parameter changed call
**/
int EMV_LoadParam2Kernel();

int EMV_InitTransaction(int transType);

int EMV_CLSSPreProcess();

int EMV_EMVProcess_Simple();

int EMV_EMVProcess();

int EMV_CLSSProcess_Simple();

int EMV_CLSSProcess();

int EMV_Completion(int OnlineResult, byte * IssuAuthenData, byte IssuAuthenDataLen, 
	byte * Issu71Script, byte Issu71ScriptLen, byte * Issu72Script, byte Issu72ScriptLen);

int EMV_GetOutCome(ST_OUTCOME * stOutCome);

/*callback*/
extern int EMV_GetTransParameter(int type, ST_TRANS * pxTRANS);
extern int EMV_CheckId(byte type, byte * no);
extern int EMV_SelectApp(int isRetry, int num, ST_CANDLIST * appList);
extern int EMV_InputOnlinePIN();
extern int EMV_ReadCardData();

// type =0 before GPO pxPARAM = NULL
// type =1 after ReadAppData PxPARAM = NULL
// return 0 -CONTINUE, OTHER - ABORT
extern int EMV_Callback(byte type, void * pxPARAM);

#endif



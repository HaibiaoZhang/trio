#ifndef __SYS_H__
#define __SYS_H__

#define HWND_APP 1000

void Sys_UpdateSignal();

int Sys_GetHandle(const char * pdev_name);
int Sys_ReadParam(int handle, const char * name, char * val, uint valSize);
int Sys_WriteParam(int handle, const char * name, const char * val);
int Sys_ReadCfg(const char * keyword, byte * val, uint size);

void Sys_GetTime(char * date, char * time);
void Sys_SetTime(const char * date, const char * time);
void Sys_Delay(uint ms);

int Sys_ReadLBS(byte * data);
int Sys_ReadLAC(byte * data);
int Sys_ReadCellID(byte * data);
int Sys_ReadSIMID(byte * data);
int Sys_ReadIMEI(byte * data);
int Sys_ReadIMSI(byte * data);
int Sys_ReadMCC(byte * data);
int Sys_ReadMNC(byte * data);

int Sys_ReadTUSN(byte * sn);
int Sys_ReadSN(byte * sn);
int Sys_ReadVendor(byte * vendor);
int Sys_ReadModel(byte * model);
int Sys_ReadPN(byte * pn);

int Sys_ReadOSVer(byte * sn);
int Sys_IsCharging();
int Sys_ReadBAT();
int Sys_GetFreeSpace();

void Sys_Beep(int baud, int ms);
void Sys_BeepOK();
void Sys_BeepFAIL();

int Sys_MallocInit(byte * buffer, uint size);
byte * Sys_Malloc(uint size);
void Sys_Free(void * addr);

int Sys_GetResInfo(const char* tag, uint *addr, uint *len);

void Sys_Random(byte * data, uint len);

extern void uart_debug(const char *str, ...);
extern void usb_debug(const char *str, ...);

void app_debug(const char *str, ...);
void Sys_PrintHex(byte *data, uint len);

#define DBG_STR(fmt, args...) {usb_debug("%s:%d "fmt"\r\n", __FUNCTION__, __LINE__, ##args); LvosSysDelayMs(10);}
#define DBG_HEX(title, data, len) {usb_debug(title); LvosSysDelayMs(10); Sys_PrintHex(data, len);}

#endif


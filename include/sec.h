#ifndef __SEC_H__
#define __SEC_H__

// 10以内密钥索引用于特殊用途，因此应用层主密钥索引实际从11开始
#define SEC_MAINKEY_INDEX   11	// 主密钥起始索引
#define SEC_KEK_INDEX       1	// 传输密钥索引，用于保护主密钥
#define SEC_MACKEY_INDEX    2	// MAC工作密钥索引
#define SEC_PINKEY_INDEX    3	// PIN工作密钥索引
#define SEC_TRACKKEY_INDEX  4	// 磁道加密密钥索引
#define SEC_TUSNKEK_INDEX   5	// KS8230 TUSN KEK密钥索引
#define SEC_TUSNKEY_INDEX   6	// KS8230 TUSN KEY 密钥索引
#define SEC_SCANKEY_INDEX   7	//扫码POS秘钥
#define SEC_TUSN_INDEX      0	// TUSN密钥

#ifndef SEC_TUSN
#define SEC_TUSN 3
#endif

// 20200817 

typedef enum{
    PKA_DECRYPT=0,
    PKA_ENCRYPT,
}PKA_CRYPT_MODE;
	
typedef enum{
    PKA_CIPHER_MODE_ECB=0,
    PKA_CIPHER_MODE_CBC,
    PKA_CIPHER_MODE_CFB,
    PKA_CIPHER_MODE_OFB,
}PKA_DES_MODE;

enum TYPE_TERM_HARD_INFO
{
	TYPE_TERM_HARD_LIFE			=0x01,
	TYPE_TERM_HARD_SN			=0x02,
	TYPE_TERM_HARD_KEY			=0x04,
	TYPE_TERM_HARD_ALL			=0x07,
};

#ifndef SEC_RET_OK
#define SEC_RET_OK 0
#endif
#define SEC_RET_BASE      -5000
#define SEC_RET_SAVE_MK   (SEC_RET_BASE-1)
#define SEC_RET_SAVE_WK   (SEC_RET_BASE-2)
#define SEC_RET_CALC_MAC  (SEC_RET_BASE-3)
#define SEC_RET_NO_PIN    (SEC_RET_BASE-4)
#define SEC_RET_PIN_ABORT (SEC_RET_BASE-5)
#define SEC_RET_PIN_ERR   (SEC_RET_BASE-6)
#define SEC_RET_ENCRYPT   (SEC_RET_BASE-7)
#define SEC_RET_SAVE_KEK  (SEC_RET_BASE-8)
#define SEC_RET_PAN_ERR   (SEC_RET_BASE-9)
#define SEC_RET_KEY_ERR   (SEC_RET_BASE-10)
#define SEC_RET_CHK_ERR   (SEC_RET_BASE-11)

int Sec_WriteKEK(byte kekIdx, byte * kek, byte kekLen);
int Sec_SaveMainKey(byte kekIdx, byte mkIdx, byte * key, byte keyLen, byte * checkVal);
int Sec_SavePINKey(byte mkIdx, byte * key, byte keyLen, byte * checkVal);
int Sec_SaveMACKey(byte mkIdx, byte * key, byte keyLen, byte * checkVal);
int Sec_SaveTDKKey(byte mkIdx, byte * key, byte keyLen, byte * checkVal);
int Sec_CalcMAC(byte * data, uint dataLen, byte * mac);
int Sec_CalcXORMAC(byte * data, uint dataLen, byte * mac);
int Sec_CalcCBCMAC(byte * data, uint dataLen, byte * mac);
int Sec_CalcCBC2MAC(byte * data, uint dataLen, byte * mac);

int Sec_EncryptTrack(byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);
int Sec_DecryptTrack(byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);

// keylen 最长16字节
int Sec_WriteAESKey(byte keyIndex, byte * key, uint keyLen);
int Sec_Aes(byte keyIndex, PKA_CRYPT_MODE mode, byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);
// keylen 最长32字节
int Sec_SoftAes(byte *iv, byte *datain, byte *dataout, uint datalen, byte *key, uint keylen, PKA_DES_MODE mode, PKA_CRYPT_MODE cMode);
// keylen 最长24字节
int Sec_WriteDESKey(byte keyIndex, byte * key, uint keyLen);
int Sec_Des(byte keyIndex, PKA_CRYPT_MODE mode, byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);
// keylen 最长24字节
int Sec_SoftDes(byte *iv, byte *datain, byte *dataout, uint datalen, byte *key, uint keylen, PKA_DES_MODE mode, PKA_CRYPT_MODE cMode);
int Sec_CalcTUSNMac(byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);
int Sec_EncryptTUSN(byte * dataIn, uint dataInLen, byte * dataOut, uint * dataOutLen);
int Sec_EnterPIN(const char * lenSet, byte * card, byte * pin, uint timeMS);

int Sec_Sm4(byte *iv, byte *datain, byte *dataout, uint datalen, byte *key, PKA_DES_MODE mode, PKA_CRYPT_MODE cMode);


#endif

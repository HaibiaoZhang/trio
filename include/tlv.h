/*
 自定义TLV操作函数
 定长2字节TAG,T长度2字节
 可用于参数处理
*/
#ifndef __TLV__
#define __TLV__

typedef struct
{
	ushort t;  /*!< \brief Tag ID */
	ushort l;  /*!< \brief Length of Data */
	byte *v;  /*!< \brief Value */
} TLVTag;

typedef struct
{
	ushort mlen;   /*!< \brief max data length */
	ushort len;      /*!< \brief buffer data length */
	byte  *buf; /*!< \brief data buffer */
} TLVBuf;

int Tlv_GetTag(TLVBuf * buffer, ushort tag, byte * val, ushort valSize);
int Tlv_SetTag(TLVBuf    * buffer, ushort tag, byte * val, ushort valLen);
int Tlv_DelTag(TLVBuf     * buffer, ushort tag);

#endif


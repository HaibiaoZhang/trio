#ifndef __FILE_H__
#define __FILE_H__

#define RET_FILE_OK             0
#define RET_FILE_BASE           -400
#define RET_FILE_PARAM          (RET_FILE_BASE-1)
#define RET_FILE_FILE_OPEN      (RET_FILE_BASE-2)
#define RET_FILE_FILE_READ      (RET_FILE_BASE-3)
#define RET_FILE_FILE_WRITE     (RET_FILE_BASE-4)
#define RET_FILE_FILE_SEEK      (RET_FILE_BASE-5)
#define RET_FILE_FILE_SIZE      (RET_FILE_BASE-6)
#define RET_FILE_NO_RECORD      (RET_FILE_BASE-7)
#define RET_FILE_NOT_FOUND      (RET_FILE_BASE-8)
#define RET_FILE_DATA           (RET_FILE_BASE-9)
#define RET_FILE_REMOVE         (RET_FILE_BASE-10)
#define RET_FILE_NOT_MATCH      (RET_FILE_BASE-11)
#define RET_FILE_END            (RET_FILE_BASE-12)
#define RET_FILE_TIMEOUT        (RET_FILE_BASE-13)

/*
记录遍历回调函数
用户遍历或查找记录
输入参数：
	no：记录号从0开始
	data：记录数据
	len：记录数据长度
	param：附加参数，可用于匹配记录
返回值：>= 0 匹配结束
        < 0 继续遍历
*/
typedef int (*RecodeForEach)(int no, byte * data, uint len, void * param);

int File_GetRecordNum(const char * name, uint recSize);
int File_Read(const char * name, int offset, byte * data, uint len);
int File_Write(const char * name, int offset, byte * data, uint len);
int File_Append(const char * name, byte * data, uint len);
int File_ReadRecord(const char * name, uint no, uint recSize, byte * data);
int File_UpdateRecord(const char * name, uint no, uint recSize, byte * data );
int File_ForEachRecord(const char * name, uint recSize, byte * data, RecodeForEach each, void * param);
int File_Size(const char * name);
int File_Remove(const char * name);
int File_IsExist(const char * name);

#endif


#ifndef __LIMITAPI_H__
#define __LIMITAPI_H__
#include "ui.h"

#define LIMITSDK_RET_BASE          -9000
#define LIMITSDK_RET_SUCCESS       0
#define LIMITSDK_RET_FAIL          (LIMITSDK_RET_BASE)
#define LIMITSDK_RET_TIMEOUT       (LIMITSDK_RET_BASE - 1)
#define LIMITSDK_RET_ABORT         (LIMITSDK_RET_BASE - 2)
#define LIMITSDK_RET_PARAM         (LIMITSDK_RET_BASE - 3)

// 和OS层共享的数据结构
typedef struct	{
	int offset;		
	int count;		
	int	tmsflag;  	
	int VerDate;	
}ST_OSTMSFLAG;

//Signature
#define PW_SIGN_TOP_LEFT                    0x01 //起点左靠上
#define PW_SIGN_TOP_CENTER                  0x02 //起点中靠上
#define PW_SIGN_MID_LEFT                    0x03 //起点左靠中
#define PW_SIGN_MID_CENTER                  0x04 //居中心位置

#define PW_SIGN_WM_NOT_BUILT_IN             0x00 //watermark 水印内嵌模式, 只作为背景图，不作为数据流输出
#define PW_SIGN_WM_BUILT_IN                 0x01 //watermark 水印内嵌模式, 嵌入数据流输出

typedef struct
{
    char* pTitle;
    char* pTips;
    u32  style;  //default 0x00 : Auto THEME
}WIDGET_HEAD;


typedef struct
{
    WIDGET_HEAD head;
    u32  timeout;
    char* pStr;
    char* bgName;
    u32  bgPosition;
    u32  watermarkMode;
	u32  ctrl;	// 0x01 support cancel
}SIGNATURE_WIDGET;

int LvoslitEnable (char * pwd);
int LvoslitESign(SIGNATURE_WIDGET* sign, byte *dataout, uint *datalen);
int LvoslitWriteTmsArea (uint offset,byte *data, uint len);
void LvoslitClearTmsArea (u32 size);
int LvoslitWriteTmsShare (ST_OSTMSFLAG * tmsFlag);
int LvoslitReadTmsShare (ST_OSTMSFLAG * tmsFlag);
int LvoslitUpdateTmsApp (char *app_name, char *app_version);
#if 0
int LvoslitPkaAes(byte *iv, byte *datain, byte *dataout, uint datalen, byte *key, uint keylen, PKA_DES_MODE mode, PKA_CRYPT_MODE cMode);
int LvosliPkaDes(u8 *indata, u8 *outdata, u8 *key, u32 mode);
int LvoslitPkaSm4(byte *iv, byte *datain, byte *dataout, uint datalen, byte *key, PKA_DES_MODE mode, PKA_CRYPT_MODE cMode);
#endif
int LvoslitGetFontDot(u8* buf,u8 * dot,int* width,int* height);
int LvoslitSysReCover();
#endif


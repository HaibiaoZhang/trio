#ifndef __UI_H__
#define __UI_H__

#include "type.h"

#define QRCODE_PADDING    4							// 白色边框宽度
#define QRCODE_EXPAND_MIN 2							// 最小放大倍数
#define QRCODE_EXPAND_MAX 5							// 最大放大倍数
#define QRCODE_HEIGHT_MAX 160 						// 最大高度
#define QRCODE_TOP_LINE   2							// 顶部标题栏行数

#define UI_FONT_SMALL         0
#define UI_FONT_NORMAL        1
#define UI_FONT_BIG           2

#define UI_ATTR_FONTSIZE      0		// 0-小 1-普通 2-大
#define UI_ATTR_FONTCOLOR     1		// 字体颜色
#define UI_ATTR_BGCOLOR       2		// 背景颜色
#define UI_ATTR_REVER         3		// 1-带背景
#define UI_ATTR_CLOSEICON     4		// 1-关闭图标


#define UI_REDLIGHT           0x01
#define UI_GREENLIGHT         0x02
#define UI_YELLOWLIGHT        0x04
#define UI_BLUELIGHT          0x08
#define UI_ALLLIGHT           0X0F


#ifndef RGB // R5|G6|B5 16位色
#define RGB(r, g, b) ((uint)(((byte)(r)) >> 3 << 11) | (uint)(((byte)(g)) >> 2 << 5) | (uint)(((byte)(b)) >> 3) )
#endif


#define UI_RET_BASE          -1000
#define UI_RET_SUCCESS       0
#define UI_RET_FAIL          (UI_RET_BASE)
#define UI_RET_TIMEOUT       (UI_RET_BASE - 1)
#define UI_RET_ABORT         (UI_RET_BASE - 2)
#define UI_RET_PARAM         (UI_RET_BASE - 3)


#define DISPLAY_WIDTH       320
#define DISPLAY_HEIGHT      240
#define DISPLAY_LINE_SIZE   30

#define DISPLAY_CHAR_WIDTH   12  // px
#define DISPLAY_CHAR_HEIGHT  24  // px
#define DISPLAY_LINE_PADDING 4   // 320 不被24整除，多8个像素
//20(16x),26(12x),40(8x)
#define DISPLAY_LINE_NUM    10  // 240/24 = 10
#define DISPLAY_LINE_HZ_NUM 13  // 320/24 = 13
#define DISPLAY_LINE_EN_NUM 26  // 320/12 = 26
#define DISPLAY_LINE_BIG_NUM 17  // 320/18 = 17

#define DISPLAY_COLOR_BLACK  RGB(0, 0, 0)        // #000000
#define DISPLAY_COLOR_WHITE  RGB(255, 255, 255)  // #FFFFFF
#define DISPLAY_COLOR_GRAY   RGB(221, 221, 221)  // #DDDDDD
#define DISPLAY_COLOR_RED    RGB(255, 0, 0)    // #FF0000
#define DISPALY_COLOR_GREEN  RGB(0, 255, 0)    // #00FF00

#define DISPLAY_FONT_COLOR   DISPLAY_COLOR_BLACK
#define DISPLAY_BG_COLOR     DISPLAY_COLOR_WHITE


#define INPUT_MAX_SIZE       100

#define KB0	        '0'
#define KB1	        '1'
#define KB2	        '2'
#define KB3	        '3'
#define KB4	        '4'
#define KB5	        '5'
#define KB6	        '6'
#define KB7	        '7'
#define KB8	        '8'
#define KB9	        '9'

#define KB_DOT      '.'
#define KB_FN       0x01
#define KB_ALPHA    0x02
#define KB_CLEAR    0x03
#define KB_UP       0x05
#define KB_DOWN     0x06
#define KB_ENTER    0x0d
#define KB_CANCEL   0x1b
#define KB_NONE     0x00

typedef enum {
  DISPLAY_CENTER = 1,
  DISPLAY_LEFT,
  DISPLAY_RIGHT
}E_DISPALIGN;

typedef enum {
  MENU_1 = 1,
  MENU_2,
  MENU_3
}E_MENUTYPE;
  
typedef enum {
	  INPUT_NUM = 1,
	  INPUT_STR,
	  INPUT_AMT,
	  INPUT_HEX,
	  INPUT_IP,
	  INPUT_PWD
}E_INPUTTYPE;
	  
typedef enum {
	QR_TYPE_NUL = -1,   ///< Terminator (NUL character). Internal use only
	QR_TYPE_NUM = 0,    ///< Numeric mode
	QR_TYPE_AN,         ///< Alphabet-numeric mode
	QR_TYPE_8,          ///< 8-bit data mode
	QR_TYPE_KANJI,      ///< Kanji (shift-jis) mode
	QR_TYPE_STRUCTURE,  ///< Internal use only
	QR_TYPE_ECI,        ///< ECI mode
	QR_TYPE_FNC1FIRST,  ///< FNC1, first position
	QR_TYPE_FNC1SECOND, ///< FNC1, second position
} E_QRTYPE;

typedef struct 
{
    u16 bfType;
    u32 bfSize;
    u16 bfReserved1;
    u16 bfReserved2;
    u32 bfOffBits;

    u32 biSize;
    int biWidth;
    int biHeight;
    u16 biPlanes;
    u16 biBitCount;
    u32 biCompression;
    u32 biSizeImage;
    int biXPelsPerMeter;
    int biYPelsPerMeter;
    u32 biClrUsed;
    u32 biClrImportant;
} __attribute__ ((packed)) BMP_HEAD;

typedef int (*MenuCallback)(void * pxParam);

typedef struct {
  int          kb;
  const char * text;
  MenuCallback func;
  void *       param;
}ST_MENUITEM;


void Ui_SetLcdType(int type);
int Ui_GetLcdType();
int Ui_GetLcdWidth();
int Ui_GetLcdHeight();
int Ui_GetCharWidth();
int Ui_GetCharHeight();
int Ui_GetLineEnNum();
int Ui_GetLineHzNum();
int Ui_GetLinePadding();
int Ui_GetLineNum();

void Ui_SetKBHandle(int handle);
void Ui_SetDisplayHandle(int handle);
void Ui_ClearColor(uint color);
void Ui_Clear();
void Ui_ClearBlockColor(int x, int y, int width, int height, uint color);
void Ui_ClearBlock(int x, int y, int width, int height);
void Ui_ClearLineColor(int line, uint color);
void Ui_ClearLine(int start, int end);
void Ui_Disp(int x, int y, const char *text, uint fontColor, uint bgColor, uint size, uint rever);
void Ui_DispTextColor(int x, int y, const char *text, uint color);
void Ui_DispText(int x, int y, const char *text, int isReverse);
void Ui_DispTextAlign(int y, int align, const char *text, int isReverse);
void Ui_DispTextLineAlign(int line, int align, const char *text, int isReverse);
void Ui_DispTextLineAlignColor(int line, int align, const char *text, uint fontColor, uint bgColor);
void Ui_DispBigNumStr(int y, int align, uint color, const char *text);

void Ui_DispBMP(int x, int y, int w, int h, const char * name);
int Ui_DispSimpleBMP(int x, int y, const char * name);
int Ui_GetBMPInfo(const char * name, int *width, int * height);
int Ui_DispQRCode(int x, int y, const char *text);
int Ui_DispQRCodeNew(int x, int y, const char *text);
int Ui_DispQRCodeMini(int x, int y, int type, int expend, const char *text);

void Ui_DispTitle(const char * title);

void Ui_ClearKey();
byte Ui_GetKey();
byte Ui_WaitKey(uint timeMS);

int Ui_Dailog(const char * title, const char * text, int yesNo, uint timeMS);
int Ui_Menu(const char *title, const ST_MENUITEM *menu, int num, int mode, uint timeMS);
int Ui_Select(const char *title, const char *info, const ST_MENUITEM * menu, int num, int mode, uint timeMS);

int Ui_Input(int start, const char *title, const char *text, char *value, int mode, uint max, uint min, uint timeMS);
int Ui_InputStrSelectMode(int start, const char *title, const char *text, char *value, int mode, uint max, uint min, uint timeMS);

int Ui_InputStr(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputStrEx(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputNum(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputPwd(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputHex(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputIp(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Ui_InputAmt(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);


int Ui_LightOn(int lights);
int Ui_LightOff(int lights);

void Ui_BreakPoint();

#endif


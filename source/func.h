#ifndef __FUNC_H__
#define __FUNC_H__

	
void Func_DispTransTitle(byte * title);
void Func_DispRetCode(byte * title,const char * rspMsg);

int Func_Detect(int handle, char * reset);
void Func_RemoveCard();

int Func_Signature(char * title, byte * code, byte * data, uint size);

int Func_EnterPIN(int isRetry);

int Func_Scan(const char * amt, char * payCode, uint ms);
int Func_GetPayType(const char * payCode);

int Func_InputNum(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Func_InputPwd(const char * title, const char * text, char * value, uint max, uint min, uint timeMS);
int Func_DispInfo(const char *pTitle, const char *pMsg, int timeOutMs);
int Func_Menu(const char *title, const ST_MENUITEM * menu, int num, uint timeMS);
#endif


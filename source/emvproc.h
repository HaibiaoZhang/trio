#ifndef __EMVPROC_H__
#define __EMVPROC_H__


int EMV_Process(int cardType);
int EMV_Process_Simple(int cardType);
int EMV_QPSProcess();

/*callback*/
int EMV_SelectApp(int isRetry, int num, ST_CANDLIST * appList);
int EMV_CheckId(byte type, byte * no);
int EMV_InputOnlinePIN();
int EMV_ReadCardData();
int EMV_InitCAPK(void * pxPARAM);
int EMV_InitAidList(void * pxPARAM);
int EMV_InitDefault(void * pxPARAM);

#endif


#ifndef __TMS_H__
#define __TMS_H__


// FLASH 记录长度
#define TMS_FLASH_RECSIZE          1024
// FLASH 对齐长度
#define TMS_FLASH_PAGESIZE         4096
// 一次下载8K
#define TMS_DOWNLOAD_SIZE 		   8192	

#define TMS_BREAKFILE 				"TMSBREAK.DAT"

typedef struct  {
	char Url[256];	// 放在前面为后面的扩展升级准备
	char taskId[32+1];
	int uFileSize; 	// 文件的大小
	int offset;		// 当前写入数据的偏移长度
	int count;		// 下载过程重次数 最多5次
} ST_TMSINFO;

// 0-无更新信息 1-更新成功 2-更新中断
int Tms_GetUpdateStaus();

// 清除更新标记和断点文件
int Tms_ClearUpdate();

// <0-更新失败 0-无需更新 1-更新失败,重试次数超限，更新成功会重启
int Tms_Update(const char * appId, const char * ver, const char * code);

// 更新完成通知
int Tms_Notice(const char * taskId, int code, const char * info);

// 检查TMS更新结果, 成功则发更新完成通知，断点则继续下载
int Tms_CheckTms(const char * appId, const char * ver, const char * code);
int Tms_GetServerTime(void);
int Tms_GetServerWifiInfo(void);
int Tms_GetStringData(cJSON * pxJson, char * keyid,char * value);

#endif



#ifndef __DISPTIP_H__
#define __DISPTIP_H__

enum
{
    LANGUAGE_START = 0,
    LANGUAGE_EN = LANGUAGE_START,
    LANGUAGE_CN,
    LANGUAGE_NUMS,
};
#define LANG_STR_NUMS        (LANGUAGE_NUMS)

static  unsigned char *	_prompt_warn[LANG_STR_NUMS] =
{
	"WARNING" ,
	"警告",
};
static  unsigned char *	_prompt_nosurport[LANG_STR_NUMS] =
{
	"no surport" ,
	"功能不支持",
};
static  unsigned char *	_prompt_soundflag[LANG_STR_NUMS] =
{
	"sound switch" ,
	"音量开关",
};
static unsigned char *	_prompt_keytone[LANG_STR_NUMS] =
{
	"set key tone" ,
	"设置按键音",
};
static unsigned char *	_prompt_open[LANG_STR_NUMS] =
{
	"open" ,
	"打开",
};
static unsigned char *	_prompt_close[LANG_STR_NUMS] =
{
	"close" ,
	"关闭",
};
static unsigned char *	_prompt_reset[LANG_STR_NUMS] =
{
	"restore setting" ,
	"恢复出厂设置",
};
static unsigned char *	_prompt_wait[LANG_STR_NUMS] =
{
	"pls waiting" ,
	"请稍后",
};
static unsigned char *	_prompt_end[LANG_STR_NUMS] =
{
	"the end" ,
	"已完成",
};
static unsigned char *	_prompt_reboot[LANG_STR_NUMS] =
{
	"pls reboot" ,
	"请重启设备",
};
static unsigned char *	_prompt_enter[LANG_STR_NUMS] =
{
	"ENTER          CANCEL",
	"[确定]继续 [取消]退出",
};
static unsigned char *	_prompt_clrdata[LANG_STR_NUMS] =
{
	"clear all data?",
	"操作将清除所有数据",
};
static unsigned char *	_prompt_time[LANG_STR_NUMS] =
{
	"TIME MODIFY",
	"时间修改",
};
static unsigned char *	_prompt_dtime[LANG_STR_NUMS] =
{
	"input daytime:",
	"请输入日期时间:",
};

static unsigned char * _prompt_format[LANG_STR_NUMS] =
{
	"format(YYYYMMDDHHMMSS)",
	"时间格式(YYYYMMDDHHMMSS)",
};

static unsigned char * _prompt_ctime[LANG_STR_NUMS] =
{
	"current daytime",
	"当前日期时间",
};
static unsigned char * _prompt_timeerror[LANG_STR_NUMS] =
{
	"format is wrong",
	"日期时间错误",
};
static unsigned char * _prompt_scan[LANG_STR_NUMS] =
{
	"scanning...",
	"扫描热点",
};
static unsigned char * _prompt_nosid[LANG_STR_NUMS] =
{
	"no ssid",
	"无热点",
};
static unsigned char * _prompt_selsid[LANG_STR_NUMS] =
{
	"select ssid",
	"请选择热点",
};

static unsigned char * _prompt_confirmsid[LANG_STR_NUMS] =
{
	"confirm ssid",
	"确认选择的热点",
};
static unsigned char * _prompt_ssid[LANG_STR_NUMS] =
{
	"ssid:",
	"所选热点:",
};
static unsigned char * _prompt_entercan[LANG_STR_NUMS] =
{
	"ENTER/GO CANCEL/RESEL",
	"[确认]继续 [取消]重选",
};
static unsigned char * _prompt_ssidpwd[LANG_STR_NUMS] =
{
	"set ssid pwd",
	"设置热点密码",
};
static unsigned char * _prompt_curssidpwd[LANG_STR_NUMS] =
{
	"current ssid pwd:",
	"当前热点密码:",
};
static unsigned char * _prompt_enterfix[LANG_STR_NUMS] =
{
	"[enter] to fix",
	"按[确认键]修改",
};
static unsigned char * _prompt_issidpwd[LANG_STR_NUMS] =
{
	"input ssid pwd:",
	"请输入热点密码:",
};
static unsigned char * _prompt_wifisetok[LANG_STR_NUMS] =
{
	"WIFI setting success",
	"WIFI设置成功",
};
static unsigned char * _prompt_ssidname[LANG_STR_NUMS] =
{
	"set ssid name",
	"设置热点名称",
};
static unsigned char * _prompt_curssid[LANG_STR_NUMS] =
{
	"current ssid",
	"当前热点",
};
static unsigned char * _prompt_issidname[LANG_STR_NUMS] =
{
	"input ssid name:",
	"请输入热点名称:",
};
static unsigned char * _prompt_ssidmode[LANG_STR_NUMS] =
{
	"set ssid mode",
	"设置热点模式",
};
static unsigned char * _prompt_curmode[LANG_STR_NUMS] =
{
	"current ssid mode",
	"当前模式",
};
static unsigned char * _prompt_wifissid[LANG_STR_NUMS] =
{
	"set wifi ssid",
	"设置WIFI热点",
};
static unsigned char * _prompt_scanssid[LANG_STR_NUMS] =
{
	"scan ssid?",
	"是否扫描热点",
};
static unsigned char * _prompt_yes[LANG_STR_NUMS] =
{
	"1 yes",
	"1 是",
};
static unsigned char * _prompt_no[LANG_STR_NUMS] =
{
	"0 no",
	"0 否",
};

static unsigned char * _prompt_curcomm[LANG_STR_NUMS] =
{
	"current:GPRS",
	"当前:GPRS",
};
static unsigned char * _prompt_onlycomm[LANG_STR_NUMS] =
{
	"only surport GPRS",
	"设备仅支持GPRS",
};
static unsigned char * _prompt_commok[LANG_STR_NUMS] =
{
	"set comtype succes",
	"通讯方式设置成功",
};
static unsigned char * _prompt_nosim[LANG_STR_NUMS] =
{
	"no sim card",
	"未插入sim卡",
};
static unsigned char * _prompt_signalweak[LANG_STR_NUMS] =
{
	"signal weak",
	"GPRS信号弱",
};
static unsigned char * _prompt_contimeout[LANG_STR_NUMS] =
{
	"connect timeout",
	"连接超时",
};
static unsigned char * _prompt_confail[LANG_STR_NUMS] =
{
	"connect fail",
	"连接失败",
};
static unsigned char * _prompt_connectnet[LANG_STR_NUMS] =
{
	"connect host",
	"正在连接网络",
};
static unsigned char * _prompt_initcom[LANG_STR_NUMS] =
{
	"init comm",
	"通讯初始化",
};
static unsigned char * _prompt_cardno[LANG_STR_NUMS] =
{
	"confirm card No:",
	"请确认卡号:",
};
static unsigned char * _prompt_otherface[LANG_STR_NUMS] =
{
	"use other interface",
	"请使用其他界面交易",
};
static unsigned char * _prompt_again[LANG_STR_NUMS] =
{
	"try again",
	"请重试",
};

static unsigned char * _prompt_refuse[LANG_STR_NUMS] =
{
	"trans refuse",
	"交易拒绝",
};
static unsigned char * _prompt_loadprm[LANG_STR_NUMS] =
{
	"load default prm",
	"导入默认终端参数",
};
static unsigned char * _prompt_loadsuc[LANG_STR_NUMS] =
{
	"load success",
	"导入成功",
};
static unsigned char * _prompt_loadfail[LANG_STR_NUMS] =
{
	"load fail",
	"导入失败",
};
static unsigned char * _prompt_loadaid[LANG_STR_NUMS] =
{
	"load aid prm",
	"导入AID参数",
};
static unsigned char * _prompt_loadcapk[LANG_STR_NUMS] =
{
	"load capk prm",
	"导入CAPK参数",
};
static unsigned char * _prompt_loading[LANG_STR_NUMS] =
{
	"loading...",
	"正在导入",
};
static unsigned char * _prompt_entergo[LANG_STR_NUMS] =
{
	"right? ENTER/GO",
	"正确按[确认]继续",
};
static unsigned char * _prompt_certificate[LANG_STR_NUMS] =
{
	"certificate:",
	"证件:",
};
static unsigned char * _prompt_tempidcard[LANG_STR_NUMS] =
{
	"temporary id card:",
	"临时身份证:",
};
static unsigned char * _prompt_entryid[LANG_STR_NUMS] =
{
	"entry permit:",
	"入境证:",
};
static unsigned char * _prompt_passport[LANG_STR_NUMS] =
{
	"passport:",
	"护照:",
};
static unsigned char * _prompt_officer[LANG_STR_NUMS] =
{
	"officer certificate:",
	"军官证:",
};
static unsigned char * _prompt_identity[LANG_STR_NUMS] =
{
	"identity card:",
	"身份证:",
};
static unsigned char * _prompt_notact[LANG_STR_NUMS] =
{
	"app not accept",
	"应用不接受",
};
static unsigned char * _prompt_reselect[LANG_STR_NUMS] =
{
	"pls reselect",
	"请重新选择",
};
static unsigned char * _prompt_recvdata[LANG_STR_NUMS] =
{
	"receiving data",
	"正在接收应答",
};

static unsigned char * _prompt_senddata[LANG_STR_NUMS] =
{
	"sending data",
	"正在发送请求",
};
static unsigned char * _prompt_current[LANG_STR_NUMS] =
{
	"current",
	"当前",
};
static unsigned char * _prompt_swip[LANG_STR_NUMS] =
{
	"pls swip",
	"请刷卡",
};
static unsigned char * _prompt_insert[LANG_STR_NUMS] =
{
	"pls insert",
	"请插入IC卡",
};
	static unsigned char * _prompt_tap[LANG_STR_NUMS] =
{
	"pls tap",
	"请挥卡(或放手机)",
};
static unsigned char * _prompt_insertswip[LANG_STR_NUMS] =
{
	"pls insert/swip",
	"请插入IC卡或刷卡",
};
static unsigned char * _prompt_swiptap[LANG_STR_NUMS] =
{
	"pls swip/tap",
	"请刷卡或挥卡",
};
static unsigned char * _prompt_inserttap[LANG_STR_NUMS] =
{
	"pls insert/tap",
	"请插入IC卡或挥卡",
};
static unsigned char * _prompt_alltype[LANG_STR_NUMS] =
{
	"pls insert/swip/tap",
	"请插卡或刷卡或挥卡",
};
static unsigned char * _prompt_input[LANG_STR_NUMS] =
{
	"or input",
	"或手输卡号",
};
static unsigned char * _prompt_keyboard[LANG_STR_NUMS] =
{
	"tap on/reverse keyboard",
	"请在键盘上或键盘背面挥卡",
};
static unsigned char * _prompt_swiperror[LANG_STR_NUMS] =
{
	"swip error",
	"刷卡错误",
};
static unsigned char * _prompt_goonswip[LANG_STR_NUMS] =
{
	"pls go on swip",
	"请继续刷卡",
};
static unsigned char * _prompt_cancelq[LANG_STR_NUMS] =
{
	"or <CANCEL> quit",
	"或按<取消>退出",
};
static unsigned char * _prompt_ischipcard[LANG_STR_NUMS] =
{
	"chip card",
	"该卡为芯片卡",
};
static unsigned char * _prompt_magcard[LANG_STR_NUMS] =
{
	"mag card test",
	"磁条卡测试",
};
static unsigned char * _prompt_chipcard[LANG_STR_NUMS] =
{
	"chip card test",
	"插卡测试",
};
static unsigned char * _prompt_inputamt[LANG_STR_NUMS] =
{
	"pls input amt:",
	"请输入交易金额:",
};
static unsigned char * _prompt_pwderror[LANG_STR_NUMS] =
{
	"pwd is error",
	"密码输入错误",
};
static unsigned char * _prompt_process[LANG_STR_NUMS] =
{
	"trans is process",
	"交易处理中",
};
static unsigned char * _prompt_success[LANG_STR_NUMS] =
{
	"test sucess",
	"测试成功",
};
static unsigned char * _prompt_fail[LANG_STR_NUMS] =
{
	"tset fail",
	"测试失败",
};
static unsigned char * _prompt_faildown[LANG_STR_NUMS] =
{
	"tset fail,swip card",
	"测试失败,请降级刷卡",
};
static unsigned char * _prompt_tapcard[LANG_STR_NUMS] =
{
	"tap card test",
	"挥卡测试",
};
static unsigned char * _prompt_amtexceed[LANG_STR_NUMS] =
{
	"fail,amt exceed",
	"测试失败,金额超限",
};
static unsigned char * _prompt_ascnum[LANG_STR_NUMS] =
{
	"asc&num",
	"显示文字数字",
};
static unsigned char * _prompt_barcode[LANG_STR_NUMS] =
{
	"barcode",
	"显示二维码",
};
static unsigned char * _prompt_bitmap[LANG_STR_NUMS] =
{
	"bitmap",
	"显示图片",
};
static unsigned char * _prompt_fulscreen[LANG_STR_NUMS] =
{
	"full screen",
	"全屏显示",
};
static unsigned char * _prompt_backlight[LANG_STR_NUMS] =
{
	"lcd backlight",
	"背光测试",
};
static unsigned char * _prompt_gprswifi[LANG_STR_NUMS] =
{
	"GPRS/WIFI",
	"GPRS/WIFI通讯",
};
static unsigned char * _prompt_blutooth[LANG_STR_NUMS] =
{
	"blutooth",
	"蓝牙通讯",
};
static unsigned char * _prompt_usb[LANG_STR_NUMS] =
{
	"USB",
	"USB通讯",
};
static unsigned char * _prompt_uart[LANG_STR_NUMS] =
{
	"UART",
	"UART通讯",
};
static unsigned char * _prompt_poweroff[LANG_STR_NUMS] =
{
	"power off",
	"关机",
};
static unsigned char * _prompt_reboottip[LANG_STR_NUMS] =
{
	"reboot",
	"重启",
};
static unsigned char * _prompt_sleepawak[LANG_STR_NUMS] =
{
	"sleep awake",
	"休眠唤醒",
};
static unsigned char * _prompt_rcfg[LANG_STR_NUMS] =
{
	"read sys cfg",
	"读取系统配置",
};
static unsigned char * _prompt_timertest[LANG_STR_NUMS] =
{
	"timer test",
	"定时器测试",
};
static unsigned char * _prompt_delaytest[LANG_STR_NUMS] =
{
	"delay test",
	"延时测试",
};
static unsigned char * _prompt_randomtest[LANG_STR_NUMS] =
{
	"random test",
	"随机数测试",
};
static unsigned char * _prompt_keytest[LANG_STR_NUMS] =
{
	"key system test",
	"密钥体系测试",
};
static unsigned char * _prompt_beepertest[LANG_STR_NUMS] =
{
	"beeper test",
	"蜂鸣器测试",
};
static unsigned char * _prompt_filesys[LANG_STR_NUMS] =
{
	"filesystem test",
	"文件系统测试",
};
static unsigned char * _prompt_ledtest[LANG_STR_NUMS] =
{
	"LED",
	"LED测试",
};
static unsigned char * _prompt_powermng[LANG_STR_NUMS] =
{
	"power manage test",
	"电源管理测试",
};
static unsigned char * _prompt_sysprmtest[LANG_STR_NUMS] =
{
	"sys prm test",
	"系统参数测试",
};
static unsigned char * _prompt_keboardtest[LANG_STR_NUMS] =
{
	"keyboard test",
	"按键测试",
};
static unsigned char * _prompt_emvprm[LANG_STR_NUMS] =
{
	"load emv prm",
	"导入EMV参数",
};
static unsigned char * _prompt_elecsign[LANG_STR_NUMS] =
{
	"elec sign",
	"电子签名",
};
static unsigned char * _prompt_devinfo[LANG_STR_NUMS] =
{
	"dev info",
	"查看设备信息",
};
static unsigned char * _prompt_commprm[LANG_STR_NUMS] =
{
	"comm prm",
	"通讯参数",
};
static unsigned char * _prompt_scorrect[LANG_STR_NUMS] =
{
	"touch screen correct",
	"屏幕校准",
};

static unsigned char * _prompt_cardtest[LANG_STR_NUMS] =
{
	"card test",
	"银行卡测试",
};
static unsigned char * _prompt_othercase[LANG_STR_NUMS] =
{
	"other case test",
	"其他项测试",
};
static unsigned char * _prompt_prmset[LANG_STR_NUMS] =
{
	"test prm set",
	"测试参数设置",
};
static unsigned char * _prompt_scantest[LANG_STR_NUMS] =
{
	"scan test",
	"扫码测试",
};
static unsigned char * _prompt_lcdtest[LANG_STR_NUMS] =
{
	"LCD test",
	"显示测试",
};
static unsigned char * _prompt_commtest[LANG_STR_NUMS] =
{
	"COMM test",
	"通讯测试",
};
static unsigned char * _prompt_mainmenu[LANG_STR_NUMS] =
{
	"main menu",
	"主菜单",
};
static unsigned char * _prompt_printertest[LANG_STR_NUMS] =
{
	"printer test",
	"打印测试",
};
static unsigned char * _prompt_touchtest[LANG_STR_NUMS] =
{
	"touch screen test",
	"触屏测试",
};
static unsigned char * _prompt_printerprm[LANG_STR_NUMS] =
{
	"set printer prm",
	"设置打印参数",
};
static unsigned char * _prompt_removecard[LANG_STR_NUMS] =
{
	"pls remove card",
	"请移卡",
};


#define STRINDEX   0
#define PROMPT_WARN                  _prompt_warn[STRINDEX]
#define PROMPT_NOSURPORT             _prompt_nosurport[STRINDEX]
#define PROMPT_SOUNDSWITCH           _prompt_soundflag[STRINDEX]
#define PROMPT_KEYTONE               _prompt_keytone[STRINDEX]
#define PROMPT_OPEN                  _prompt_open[STRINDEX]
#define PROMPT_CLOSE                 _prompt_close[STRINDEX]
#define PROMPT_RESET                 _prompt_reset[STRINDEX]
#define PROMPT_WAIT                  _prompt_wait[STRINDEX]
#define PROMPT_END                   _prompt_end[STRINDEX]
#define PROMPT_REBOOT                _prompt_reboot[STRINDEX]
#define PROMPT_ENTER                 _prompt_enter[STRINDEX]
#define PROMPT_CLRDATA               _prompt_clrdata[STRINDEX]
#define PROMPT_TIME                  _prompt_time[STRINDEX]
#define PROMPT_DTIME                 _prompt_dtime[STRINDEX]
#define PROMPT_FORMAT                _prompt_format[STRINDEX]
#define PROMPT_CTIME                 _prompt_ctime[STRINDEX]
#define PROMPT_TIMEERROR             _prompt_timeerror[STRINDEX]
#define PROMPT_SCAN                  _prompt_scan[STRINDEX]
#define PROMPT_NOSID                 _prompt_nosid[STRINDEX]
#define PROMPT_SELSID                _prompt_selsid[STRINDEX]
#define PROMPT_CONFIRMSID            _prompt_confirmsid[STRINDEX]
#define PROMPT_SSID                  _prompt_ssid[STRINDEX]
#define PROMPT_CONFIRMSID            _prompt_confirmsid[STRINDEX]
#define PROMPT_ENTERCAN              _prompt_entercan[STRINDEX]
#define PROMPT_SSIDPWD               _prompt_ssidpwd[STRINDEX]
#define PROMPT_CURSSIDPWD            _prompt_curssidpwd[STRINDEX]
#define PROMPT_ENTERFIX              _prompt_enterfix[STRINDEX]
#define PROMPT_ISSIDPWD              _prompt_issidpwd[STRINDEX]
#define PROMPT_WIFISETOK             _prompt_wifisetok[STRINDEX]
#define PROMPT_SSIDNAME              _prompt_ssidname[STRINDEX]
#define PROMPT_CURRENTSID            _prompt_curssid[STRINDEX]
#define PROMPT_ISSIDNAME             _prompt_issidname[STRINDEX]
#define PROMPT_SSIDMODE              _prompt_ssidmode[STRINDEX]
#define PROMPT_CURMODE               _prompt_curmode[STRINDEX]
#define PROMPT_WIFISSID              _prompt_wifissid[STRINDEX]
#define PROMPT_SCANSSID              _prompt_scanssid[STRINDEX]
#define PROMPT_YES                   _prompt_yes[STRINDEX]
#define PROMPT_NO                    _prompt_no[STRINDEX]
#define PROMPT_PRMSET                _prompt_prmset[STRINDEX]
#define PROMPT_CURCOMM               _prompt_curcomm[STRINDEX]
#define PROMPT_ONLYCOMM              _prompt_onlycomm[STRINDEX]
#define PROMPT_COMMOK                _prompt_commok[STRINDEX]
#define PROMPT_NOSIM                 _prompt_nosim[STRINDEX]
#define PROMPT_SIGNALWEAK            _prompt_signalweak[STRINDEX]
#define PROMPT_CONTIMEOUT            _prompt_contimeout[STRINDEX]
#define PROMPT_CONFAIL               _prompt_confail[STRINDEX]
#define PROMPT_CONNECTNET            _prompt_connectnet[STRINDEX]
#define PROMPT_INITCOM               _prompt_initcom[STRINDEX]
#define PROMPT_CARDNO                _prompt_cardno[STRINDEX]
#define PROMPT_OTHERFACE             _prompt_otherface[STRINDEX]
#define PROMPT_AGAIN                 _prompt_again[STRINDEX]
#define PROMPT_REFUSE                _prompt_refuse[STRINDEX]
#define PROMPT_LOADPRM               _prompt_loadprm[STRINDEX]
#define PROMPT_LOADSUC               _prompt_loadsuc[STRINDEX]
#define PROMPT_LOADFAIL              _prompt_loadfail[STRINDEX]
#define PROMPT_LOADCAPK              _prompt_loadcapk[STRINDEX]
#define PROMPT_LOADAID               _prompt_loadaid[STRINDEX]
#define PROMPT_LOADING               _prompt_loading[STRINDEX]
#define PROMPT_ENTERGO               _prompt_entergo[STRINDEX]
#define PROMPT_CERTIFICATE           _prompt_certificate[STRINDEX]
#define PROMPT_TEMPIDCARD            _prompt_tempidcard[STRINDEX]
#define PROMPT_ENTRYID               _prompt_entryid[STRINDEX]
#define PROMPT_PASSPORT              _prompt_passport[STRINDEX]
#define PROMPT_OFFICER               _prompt_officer[STRINDEX]
#define PROMPT_IDENTITY              _prompt_identity[STRINDEX]
#define PROMPT_NOTACT                _prompt_notact[STRINDEX]
#define PROMPT_RESELECT              _prompt_reselect[STRINDEX]
#define PROMPT_RECVDATA              _prompt_recvdata[STRINDEX]
#define PROMPT_SENDDATA              _prompt_senddata[STRINDEX]
#define PROMPT_CURRENT               _prompt_current[STRINDEX]
#define PROMPT_SWIP                  _prompt_swip[STRINDEX]
#define PROMPT_INSERT                _prompt_insert[STRINDEX]
#define PROMPT_TAP                   _prompt_tap[STRINDEX]
#define PROMPT_INSERTSWIP            _prompt_insertswip[STRINDEX]
#define PROMPT_INSERTTAP             _prompt_inserttap[STRINDEX]
#define PROMPT_SWIPTAP               _prompt_swiptap[STRINDEX]
#define PROMPT_ALLTYPE               _prompt_alltype[STRINDEX]
#define PROMPT_INPUT                 _prompt_input[STRINDEX]
#define PROMPT_KEYBOARD              _prompt_keyboard[STRINDEX]
#define PROMPT_SWIPERROR             _prompt_swiperror[STRINDEX]
#define PROMPT_GOONSWIP              _prompt_goonswip[STRINDEX]
#define PROMPT_CANCELQ               _prompt_cancelq[STRINDEX]
#define PROMPT_ISCHIPCARD            _prompt_ischipcard[STRINDEX]
#define PROMPT_MAGCARD               _prompt_magcard[STRINDEX]
#define PROMPT_CHIPCARD              _prompt_chipcard[STRINDEX]
#define PROMPT_INPUTAMT              _prompt_inputamt[STRINDEX]
#define PROMPT_PWDERROR              _prompt_pwderror[STRINDEX]
#define PROMPT_PROCESS               _prompt_process[STRINDEX]
#define PROMPT_SUCCESS               _prompt_success[STRINDEX]
#define PROMPT_FAIL                  _prompt_fail[STRINDEX]
#define PROMPT_FAILDOWN              _prompt_faildown[STRINDEX]
#define PROMPT_TAPCARD               _prompt_tapcard[STRINDEX]
#define PROMPT_AMTEXCEED             _prompt_amtexceed[STRINDEX]
#define PROMPT_ASCNUM                _prompt_ascnum[STRINDEX]
#define PROMPT_BARCOD                _prompt_barcode[STRINDEX]
#define PROMPT_BITMAP                _prompt_bitmap[STRINDEX]
#define PROMPT_FULSCREEN             _prompt_fulscreen[STRINDEX]
#define PROMPT_BACKLIGHT             _prompt_backlight[STRINDEX]
#define PROMPT_BLUTOOTH              _prompt_blutooth[STRINDEX]
#define PROMPT_USB                   _prompt_usb[STRINDEX]
#define PROMPT_UART                  _prompt_uart[STRINDEX]
#define PROMPT_POWEROFF              _prompt_poweroff[STRINDEX]
#define PROMPT_REBOOTTIP             _prompt_reboottip[STRINDEX]
#define PROMPT_SLEEPAWAKE            _prompt_sleepawak[STRINDEX]
#define PROMPT_RCFG                  _prompt_rcfg[STRINDEX]
#define PROMPT_TIMERTEST             _prompt_timertest[STRINDEX]
#define PROMPT_DELAYTEST             _prompt_delaytest[STRINDEX]
#define PROMPT_RANDOMTEST            _prompt_randomtest[STRINDEX]
#define PROMPT_KEYTEST               _prompt_keytest[STRINDEX]
#define PROMPT_BEEPERTEST            _prompt_beepertest[STRINDEX]
#define PROMPT_FILESYS               _prompt_filesys[STRINDEX]
#define PROMPT_LEDTEST               _prompt_ledtest[STRINDEX]
#define PROMPT_POWERMNG              _prompt_powermng[STRINDEX]
#define PROMPT_SYSPRMTEST            _prompt_sysprmtest[STRINDEX]
#define PROMPT_KEYBOARDTEST          _prompt_keboardtest[STRINDEX]
#define PROMPT_EMVPRM                _prompt_emvprm[STRINDEX]
#define PROMPT_ELECSIGN              _prompt_elecsign[STRINDEX]
#define PROMPT_DEVINFO               _prompt_devinfo[STRINDEX]
#define PROMPT_COMMPRM               _prompt_commprm[STRINDEX]
#define PROMPT_SCORRECT              _prompt_scorrect[STRINDEX]
#define PROMPT_CARDTEST              _prompt_cardtest[STRINDEX]
#define PROMPT_OTHERCASE             _prompt_othercase[STRINDEX]
#define PROMPT_SCANTEST              _prompt_scantest[STRINDEX]
#define PROMPT_LCDTEST               _prompt_lcdtest[STRINDEX]
#define PROMPT_COMMTEST              _prompt_commtest[STRINDEX]
#define PROMPT_MAINMENU              _prompt_mainmenu[STRINDEX]
#define PROMPT_PRINTERTEST           _prompt_printertest[STRINDEX]
#define PROMPT_TOUCHTEST             _prompt_touchtest[STRINDEX]
#define PROMPT_PRINTERPRM            _prompt_printerprm[STRINDEX]
#define PROMPT_REMOVECARD            _prompt_removecard[STRINDEX]
#endif



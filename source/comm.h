#ifndef __COMM_H__
#define __COMM_H__

#define HWND_UART0      2000
#define HWND_SCC0       2010
#define HWND_SCCL0      2020
#define HWND_MSR        2030
#define HWND_KB         2031
#define HWND_PRN        2032
#define HWND_IOCTL      2033
#define HWND_USBHID0    2050
#define HWND_WLAN0      2060
#define HWND_WLAN1      2061
#define HWND_BT         2070
#define HWND_WIFI       2080

#define COMM_RET_OK       0
#define COMM_RET_FAIL    -1
#define COMM_RET_TIMEOUT -2
#define COMM_RET_SIGNAL  -3
#define COMM_RET_CANCEL  -4
#define COMM_RET_RETRY   -5
#define COMM_RET_SIM     -6

int Comm_WifiEnable(int isEnable);
void Comm_UpdateSignal(int isImmediately);
int Comm_WriteParam(int handle, const char * param, const char * buffer);
int Comm_ReadParam(int handle, const char * param, char * buffer, uint size);
int Comm_GetWifiStatus(int handle, uint * wifiStat, uint * apStat, int *signal);

int Comm_PreInit(uint timeMS);
void Comm_Init();
int Comm_Connect(uint timeMS, uint isCancel);
int Comm_ConnectHttp(uint timeMS, uint isCancel, const char * host, const char * port);
int  Comm_Send(byte * data, uint dataLen);
int  Comm_Recv(byte * data, uint dataSize, uint timeMS, uint isCancel);
void Comm_Close();
int  Commu_Recv(int handle,byte * data, uint dataSize, uint timeMS);
int  Commbt_Recv(byte * data, uint dataSize);
int  Commbt_Send(byte * data, uint dataLen);

#endif


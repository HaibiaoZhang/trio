#ifndef __HTTP_H__
#define __HTTP_H__

#define HTTP_BUFFER_SIZE 4096


typedef struct
{
	int socket;
	int status;
	int isTrunked;
	int dataLength;
	int headerLength;
	int trunckedDataLen;  
	int packLen;
	int packSize;
	char *pack;
} HTTPHANDLE;

typedef enum{
  HTTP_ERR_PARAM = -500,
  HTTP_ERR_TIMEOUT,
  HTTP_ERR_RECV,
  HTTP_ERR_SEND,
  HTTP_ERR_DATA,
  HTTP_ERR_NEED_RECV, 
  HTTP_ERR_STATUS,
  HTTP_ERR_LEGNTH,
  HTTP_ERR_SIZE,
  HTTP_SUCCESS = 0
}HTTPRETURN;
  
int Http_Connect(const char * host, const char * port);

int Http_TcpSend(int handle, const char * data, int len);

int Http_TcpRecv(int handle, char * data, int dataSize);

void Http_Close();

int Http_InitHandle(HTTPHANDLE * handle, uint size);

void Http_Release(HTTPHANDLE * handle);

int Http_SetHeader(HTTPHANDLE * request, const char * key, const char * value);

int Http_InitRequest(HTTPHANDLE * request, const char * method, const char * host, const char * port, const char * url);

int Http_SetRequestBody(HTTPHANDLE * request, const char * body, uint len);

int Http_Send(HTTPHANDLE * handle);

int Http_Recv(HTTPHANDLE * handle, uint timeMS);

int Http_GetResponseCode(HTTPHANDLE *handle);

int Http_GetResponseHeader(HTTPHANDLE * handle, char * data, int dataSize);

int Http_GetResponseBody(HTTPHANDLE * handle, char * data, int dataSize);

int Http_TmsConnect(const char * host, const char * port);
int Http_TmsSend(HTTPHANDLE * handle);
int Http_TmsRecv(HTTPHANDLE * handle, uint timeMS);
typedef int (*DecodeCB)(void * data, int len, void * param);
int Http_DecodeResponseBody(HTTPHANDLE * response, void * param, DecodeCB callback);

#endif


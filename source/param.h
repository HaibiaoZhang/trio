#ifndef __PARAM_H__
#define __PARAM_H__

#define MAX_PARAM_SIZE 4096
#define MAX_WIFIAP_NUM 50

enum {
	T_INT,
	T_STR,
	T_BYTE,
	T_SHORT,
	T_BIN,
};
	
enum {
	WIFI_PSK,
	WIFI_WPA,
	WIFI_WPA2,
	WIFI_WEP,
};
	
typedef struct {
	ushort tag;
	ushort type;
	ushort len;
	byte * data;
}ST_PARAMDEF;

typedef struct {
	byte apName[50+1];		// WIFI热点名称
	byte apSignal[5+1];	    // WIFI信号强度
	byte apMode[32+1];		// WIFI热点密钥方式
}ST_WIFIAP;

//#define TMS_HOST_IP       "TMS主机IP"
//#define TMS_HOST_PORT     "TMS主机端口"

#define PARAM_MER_ID       "merchant id"
#define PARAM_TERM_ID      "terminal id"
#define PARAM_MER_NAME     "merchant name"
#define PARAM_HOST_IP      "host IP"
#define PARAM_HOST_PORT    "host port"
#define PARAM_HOST_IP2     "back IP"
#define PARAM_HOST_PORT2   "back port"
#define PARAM_HOST_TPDU    "TPDU"
#define PARAM_KEY_INDEX    "key index"
#define PARAM_COMM_TIMEOUT "comm timeout"
#define PARAM_COMM_RETRY   "comm retry"
#define PARAM_DUP_RETRY    "reversal retry"
#define PARAM_SIGN_RETRY   "sign retry"


ST_PARAM * Param_GetTermParam();


/*terminal param file*/
int Param_ReadParam();
int Param_WriteParam();
int Param_InitParam();

/*terminal ctrl file*/
int Param_ReadCtrl();

int Param_UserUnLock(void * pxPARAM);
int Param_UserManage(void * pxPARAM);


int Param_CommParam(void * pxPARAM);
int Param_TermParam(void * pxPARAM);
int Param_ClearTrace(void * pxPARAM);
int Param_ClearFlag(void * pxPARAM);

int Param_TmsDown(void * pxPARAM);

int Param_SetTime(void * pxPARAM);
int Param_SetYear(void * pxPARAM);


int Param_SetCommParam(void * pxPARAM);

int Param_Reset(void * pxPARAM);
int Param_KeySound(void * pxPARAM);

int Param_SetClss(void * pxPARAM);
int Param_AutoSetWIFI(void * pxPARAM);

int Param_Prmset(void * pxPARAM);
int Param_Printerprm(void * pxPARAM);

#endif




#include <libapp.h>
#include <ctype.h>

#include "app.h"
#include "http.h"


#ifdef HTTP_DEBUG
#define HTTP_STRLOG DBG_STR
#define HTTP_HEXLOG DBG_HEX
#else
#define HTTP_STRLOG  
#define HTTP_HEXLOG 
#endif

#define HTTP_HEADER_START_STR "HTTP/"
#define HTTP_CONTENT_LENGTH_STR "Content-Length:"
#define HTTP_TRANSFER_ENCODEING_STR "Transfer-Encoding:"
#define HTTP_TRUNKED_STR "chunked"
#define HTTP_HEADER_END_STR "\x0D\x0A\x0D\x0A"
#define HTTP_CRLF "\x0D\x0A"
#define HTTP_SPACE "\x20"

extern int Comm_ConnectHttp(uint timeMS, uint isCancel, const char * host, const char * port);
extern int Comm_RecvHttp(byte * data, uint dataSize);
extern int Comm_SendHttp(byte * data, uint dataLen);
extern void Comm_Close();

static char * Http_TrimLeft(char * str)
{
	char * pStart = str;

	if(pStart == NULL) {
		return NULL;
	}

	while(*pStart){
		if(*pStart != 0x20 && *pStart != 0x0D && * pStart != 0x0A){
			break;
		}
		pStart ++;
	}

	return pStart;
}

static char * Http_TrimRight(char * str)
{
	char * pEnd = NULL;

	if(str == NULL) {
		return NULL;
	}

	if(strlen(str) == 0){
		return str;
	}

	pEnd = str + strlen(str) - 1;

	do
	{
		if(*pEnd == 0x20 || *pEnd == 0x0D || *pEnd == 0x0A){
			*pEnd = 0;
		}
		break;
	}while(pEnd-- > str);

	return str;
}

// 获取TRUNKED 的数据长度
// length\r\ndata\r\n, last=0\r\n\r\n
// length 为16进制ASC码字符串
static int Http_GetTrunkedBlockLength(const char *body, int *dataOffset)
{
	int idx = 0;
	int iLength = 0;
	char szLength[20] = {0};
	char szAsc[20 + 1] = {0};
	char szBcd[20 + 1] = {0};
	char *pEnd = NULL;
	char *pStart = NULL;

	if (body == NULL || dataOffset == NULL)
	{
		return HTTP_ERR_PARAM;
	}

	pEnd = strstr(body, HTTP_CRLF);
	if (pEnd == NULL)
	{
		return HTTP_ERR_DATA;
	}
	iLength = pEnd - body;
	if (iLength > sizeof(szLength) - 1)
	{
		return HTTP_ERR_SIZE;
	}

	*dataOffset = iLength + 2;

	memcpy(szLength, body, iLength);
	Http_TrimRight(szLength);
	pStart = Http_TrimLeft(szLength);
	if (strlen(pStart) % 2)
	{
		szAsc[0] = '0';
		strcat(szAsc, pStart);
	}
	else
	{
		strcpy(szAsc, pStart);
	}

	Utils_Asc2Bcd((unsigned char *)szAsc, strlen(szAsc), (unsigned char *)szBcd);
	for(idx = 0, iLength = 0; idx < (strlen(szAsc) / 2); idx ++){
		iLength *= 256;
		iLength += (unsigned char )szBcd[idx];
	}

	return iLength;
}

/**
 * format : length\r\ndata\r\n, last=0\r\n\r\n
 */
static int Http_GetTrunkedLength(HTTPHANDLE *response)
{
	int iRet = 0;
	int iLength = 0;
	int iOffSet = 0;

	response->trunckedDataLen = 0;
	response->dataLength = 0;
	while (1)
	{
		int lenLength = 0;
		iRet = Http_GetTrunkedBlockLength(response->pack + response->headerLength + iOffSet, &lenLength);
		HTTP_STRLOG("Http_GetTrunkedBlockLength: lenLength = %d, iRet = %d", lenLength, iRet);
		if (iRet < 0)
		{
			return iRet;
		}
		if (iRet == 0)
		{
			response->dataLength = iLength;
			response->trunckedDataLen = iOffSet + lenLength + 2;
			return HTTP_SUCCESS;
		}
		iLength += iRet;		
		iOffSet += lenLength; 
		iOffSet += iRet;		
		iOffSet += 2;				
		if (iOffSet >= (response->packLen - response->headerLength))
		{
			return HTTP_ERR_LEGNTH;
		}
	}

	return HTTP_ERR_DATA;
}

static int Http_GetHeaderLength(HTTPHANDLE * response)
{
	char *pEnd = NULL;
	char *header = NULL;

	if (response == NULL || response->pack == NULL 
		|| memcmp(response->pack, HTTP_HEADER_START_STR, strlen(HTTP_HEADER_START_STR)) != 0)
	{
		return HTTP_ERR_PARAM;
	}
	header = response->pack;
	pEnd = strstr(header, HTTP_HEADER_END_STR);
	if (pEnd != NULL)
	{
		response->headerLength = pEnd - header + strlen(HTTP_HEADER_END_STR);
		return HTTP_SUCCESS;
	}

	return HTTP_ERR_NEED_RECV;
}

static int Http_GetStatus(HTTPHANDLE *response)
{
	int iLength = 0;
	char szStatus[10] = {0};
	char *pStart = NULL;
	char *pEnd = NULL;
	char *header = response->pack;

	if (header == NULL || memcmp(header, HTTP_HEADER_START_STR, strlen(HTTP_HEADER_START_STR)) != 0)
	{
		return HTTP_ERR_PARAM;
	}

	pStart = strstr(header, HTTP_SPACE);
	if (pStart == NULL)
	{
		return HTTP_ERR_DATA;
	}
	pStart += 1;
	pEnd = strstr(pStart, HTTP_SPACE);
	if (pEnd == NULL)
	{
		return HTTP_ERR_DATA;
	}
	iLength = pEnd - pStart;
	if (iLength > sizeof(szStatus) - 1)
	{
		return HTTP_ERR_SIZE;
	}
	memcpy(szStatus, pStart, iLength);
	Http_TrimRight(szStatus);
	pStart = Http_TrimLeft(szStatus);

	response->status = atoi(pStart);

	return HTTP_SUCCESS;
}

static int Http_GetContentLength(HTTPHANDLE *response)
{
	int iLength = 0;
	char szLength[20] = {0};
	char *pEnd = NULL;
	char *pStart = NULL;
	char *header = response->pack;

	if (header == NULL || memcmp(header, HTTP_HEADER_START_STR, strlen(HTTP_HEADER_START_STR)) != 0)
	{
		return HTTP_ERR_PARAM;
	}

	pStart = strstr(header, HTTP_CONTENT_LENGTH_STR);
	if (pStart == NULL)
	{
		return HTTP_ERR_NEED_RECV;
	}
	pStart += strlen(HTTP_CONTENT_LENGTH_STR);

	pEnd = strstr(pStart, HTTP_CRLF);
	if (pEnd == NULL)
	{
		return HTTP_ERR_NEED_RECV;
	}

	iLength = pEnd - pStart;
	if (iLength > sizeof(szLength) - 1)
	{
		return HTTP_ERR_SIZE;
	}
	memcpy(szLength, pStart, iLength);

	Http_TrimRight(szLength);
	pStart = Http_TrimLeft(szLength);

	response->dataLength = atoi(pStart);

	return HTTP_SUCCESS;
}

static int Http_GetHeaderTrunked(HTTPHANDLE *response)
{
	char *pEnd = NULL;
	char *pStart = NULL;
	char *header = response->pack;

	if (header == NULL || memcmp(header, HTTP_HEADER_START_STR, strlen(HTTP_HEADER_START_STR)) != 0)
	{
		return HTTP_ERR_PARAM;
	}
	
	pStart = strstr(header, HTTP_TRANSFER_ENCODEING_STR);
	if (pStart == NULL)
	{
		return HTTP_ERR_DATA;
	}

	pEnd = strstr(pStart, HTTP_TRUNKED_STR);
	if (pEnd == NULL)
	{
		return HTTP_ERR_DATA;
	}

	response->isTrunked = 1;

	return HTTP_SUCCESS;
}

static int Http_DecodeHeader(HTTPHANDLE *response)
{
	int iRet = 0;

	iRet = Http_GetHeaderLength(response);
	if (iRet < 0)
	{
		HTTP_STRLOG("Http_GetHeaderLength :%d", iRet);
		return iRet;
	}

	iRet = Http_GetStatus(response);
	if (iRet < 0)
	{
		HTTP_STRLOG("Http_GetStatus :%d", iRet);
		return iRet;
	}

	iRet = Http_GetHeaderTrunked(response);
	if (iRet < 0)
	{
		HTTP_STRLOG("Http_GetHeaderTrunked :%d", iRet);
		iRet = Http_GetContentLength(response);
		HTTP_STRLOG("Http_GetContentLength :%d,%d", iRet, response->dataLength);
		return iRet;
	}

	return HTTP_SUCCESS;
}

static int Http_CheckLen(HTTPHANDLE * response)
{
	int        iRet = 0;
	int        packLen = 0;

	if(response->headerLength == 0) {
		iRet = Http_DecodeHeader(response);
		if(iRet < 0){
			HTTP_STRLOG("Http_DecodeHeader :%d", iRet);
			return iRet;
		}
	}

	if (response->isTrunked == 0) {
		packLen = response->headerLength + response->dataLength;
		if(packLen <= response->packLen) {
			return packLen;
		}
		HTTP_STRLOG("response->packLen :%d", response->packLen);
		HTTP_STRLOG("packLen :%d", packLen);
		return HTTP_ERR_NEED_RECV;
	}

	iRet = Http_GetTrunkedLength(response);
	if (iRet < 0) {
		HTTP_STRLOG("Http_GetTrunkedLength :%d", iRet);
		return iRet;
	}
	packLen = response->headerLength + response->trunckedDataLen;
	if (packLen <= response->packLen ) {
		return packLen;
	}
	HTTP_STRLOG("response->packLen :%d", response->packLen);
	HTTP_STRLOG("packLen :%d", packLen);

	return HTTP_ERR_NEED_RECV;
}

int Http_InitHandle(HTTPHANDLE * handle, uint size)
{
	if(handle == NULL) {
		return HTTP_ERR_PARAM;
	}

	memset(handle, 0, sizeof(handle));
	handle->status = 0;
	handle->dataLength = 0;
	handle->headerLength = 0;
	handle->isTrunked = 0;
	handle->trunckedDataLen = 0;
	handle->packLen = 0;
	handle->packSize = size;
	handle->pack = Sys_Malloc(size);
	if(handle->pack == NULL) {
		return HTTP_ERR_SIZE;
	}
	memset(handle->pack, 0, handle->packSize);

	return HTTP_SUCCESS;
}

#define NewLine() strcat((char *)request->pack,"\x0D\x0A")

int Http_SetHeader(HTTPHANDLE * request, const char * key, const char * value)
{
	if(request == NULL || request->pack == NULL) {
		return HTTP_ERR_PARAM;
	}
	
	sprintf((char *)request->pack + request->packLen, "%s: %s", key, value);
	
	NewLine();
	request->packLen = strlen(request->pack);
	
	return HTTP_SUCCESS;
}

int Http_InitRequest(HTTPHANDLE * request, const char * method, const char * host, const char * port, const char * url)
{	
	if(method == NULL || host == NULL || url == NULL) {
		return HTTP_ERR_PARAM;
	}
	sprintf(request->pack, "%s %s HTTP/1.1", method, url);
	NewLine();
	if(atol(port) != 80 && atol(port) != 443) {
		sprintf((char *)request->pack + strlen(request->pack), "HOST: %s:%s", host, port);
	} else {
		sprintf((char *)request->pack + strlen(request->pack), "HOST: %s", host);
	}
	NewLine();
	request->packLen = strlen(request->pack);
	
	return HTTP_SUCCESS;
}

int Http_SetRequestBody(HTTPHANDLE * request, const char * body, uint len)
{
	if(request == NULL || request->pack == NULL) {
		return HTTP_ERR_PARAM;
	}

	if(request->packLen + len > request->packSize - 40) {
		return HTTP_ERR_SIZE;
	}
	
	sprintf((char *)request->pack + strlen(request->pack), "Content-Length: %d", len);
	NewLine();
	NewLine();
	request->packLen = strlen(request->pack);
	
	if(body != NULL && len > 0) {
		memcpy(request->pack + request->packLen, body, len);
		request->packLen += len;
	}
		
	return HTTP_SUCCESS;
}

int Http_TmsConnect(const char * host, const char * port)
{
	int iRet = 0;

	iRet = Comm_ConnectHttp(APP_UI_TIMEOUT, 0, host, port);
	return iRet;
}

int Http_TmsSend(HTTPHANDLE * request)
{
	int iRet;
	int dataLen = 0;

	if(request == NULL || request->pack == NULL || request->packLen <= 0) {
		return HTTP_ERR_PARAM;
	}
	
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, PROMPT_SENDDATA, 0);
	
	while(dataLen != request->packLen) {
		if((request->packLen-dataLen) > 1024) {		
			iRet = LvosCommSend(request->socket, request->pack+dataLen, 1024);
		} else {
			iRet = LvosCommSend(request->socket, request->pack+dataLen, request->packLen-dataLen);
		}
		if(iRet < 0){
			HTTP_STRLOG("LvosCommSend(%d):%d", request->socket, iRet);
			return HTTP_ERR_SEND;
		}
		dataLen += iRet;
	}
	
	return dataLen;
}

int Http_TmsRecv(HTTPHANDLE * response, uint timeMS)
{
	int        iRet = 0;
	int        iLeft = 0;
	uint       start = LvosSysTick();
	
	if(response == NULL || response->pack == NULL || response->packSize <= 0 || timeMS <= 0) {
		return HTTP_ERR_PARAM;
	}
	
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, PROMPT_RECVDATA, 0);
	
	memset(response->pack, 0, response->packSize);
	response->packLen = 0;

	while(1)
	{
		iRet = LvosSysTick() - start;
		if(iRet < timeMS) {				
			if(timeMS > iRet && iLeft != (timeMS -iRet) / 1000) {
				char left[10];
				iLeft = (timeMS -iRet) / 1000;
				sprintf(left, "%02d", iLeft);
				Ui_ClearLine(2, 2);
				Ui_DispTextLineAlign(2, DISPLAY_CENTER, left, 0);
			}
		} else {
			return HTTP_ERR_TIMEOUT;
		}

		iRet = LvosCommRecv(response->socket, 
			response->pack + response->packLen, 
			response->packSize - response->packLen);
		if(iRet < 0) {
			HTTP_STRLOG("LvosCommRecv(%d): %d", response->socket, iRet);
		  	return HTTP_ERR_RECV;
	 	}else if(iRet == 0)	{
			LvosSysDelayMs(100);
			continue;
		}	
		response->packLen += iRet;
		iRet = Http_CheckLen(response);
		if(iRet < 0) {
			HTTP_STRLOG("Http_CheckLen: %d", iRet);
			LvosSysDelayMs(100);
			continue;
		}
		HTTP_STRLOG("response->status: %d", response->status);
		HTTP_STRLOG("response->headerLength: %d", response->headerLength);
		HTTP_STRLOG("response->dataLength: %d", response->dataLength);
		HTTP_STRLOG("response->packLen: %d", response->packLen);
		
		if (response->status != 200 && response->status != 206) {
			HTTP_HEXLOG("response->pack:", response->pack,response->packLen);
			return HTTP_ERR_STATUS;
		}
		
		return response->packLen;
  	}

	return HTTP_ERR_DATA;
}

void Http_Release(HTTPHANDLE * handle)
{
	if(handle->pack != NULL) {
		Sys_Free(handle->pack);
		handle->pack = NULL;
	}		
	Comm_Close();
}

static int Http_GetTrunkedData(HTTPHANDLE *response, char *data, int dataSize)
{
	int iRet = 0;
	int iLength = 0;
	int iOffSet = 0;
	int bodyLen = 0;
	char *body = response->pack + response->headerLength;
	bodyLen = response->packLen - response->headerLength;

	while (1)
	{
		int lenLength = 0;
		iRet = Http_GetTrunkedBlockLength(body + iOffSet, &lenLength);
		if (iRet == 0)
		{
			return HTTP_SUCCESS;
		}
		if (iRet < 0)
		{
			return iRet;
		}

		if (iLength + iRet > dataSize)
		{
			return HTTP_ERR_SIZE;
		}

		memcpy(data + iLength, body + iOffSet + lenLength, iRet);

		iLength += iRet; 

		iOffSet += lenLength; 
		iOffSet += iRet;			
		iOffSet += 2;		
		if (iOffSet >= bodyLen)
		{
			return HTTP_ERR_LEGNTH;
		}
	}

	return HTTP_ERR_DATA;
}


int Http_GetResponseBody(HTTPHANDLE * response, char * data, int dataSize)
{
	int        iRet = 0;

	iRet = Http_CheckLen(response);
	if(iRet < 0) {
		HTTP_STRLOG("Http_CheckLen: %d", iRet);
		return iRet;
	}
	
	HTTP_STRLOG("response->isTrunked: %d", response->isTrunked);
	HTTP_STRLOG("response->headerLength: %d", response->headerLength);
	HTTP_STRLOG("response->dataLength: %d", response->dataLength);
	HTTP_STRLOG("response->packLen: %d", response->packLen);
	
	if (response->isTrunked == 0) {
		if (response->headerLength + response->dataLength > response->packLen) {
			return HTTP_ERR_NEED_RECV;
		}
		if (response->dataLength > dataSize) {
			HTTP_STRLOG("response->dataLength: %d", response->dataLength);
			HTTP_STRLOG("dataSize: %d", dataSize);
			return HTTP_ERR_SIZE;
		}
		
		memcpy(data, response->pack + response->headerLength, response->dataLength);
		
		return response->dataLength;
	} else {
		iRet = Http_GetTrunkedLength(response);
 		if (iRet < 0) {
			HTTP_STRLOG("Http_GetTrunkedLength: %d", iRet);
			return HTTP_ERR_NEED_RECV;
		}

		if (response->dataLength > dataSize) {
			HTTP_STRLOG("response->dataLength: %d", response->dataLength);
			HTTP_STRLOG("dataSize: %d", dataSize);
			return HTTP_ERR_SIZE;
		}

		iRet = Http_GetTrunkedData(response, data, dataSize);
		if (iRet < 0) {
			HTTP_STRLOG("Http_GetTrunkedData: %d", iRet);
			return HTTP_ERR_DATA;
		}
		return response->dataLength;
	}
	
	return HTTP_ERR_DATA;
}

int Http_DecodeResponseBody(HTTPHANDLE * response, void * param, DecodeCB callback)
{
	int iRet = 0;
	int iLength = 0;
	int iOffSet = 0;
	int bodyLen = 0;
	char * body = NULL;
	
	HTTP_STRLOG("response->isTrunked: %d", response->isTrunked);
	HTTP_STRLOG("response->headerLength: %d", response->headerLength);
	HTTP_STRLOG("response->dataLength: %d", response->dataLength);
	HTTP_STRLOG("response->packLen: %d", response->packLen);
	
	if (response->isTrunked == 0) {
		
		iRet = callback(response->pack + response->headerLength, 
			response->dataLength, param);
		if(iRet < 0) {
			return iRet;
		}
		return response->dataLength;
	} 
	
	// trunked mode
	body = response->pack + response->headerLength;
	bodyLen = response->packLen - response->headerLength;
	while (1)
	{
		int writeRet;
		int lenLength = 0;
		iRet = Http_GetTrunkedBlockLength(body + iOffSet, &lenLength);
		if (iRet == 0)
		{
    		HTTP_STRLOG("last pack, total len = %d", iLength);
			return iLength;
		}
		if (iRet < 0)
		{
			return iRet;
		}

		writeRet = callback(body + iOffSet + lenLength, 
			iRet, param);
		if(writeRet < 0) {
			return writeRet;
		}

		iLength += iRet; 

		iOffSet += lenLength; 
		iOffSet += iRet;			
		iOffSet += 2;				
		if (iOffSet >= bodyLen)
		{
			return HTTP_ERR_LEGNTH;
		}
	}

	return HTTP_ERR_DATA;
}



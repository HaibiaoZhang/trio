#ifndef __TRANS_H__
#define __TRANS_H__


typedef struct _DEV_INF_
{
	u32	PrintHeadWidth;
	u32	MaxLines;
	u32	FWSpace;
	u8	MaxSpeed;
	u8	MaxDensity;
	u16	MaxPackageLen;
	u8	Version[32];
	u8	FWHash[32];
	u8	Reserved[8];
}PRT_DEV_INF_T;

typedef struct _DEV_STATUS_
{
	u8	CurTemp;
	u8	CurDensity;
	u8	CurSpeed;
	u8	IsOverHeated;
	u8	IsNoPaper;
	u8	KeyValue;
	u8	Reserved_a[2];
	u32	CurVoltage;
	u32	BufLeftLines;
	u32	ErrorNo;
	u8	Reserved_b[4];
}PRT_DEV_STATUS_T;

typedef struct _CFG_PRINTER_
{
	u8	CfgSpeed;
	u8	CfgDensity;
	u8	Reversed[30];
}PRT_CFG_T;

extern int s_PrtGetDevInfo(PRT_DEV_INF_T *dev_info);
extern int s_PrtGetDevStatus(PRT_DEV_STATUS_T *dev_st);
extern int s_PrtConfigure(PRT_CFG_T *dev_cfg);
extern int s_PrtImage(unsigned char *image, u32 len_in_bytes);


int Trans_MagcardTest(void * pxPARAM);
int Trans_IcccardTest(void * pxPARAM);
int Trans_PicccardTest(void * pxPARAM);
int Trans_ScanTest(void * pxPARAM);
int Trans_MkskKeyTest(void * pxPARAM);
int Trans_DukptKeyTest(void * pxPARAM);
int Trans_LcdPictureshow(void * pxPARAM);		
int Trans_LcdCodeshow(void * pxPARAM);
int Trans_LcdNormalshow(void * pxPARAM);	
int Trans_LcdPictureshow(void * pxPARAM);		
int Trans_LcdLightTest(void * pxPARAM);
int Trans_LcdScreenshow(void * pxPARAM);		
int Trans_GetServerTime(void * pxPARAM);
int Trans_USBTest(void * pxPARAM);
int Trans_BtTest(void * pxPARAM);
int Trans_PrinterTest(void * pxPARAM);
int Trans_BeepTest(void * pxPARAM);
int Trans_LedTest(void * pxPARAM);
int Trans_KeyboardTest(void * pxPARAM);
int Trans_FilesystemTest(void * pxPARAM);
int Trans_SetSleep(void * pxPARAM);
int Trans_Powerdown(void * pxPARAM);
int Trans_Reboot(void * pxPARAM);
int Trans_RandomTest(void * pxPARAM);
int Trans_DelayTest(void * pxPARAM);
int Trans_TimerTest(void * pxPARAM);
int Trans_SysPrmReadTest(void * pxPARAM);
int Trans_ESignTest(void * pxPARAM);	
int Trans_GetCardInfo();

#endif


#include <libapp.h>
#include <cJSON.h>
#include "app.h"
#include "func.h"
#include "param.h"
#include "trans.h"
#include "comm.h"
#include "emvproc.h"
#include "tms.h"

#ifdef APP_DEBUG
#define APP_STRLOG DBG_STR
#define APP_HEXLOG DBG_HEX
#else
#define APP_STRLOG  
#define APP_HEXLOG 
#endif

static ST_TRANLOG m_translog;

#define TMP_SIZE 1024
static uint m_DispLast = 0;
static uint m_idleTime = 0;	// 用于休眠控制
extern int TOUCH_Adjust(void);

ST_TRANLOG * App_GetTranLog()
{
	return &m_translog;	
}

void App_PrintHex(byte *data, uint len)
{
    uint i;
    char hex[80];
    char disp[20];
	
	memset(hex, 0, sizeof(hex));
	memset(disp, 0, sizeof(disp));
    for(i=0; i<len; ){
		sprintf(hex + strlen(hex), "%02X ", data[i]);
		disp[i%16] = data[i];
		if(data[i] < 0x20 || data[i] > 0x7E ) {
			disp[i%16] = '.';
		}
        i++;
        if(!(i%16)) {
			strcat(hex, " : ");
			strcat(hex, disp);
			strcat(hex, "\r\n");
            usb_debug(hex);
            LvosSysDelayMs(100);
			memset(hex, 0, sizeof(hex));
			memset(disp, 0, sizeof(disp));
        }
    }
	strcat(hex, " : ");
	strcat(hex, disp);
	strcat(hex, "\r\n");
    usb_debug(hex);
    LvosSysDelayMs(100);
}

int  App_NotSupport(void * pxPARAM)
{
	Ui_Clear();
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, "no surport!", 0);
	Sys_BeepFAIL();
	Ui_WaitKey(APP_PROMPT_TIMEOUT);
	
	return APP_RET_NO_DISP;
}

int App_GetMagHandle()
{
	static int m_MagHandle = -1;
	if(m_MagHandle  < 0) {
		m_MagHandle = Sys_GetHandle("MSR");
	}
	//APP_STRLOG("m_MagHandle=%d\r\n", m_MagHandle);
	return m_MagHandle;
}

int App_GetIccHandle()
{
	static int m_IccHandle = -1;
	if(m_IccHandle  < 0) {
		m_IccHandle = Sys_GetHandle("SCC");
	}
	//APP_STRLOG("m_IccHandle=%d\r\n", m_IccHandle);
	return m_IccHandle;
}

int App_GetPiccHandle()
{
	static int m_PiccHandle = -1;
	if(m_PiccHandle  < 0) {
		m_PiccHandle = Sys_GetHandle("SCCL");
	}
	//APP_STRLOG("m_PiccHandle=%d\r\n", m_PiccHandle);
	return m_PiccHandle;
}

int App_GetIoHandle()
{
	static int m_IoHandle = -1;
	if(m_IoHandle  < 0) {
		m_IoHandle = Sys_GetHandle("IOCTL");
	}
	//APP_STRLOG("m_IoHandle=%d\r\n", m_IoHandle);
	return m_IoHandle;
}

int App_GetUsbHandle()
{
	static int m_UsbHandle = -1;
	if(m_UsbHandle  < 0) {
		m_UsbHandle = Sys_GetHandle("USB");
	}
	//APP_STRLOG("m_UsbHandle=%d\r\n", m_UsbHandle);
	return m_UsbHandle;
}

ST_PARAM * App_GetTermParam()
{
	return Param_GetTermParam();
}


uint App_GetKeyExpLen()
{
	return 2;
}

uint App_GetKeyModLen()
{
	return 1024;
}

int App_ReadTUSN(char * sn)
{
	char * text;
	cJSON * pxJSON = NULL;
	cJSON * pxItem = NULL;
	
	text = Sys_Malloc(TMP_SIZE);
	if(text == NULL) {
		return APP_RET_FAIL;
	}
	
	memset(text, 0, TMP_SIZE);
	Sys_ReadCfg("TUSN", text, TMP_SIZE);
	APP_STRLOG("TUSN:\r\n%s", text);
	pxJSON = cJSON_Parse(text);
	if(pxJSON == NULL) {
		Sys_Free(text);
		return APP_RET_FAIL;
	}
	pxItem = cJSON_GetObjectItem(pxJSON, "terminal_sn");
	if(pxItem == NULL || pxItem->type != cJSON_String || pxItem->valuestring == NULL) {
		cJSON_Delete(pxJSON);
		Sys_Free(text );
		return APP_RET_FAIL;
	}
	
	strcpy(sn, pxItem->valuestring);
	cJSON_Delete(pxJSON);
	Sys_Free(text );
	return strlen(sn);
}

int App_ReadSN(char * sn)
{
    static char buffer[30]={0};

	if(strlen(buffer)>0){
		strcpy(sn,buffer);
		return strlen(sn);
	}
	memset(buffer, 0, sizeof(buffer));
	Sys_ReadSN(buffer);
	strcpy(sn,buffer);
	return strlen(sn);
}

int App_ReadMCC(byte * data)
{
	// 中国
	strcpy(data, "460");
	return strlen(data);
}

int App_ReadMNC(byte * data)
{
	strcpy(data, "00"); 
	return strlen(data);
}

int App_GetLcdType()
{
	byte val[40];
	
	memset(val, 0, sizeof(val));
	Sys_ReadCfg("LCD_RES", val, sizeof(val) - 1);
	
	APP_STRLOG("LCD_RES=%s",val);
	if( !strcmp(val, "128X96") ) {
		return 1;
	}
	
	if( !strcmp(val, "128X64") ) {
		return 3;
	}
	
	return 0;
}

int App_IsLTE()
{
	static int m_isLTE = -1;
	
	if(m_isLTE < 0) {
   		char pn[30];
		memset(pn, 0, sizeof(pn));
		Sys_ReadPN(pn);
		if(pn[0] != 'L') {
			m_isLTE = 0;
		} else {
			m_isLTE = 1;
		}
	}
	
	return m_isLTE;
}

int App_HasWifi()
{
	static int m_hasWifi = -1;
	
	if(m_hasWifi < 0) {
   		char pn[30];
		memset(pn, 0, sizeof(pn));
		Sys_ReadPN(pn);
		if(pn[1] != 'W') {
			m_hasWifi = 0;
		} else {
			m_hasWifi = 1;
		}
	}
	
	return m_hasWifi;
}

int App_HasCamera()
{
	static int m_hasCamera = -1;
	
	if(m_hasCamera < 0) {
   		char pn[30];
		memset(pn, 0, sizeof(pn));
		Sys_ReadPN(pn);
		if(pn[2] != 'C') {
			m_hasCamera = 0;
		} else {
			m_hasCamera = 1;
		}
	}
	
	return m_hasCamera;
}

int App_IsBlack()
{
	static int m_isBlack= -1;
	
	if(m_isBlack < 0) {
   		char pn[30];
		memset(pn, 0, sizeof(pn));
		Sys_ReadPN(pn);
		if(pn[3] != 'B') {
			m_isBlack = 0;
		} else {
			m_isBlack = 1;
		}
	}
	
	return m_isBlack;
}

void App_SetDevParam()
{
	int iRet;
	char tmp[20];
	ST_PARAM *   pxTERM = App_GetTermParam();

	if(pxTERM->keyTone) {
		iRet = Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "ENABLE");
		APP_STRLOG("Sys_WriteParam(KBTONE, ENABLE) = %d", iRet);
	} else {
		iRet = Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "DISABLE");
		APP_STRLOG("Sys_WriteParam(KBTONE, DISABLE) = %d", iRet);
	}

	sprintf(tmp, "%d", pxTERM->devSleep);
	iRet = Sys_WriteParam(HWND_APP, "SLEEP_DEV_TIME", tmp);
	APP_STRLOG("Sys_WriteParam(SLEEP_DEV_TIME, %s) = %d", tmp, iRet);
}

void App_Sleep(uint isFresh)
{
	int iRet;
	uint now = LvosSysTick();
	ST_PARAM *   pxTERM = App_GetTermParam();

	if( Sys_IsCharging() ) {
		return;
	}

	if(isFresh || m_idleTime == 0) {
		m_idleTime = now;
		APP_STRLOG("m_idleTime = %d", m_idleTime);
	}

	if(now - m_idleTime > pxTERM->lcdSleep) {
		APP_STRLOG("%d, %d, %d", now, m_idleTime, pxTERM->lcdSleep);
		iRet = Sys_WriteParam(HWND_APP, "SLEEP_ENABLE", "ENABLE");
		APP_STRLOG("Sys_WriteParam(SLEEP_ENABLE, ENABLE) = %d", iRet);
		m_idleTime = 0;
	}
}

int App_ViewDevInfo(void * pxPARAM)
{
	byte line = 1;
	char tmp[1024];
	
	Ui_Clear();
    Ui_ClearKey();
	
	memset(tmp, 0, sizeof(tmp));
	
	strcat(tmp+strlen(tmp), "sys ver: ");
	Sys_ReadOSVer(tmp + strlen(tmp));
	strcat(tmp+strlen(tmp), "\n");
	
	sprintf(tmp+strlen(tmp), "app name: %s\n", APP_NAME);
	sprintf(tmp+strlen(tmp), "app ver: %s_%s\n", APP_VER, APP_CODE);
	sprintf(tmp+strlen(tmp), "build date: %s\n", __DATE__);
	sprintf(tmp+strlen(tmp), "build time: %s\n", __TIME__);
	
	sprintf(tmp+strlen(tmp), "TUSN: ");
	App_ReadTUSN(tmp + strlen(tmp));
	strcat(tmp+strlen(tmp), "\n");
	
	sprintf(tmp+strlen(tmp), "SN: ");
	Sys_ReadSN(tmp + strlen(tmp));
	strcat(tmp+strlen(tmp), "\n");

	sprintf(tmp+strlen(tmp), "SIM: ");
	Sys_ReadSIMID(tmp + strlen(tmp));
	strcat(tmp+strlen(tmp), "\n");
	
	Func_DispInfo("DEVICE INFO", tmp, APP_UI_TIMEOUT);
	
	return 0;
}

void App_InitTran()
{
	SYS_TIME     time;
	ST_TRANLOG * pxLOG = App_GetTranLog();
	
	memset((char *)pxLOG, 0, sizeof(ST_TRANLOG));
	pxLOG->type = 1;
	Sys_GetTime((char *)pxLOG->date, (char *)pxLOG->time);	
	strcpy(pxLOG->cardUnit, "CUP");
}

int APP_Ajust(void * pxPARAM)
{
	int iRet;
#if !defined(TOUCHLCD)
	return 0;
#endif
	iRet = TOUCH_Adjust();

	Ui_Clear();
	Ui_ClearLineColor(0, DISPLAY_FONT_COLOR);
	Ui_DispTextLineAlign(0, DISPLAY_CENTER, "screen adjust", 1);
	if(iRet) {
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, "adjust fail", 0);
		Sys_BeepFAIL();
	} else {
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, "adjust succ", 0);
		Sys_BeepOK();
	}
	Ui_WaitKey(APP_UI_TIMEOUT);
	
	return 0;
}


int App_CardMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30];
	ST_MENUITEM menu[4];

#ifdef MAGTYPE
	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,PROMPT_MAGCARD);
	menu[i].text = tmp1;
	menu[i].func= Trans_MagcardTest;
	menu[i].param= (void *)NULL;
	i++;
#endif
#ifdef ICCTYPE
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1';		
	sprintf(tmp2,"%d.%s",i+1,PROMPT_CHIPCARD);
	menu[i].text = tmp2;
	menu[i].func= Trans_IcccardTest;
	menu[i].param= (void *)NULL;
	i++;
#endif
#ifdef PICCTYPE
	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,PROMPT_TAPCARD);
	menu[i].text = tmp3;
	menu[i].func= Trans_PicccardTest;
	menu[i].param= (void *)NULL;
	i++;
#endif
	Func_Menu(PROMPT_CARDTEST, menu, i, APP_UI_TIMEOUT);
  	return APP_RET_NO_DISP;
}


int App_LcdMenu(void * pxPARAM)
{
	const ST_MENUITEM menu[] = {
		{KB1, "1.disp normal",     Trans_LcdNormalshow, (void *)NULL},
		{KB2, "2.disp code",       Trans_LcdCodeshow, (void *)NULL},
		{KB3, "3.disp picture",         Trans_LcdPictureshow, (void *)NULL},
		{KB4, "4.full screen",         Trans_LcdScreenshow, (void *)NULL},
		{KB5, "5.back light", 	    Trans_LcdLightTest, (void *)NULL},
	};
	
	Func_Menu(PROMPT_LCDTEST, menu, sizeof(menu)/sizeof(ST_MENUITEM),    APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

int App_CommMenu(void * pxPARAM)
{
	const ST_MENUITEM menu[] = {
		{KB1, "1.GPRS/WIFI",    Trans_GetServerTime, (void *)NULL},
		{KB2, "2.BT",         	Trans_BtTest, (void *)NULL},
		{KB3, "3.USB",          Trans_USBTest, (void *)Sys_GetHandle("USB")},
#if defined(UARTCOM)		
		{KB4, "4.UART", 	    Trans_USBTest, (void *)Sys_GetHandle("UART")},
#endif		
	};
	
	Func_Menu("COMM TEST", menu, sizeof(menu)/sizeof(ST_MENUITEM),    APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

int App_KeyMenu(void * pxPARAM)
{
	const ST_MENUITEM menu[] = {
		{KB1, "1.MKSK",         Trans_MkskKeyTest, (void *)NULL},
		{KB2, "2.DUKPT",        Trans_DukptKeyTest, (void *)NULL},
	};
	
	Func_Menu(PROMPT_KEYTEST, menu, sizeof(menu)/sizeof(ST_MENUITEM),    APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

int App_EMVParamMenu(void * pxPARAM)
{
	const ST_MENUITEM menu[] = {
		{KB1, "1.load default prm",    EMV_InitDefault, (void *)1},
		{KB2, "2.load AID",         EMV_InitAidList, (void *)1},
		{KB3, "3.load CAPK",		   EMV_InitCAPK, (void *)1},
	};
	
	Func_Menu("EMVPARAM", menu, sizeof(menu)/sizeof(ST_MENUITEM),    APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

int App_PowerMenu(void * pxPARAM)
{
	const ST_MENUITEM menu[] = {
		{KB1, "1.power off",         Trans_Powerdown, (void *)NULL},
		{KB2, "2.reboot",         Trans_Reboot, (void *)NULL},
		{KB3, "3.sleep awake",		Trans_SetSleep, (void *)60000},
	};
	
	Func_Menu("POWER MANAGE", menu, sizeof(menu)/sizeof(ST_MENUITEM),    APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

int App_SysPrmMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30];
	ST_MENUITEM menu[7];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,PROMPT_RCFG);
	menu[i].text = tmp1;
	menu[i].func= Trans_SysPrmReadTest;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1'; 	
	sprintf(tmp2,"%d.%s",i+1,PROMPT_TIMERTEST);
	menu[i].text = tmp2;
	menu[i].func= Trans_TimerTest;
	menu[i].param= (void *)NULL;
	i++;	
	
	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,PROMPT_DELAYTEST);
	menu[i].text = tmp3;
	menu[i].func= Trans_DelayTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,PROMPT_RANDOMTEST);
	menu[i].text = tmp4;
	menu[i].func= Trans_RandomTest;
	menu[i].param= (void *)NULL;
	i++;

	Func_Menu(PROMPT_SYSPRMTEST, menu, i, APP_UI_TIMEOUT);	
	return APP_RET_NO_DISP;
}

int App_TmsDown(void * pxPARAM)
{
	int iRet;
	int flag = (int)pxPARAM;
	char ver[32+1];
	char * task;
		
	Ui_Clear();
	Ui_DispTitle("TMS UPADATE");
	
	if( !Sys_IsCharging() && Sys_ReadBAT() == 0) {
		Ui_ClearLine(1, Ui_GetLineNum()-2);
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, "low battery,pls charg", 0);
		Ui_WaitKey(APP_PROMPT_TIMEOUT);
		return APP_RET_OK;
	}
	
	Tms_ClearUpdate();
	iRet = Tms_Update("SHOUFUBEI", "302001", "1");
	if(iRet != 0) {
		return APP_RET_FAIL;	
	}
	return APP_RET_OK;	
}


int App_OtherMenu(void * pxPARAM)
{	
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30],tmp5[30],tmp6[30],tmp7[30],tmp8[30];
	ST_MENUITEM menu[8];

	/*memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1'; 	
	sprintf(tmp1,"%d.%s",i+1,"TMS update");
	menu[i].text = tmp1;
	menu[i].func= App_TmsDown;
	menu[i].param= (void *)NULL;
	i++;*/
	
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1'; 	
	sprintf(tmp2,"%d.%s",i+1,PROMPT_BEEPERTEST);
	menu[i].text = tmp2;
	menu[i].func= Trans_BeepTest;
	menu[i].param= (void *)NULL;
	i++;	

	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,PROMPT_FILESYS);
	menu[i].text = tmp3;
	menu[i].func= Trans_FilesystemTest;
	menu[i].param= (void *)NULL;
	i++;

#ifdef LEDTEST
	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,PROMPT_LEDTEST);
	menu[i].text = tmp4;
	menu[i].func= Trans_LedTest;
	menu[i].param= (void *)NULL;
	i++;
#endif
	memset(tmp5,0,sizeof(tmp5));
	menu[i].kb = i+'1';
	sprintf(tmp5,"%d.%s",i+1,PROMPT_POWERMNG);
	menu[i].text = tmp5;
	menu[i].func= App_PowerMenu;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp6,0,sizeof(tmp6));
	menu[i].kb = i+'1';
	sprintf(tmp6,"%d.%s",i+1,PROMPT_SYSPRMTEST);
	menu[i].text = tmp6;
	menu[i].func= App_SysPrmMenu;
	menu[i].param= (void *)NULL;
	i++;
#ifdef KEYBOARD	
	memset(tmp7,0,sizeof(tmp7));
	menu[i].kb = i+'1';
	sprintf(tmp7,"%d.%s",i+1,PROMPT_KEYBOARDTEST);
	menu[i].text = tmp7;
	menu[i].func= Trans_KeyboardTest;
	menu[i].param= (void *)NULL;
	i++;
#endif	

#if defined(ICCTYPE) || defined(PICCTYPE)
	memset(tmp8,0,sizeof(tmp8));
	menu[i].kb = i+'1';
	sprintf(tmp8,"%d.%s",i+1,PROMPT_EMVPRM);
	menu[i].text = tmp8;
	menu[i].func= App_EMVParamMenu;
	menu[i].param= (void *)NULL;
	i++;
#endif

	Func_Menu(PROMPT_OTHERCASE, menu, i, APP_UI_TIMEOUT);	
	return APP_RET_NO_DISP;
}

int App_TouchMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30];
	ST_MENUITEM menu[4];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,PROMPT_SCORRECT);
	menu[i].text = tmp1;
	menu[i].func= APP_Ajust;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1';
	sprintf(tmp2,"%d.%s",i+1,PROMPT_ELECSIGN);
	menu[i].text = tmp2;
	menu[i].func= Trans_ESignTest;
	menu[i].param= (void *)NULL;
	i++;

	Func_Menu(PROMPT_TOUCHTEST, menu, i, APP_UI_TIMEOUT);
  	return APP_RET_NO_DISP;
}


int App_SysMenu(void * pxPARAM)
{	
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30],tmp5[30],tmp6[30],tmp7[30];
	ST_MENUITEM menu[7];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,PROMPT_DEVINFO);
	menu[i].text = tmp1;
	menu[i].func= App_ViewDevInfo;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1'; 	
	sprintf(tmp2,"%d.%s",i+1,PROMPT_COMMPRM);
	menu[i].text = tmp2;
	menu[i].func= Param_Prmset;
	menu[i].param= (void *)NULL;
	i++;	

	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,PROMPT_TIME);
	menu[i].text = tmp4;
	menu[i].func= Param_SetTime;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp5,0,sizeof(tmp5));
	menu[i].kb = i+'1';
	sprintf(tmp5,"%d.%s",i+1,PROMPT_RESET);
	menu[i].text = tmp5;
	menu[i].func= Param_Reset;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp6,0,sizeof(tmp6));
	menu[i].kb = i+'1';
	sprintf(tmp6,"%d.%s",i+1,PROMPT_KEYTEST);
	menu[i].text = tmp6;
	menu[i].func= App_KeyMenu;
	menu[i].param= (void *)NULL;
	i++;
	
	Func_Menu(PROMPT_PRMSET, menu, i, APP_UI_TIMEOUT);	
  	return APP_RET_NO_DISP;
}

int App_TransMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30],tmp5[30],tmp6[30],tmp7[30],tmp8[30];
	ST_MENUITEM menu[8];

#if defined(MAGTYPE) ||defined(ICCTYPE) || defined(PICCTYPE)
	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,PROMPT_CARDTEST);
	menu[i].text = tmp1;
	menu[i].func= App_CardMenu;
	menu[i].param= (void *)NULL;
	i++;
#endif
#ifdef CAMERA
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1'; 	
	sprintf(tmp2,"%d.%s",i+1,PROMPT_SCANTEST);
	menu[i].text = tmp2;
	menu[i].func= Trans_ScanTest;
	menu[i].param= (void *)NULL;
	i++;
#endif
#ifdef LCDSHOW
	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,PROMPT_LCDTEST);
	menu[i].text = tmp3;
	menu[i].func= App_LcdMenu;
	menu[i].param= (void *)NULL;
	i++;
#endif
#if defined(GPRSCOM) ||defined(WIFICOM) || defined(USBCOM) || defined(UARTCOM)
	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,PROMPT_COMMTEST);
	menu[i].text = tmp4;
	menu[i].func= App_CommMenu;
	menu[i].param= (void *)NULL;
	i++;
#endif
#ifdef PRINTER
	memset(tmp5,0,sizeof(tmp5));
	menu[i].kb = i+'1';
	sprintf(tmp5,"%d.%s",i+1,PROMPT_PRINTERTEST);
	menu[i].text = tmp5;
	menu[i].func= Trans_PrinterTest;
	menu[i].param= (void *)NULL;
	i++;
#endif

#ifdef TOUCHLCD
	memset(tmp6,0,sizeof(tmp6));
	menu[i].kb = i+'1';
	sprintf(tmp6,"%d.%s",i+1,PROMPT_TOUCHTEST);
	menu[i].text = tmp6;
	menu[i].func= App_TouchMenu;
	menu[i].param= (void *)NULL;
	i++;
#endif

	memset(tmp7,0,sizeof(tmp7));
	menu[i].kb = i+'1';
	sprintf(tmp7,"%d.%s",i+1,PROMPT_OTHERCASE);
	menu[i].text = tmp7;
	menu[i].func= App_OtherMenu;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp8,0,sizeof(tmp8));
	menu[i].kb = i+'1';
	sprintf(tmp8,"%d.%s",i+1,PROMPT_PRMSET);
	menu[i].text = tmp8;
	menu[i].func= App_SysMenu;
	menu[i].param= (void *)NULL;
	i++;

	Func_Menu(PROMPT_MAINMENU, menu, i, APP_UI_TIMEOUT);
	return APP_RET_NO_DISP;
}

void App_InitMem()
{
    cJSON_Hooks hook;
	
	Sys_MallocInit(NULL, 0);
    hook.malloc_fn = (void *)Sys_Malloc;
    hook.free_fn = Sys_Free;
    cJSON_InitHooks(&hook);
}


int App_Initbmplogo()
{
	int i=0,left;
	
    Ui_Clear();	
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, "loading logo.bmp...", 0);
	File_Remove(LOGO_FILE);
	APP_HEXLOG("logo.bmp data:",(byte *)(APP_BASE_ADDR+LOGOBMP_ADDR),20);
	while(i < 86454 ){
		left = 86454-i;
		
		if(left > 8192){
			File_Append(LOGO_FILE,(byte *)(APP_BASE_ADDR+LOGOBMP_ADDR)+i,8192);
			i += 8192;
		}else{
			File_Append(LOGO_FILE,(byte *)(APP_BASE_ADDR+LOGOBMP_ADDR)+i,left);
			i +=left;
		}
	}
	return 0;
}

extern void CreateFileSys(void);
void app_main(void)
{
	int       iRet;
	APP_STRLOG("into app_main");
	
	App_InitMem();
	switch(App_GetLcdType()) {
		case 1://128X96
			Ui_SetLcdType(1);
			break;
		
		case 3://128X64
			Ui_SetLcdType(3);
			break;
		
		default:
			Ui_SetLcdType(0);
			break;
	}
	//load bmp file
	//App_Initbmplogo();
	
	iRet = Param_InitParam();
	if(iRet != APP_RET_OK) {
		APP_STRLOG("Param_InitParam = %d", iRet);
		goto APP_EXIT;
	}
	
#if defined(GPRSCOM) || defined(WIFICOM)
	Comm_PreInit(1000);
#endif	

#if defined(ICCTYPE) || defined(PICCTYPE)
	EMV_InitDefault((void *)0);
	EMV_InitCAPK((void *)0);
	EMV_InitAidList((void *)0);
#endif

	Ui_Clear();
	Ui_ClearKey();
	LvosUiSetAttr(0, 4, 0);
		
	App_Sleep(1);
	while(1)
	{
#if defined(GPRSCOM) || defined(WIFICOM)
		Comm_UpdateSignal(0);
#endif
		iRet = App_TransMenu((void *)NULL);
		if(iRet != 0) {
			App_Sleep(1);
			goto UI_CLEAN;
		} else {
			App_Sleep(0);
			continue;
		}
	
UI_CLEAN:
		Ui_Clear();
		Ui_ClearKey();
	}
	
APP_EXIT:
	while(1)
	{
		Ui_Clear();
		iRet = Ui_Dailog("system exception", "reboot?", 1, APP_UI_TIMEOUT);
		if(iRet == KB_ENTER) {
			break;
		}
	}
	APP_STRLOG("APP Exist");
	return;
}


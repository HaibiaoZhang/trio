#include <libapp.h>
#include <cJSON.h>

#include "app.h"
#include "param.h"
#include "comm.h"

#ifdef PARAM_DEBUG
#define PARAM_STRLOG DBG_STR
#define PARAM_HEXLOG DBG_HEX
#else
#define PARAM_STRLOG  
#define PARAM_HEXLOG 
#endif
static ST_PARAM   m_TermParam;

#define TEST_HOST_IP         "183.60.22.146"
#define TEST_HOST_PORT       "10020"

static const ST_PARAMDEF m_ParamDefs[] = {
	{0x9F01, T_STR, 8, m_TermParam.termId},
	{0x9F02, T_STR, 15, m_TermParam.merId},
	{0x9F03, T_STR, 40, m_TermParam.merName},
	{0x9F05, T_STR, 2, m_TermParam.keyIndex},
	{0x9F09, T_STR, 100, m_TermParam.hostIP[0]},
	{0x9F10, T_STR, 100, m_TermParam.hostIP[1]},
	{0x9F11, T_STR, 5, m_TermParam.hostPort[0]},
	{0x9F12, T_STR, 5, m_TermParam.hostPort[1]},
	{0x9F13, T_STR, 10, m_TermParam.TPDU},
	{0x9F14, T_BYTE, 1, &m_TermParam.commRetry},
	{0x9F15, T_BYTE, 1, &m_TermParam.commTimeout},
	{0x9F20, T_STR, 8, m_TermParam.sysPwd},
	{0x9F30, T_BYTE, 1, &m_TermParam.keyTone},
	{0x9F31, T_INT, 4, (byte *)&m_TermParam.commMode},
	{0x9F32, T_STR, 40, m_TermParam.gprs.APN},
	{0x9F33, T_STR, 32, m_TermParam.gprs.user},
	{0x9F34, T_STR, 32, m_TermParam.gprs.pwd},
	{0x9F35, T_BYTE, 1, &m_TermParam.gprs.shortPPP},
	{0x9F36, T_STR, 50, m_TermParam.wifi.apName},
	{0x9F37, T_STR, 50, m_TermParam.wifi.apKey},
	{0x9F38, T_STR, 32, m_TermParam.wifi.apMode},
	{0x9F39, T_BYTE, 1, &m_TermParam.wifi.dhcp},
	{0x9F40, T_INT, 4, (byte *)&m_TermParam.lcdSleep},
	{0x9F41, T_INT, 4, (byte *)&m_TermParam.devSleep},
	{0x9F42, T_BYTE, 1, &m_TermParam.hostType},
	{0x9F43, T_BYTE, 1, &m_TermParam.isEmvParamDown},
	{0x9F44, T_BYTE, 1, &m_TermParam.isEmvCaDown},
	{0x9F45, T_STR, 2, (byte *)&m_TermParam.printerdesity},
	{0x9F46, T_STR, 2, (byte *)&m_TermParam.printerline},
};

static int Param_isSpace(int c) /*  是空否  */
{
    switch (c)
    {
        case 0x20:
        case 0xD:
        case 0xA:
        case 0x9:
            return 1;
    }
    
    return 0;
}

static char * Param_DelSpace(char *xbuf) 
{
    char *p;
    for (p = xbuf; Param_isSpace(*p); p++);
    return (p);
}


static void Param_RemoveFiles()
{
	File_Remove(TERM_PARAM_FILE);
	File_Remove(AID_PARAM_FILE);
	File_Remove(CAPK_PARAM_FILE);
}

ST_PARAM * Param_GetTermParam()
{
	return &m_TermParam;
}

static int Param_GetParams(TLVBuf * tlv, const ST_PARAMDEF * defs, int num)
{
	int iRet;
	int idx;
	
	for(idx = 0; idx < num; idx ++) {
		switch(defs[idx].type)
		{
		case T_BIN:
			iRet = Tlv_GetTag(tlv, defs[idx].tag, defs[idx].data, defs[idx].len);
			if(iRet < 0 || (iRet != defs[idx].len && iRet != 0)) {
				return APP_RET_FAIL;
			}
			break;
		case T_STR:
			iRet = Tlv_GetTag(tlv, defs[idx].tag, defs[idx].data, defs[idx].len);
			if(iRet < 0) {
				return APP_RET_FAIL;
			}
			break;
		case T_BYTE:
			iRet = Tlv_GetTag(tlv, defs[idx].tag, defs[idx].data, 1);
			if(iRet < 0 || (iRet != 1 && iRet != 0)) {
				return APP_RET_FAIL;
			}
			break;
		case T_SHORT:
			iRet = Tlv_GetTag(tlv, defs[idx].tag, defs[idx].data, 2);
			if(iRet < 0 || (iRet != 2 && iRet != 0)) {
				return APP_RET_FAIL;
			}
			break;
		case T_INT:
			iRet = Tlv_GetTag(tlv, defs[idx].tag, defs[idx].data, 4);
			if(iRet < 0 || (iRet != 4 && iRet != 0)) {
				return APP_RET_FAIL;
			}
			break;
		default:
			return APP_RET_PARAM;
		}
	}

	return APP_RET_OK;
}

int Param_ReadParam()
{
	int iRet;
	int num;
	int idx;
	TLVBuf tlv;
	
	iRet = File_Size(TERM_PARAM_FILE);
	if(iRet <= 0) {
		return 0;
	}
	tlv.mlen = MAX_PARAM_SIZE;
	tlv.len = (ushort)iRet;
	tlv.buf = Sys_Malloc(tlv.mlen);
	if(tlv.buf == NULL) {
		return APP_RET_ERR_MEM;
	}
	
	memset(tlv.buf, 0, MAX_PARAM_SIZE);
	iRet = File_Read(TERM_PARAM_FILE, 0, tlv.buf, tlv.len);
	if(iRet != tlv.len) {
		Sys_Free(tlv.buf);
		return APP_RET_ERR_MEM;
	}
	iRet = Param_GetParams(&tlv, &m_ParamDefs[0], sizeof(m_ParamDefs) / sizeof(ST_PARAMDEF));
	Sys_Free(tlv.buf);

	if(iRet != APP_RET_OK) {
		return APP_RET_ERR_MEM;
	}

	return 1;
}

int Param_InitParam()
{
	int iRet;
	ST_PARAM * pxPARAM = Param_GetTermParam();
	
	memset((char *)pxPARAM, 0, sizeof(ST_PARAM));
	iRet = Param_ReadParam();
	if(iRet < 0) {
		goto INIT;
	}
	if(iRet > 0) {
		return APP_RET_OK;
	}
	
INIT:
	File_Remove(TERM_PARAM_FILE);

	strcpy(pxPARAM->merId, DEFAULT_MERID);
	strcpy(pxPARAM->termId, DEFAULT_TERMID);
	strcpy(pxPARAM->hostIP[0], TEST_HOST_IP);
	strcpy(pxPARAM->hostPort[0], TEST_HOST_PORT);
	strcpy(pxPARAM->hostIP[1], TEST_HOST_IP);
	strcpy(pxPARAM->hostPort[1], TEST_HOST_PORT);
	strcpy(pxPARAM->TPDU, "6000050000");
	
	pxPARAM->hostType = 1;
	strcpy(pxPARAM->keyIndex, "01");
	pxPARAM->commRetry = 2;
	pxPARAM->keyTone = 1;
#if defined(GPRSCOM)	
	pxPARAM->commMode = COMM_GPRS;
#else
	pxPARAM->commMode = COMM_USB;
#endif
	pxPARAM->wifi.dhcp = 1;
	strcpy(pxPARAM->wifi.apMode, "PSK");

	pxPARAM->lcdSleep = 60000; // 1 m
	pxPARAM->devSleep = 180000; // 3 m
	pxPARAM->isEmvParamDown =0;
	pxPARAM->isEmvCaDown =0;
	
	iRet = Param_WriteParam();
	PARAM_STRLOG("init terminal params = %d", iRet);
	if(iRet != APP_RET_OK) {
		return iRet;
	}

	return APP_RET_OK;
}

int Param_SetParam(const char *dispTitle, const char * dispText1, const char  * dispText2, int curValue, uint timeMS)
{
	int line =1;
	char disp[64];
	
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	
	if(curValue == 1) {
		sprintf(disp, "%s: %s", PROMPT_CURRENT,dispText1);
	} else {
		sprintf(disp, "%s: %s", PROMPT_CURRENT,dispText2);
	}
	Ui_DispTextLineAlign(line++, DISPLAY_RIGHT, disp, 0);
	
	if(dispTitle != NULL && strlen(dispTitle))
	{
		Ui_DispTextLineAlign(line++, DISPLAY_LEFT, dispTitle, 0);
	}
	if(Ui_GetLcdHeight() == 64) {
		memset(disp, 0, sizeof(disp));
		memset(disp, ' ', Ui_GetLineEnNum());
		memcpy(disp, "1.", 2);
		memcpy(disp + 2, dispText1, strlen(dispText1));
		memcpy(disp + Ui_GetLineEnNum() / 2, "0.", 2);
		memcpy(disp + Ui_GetLineEnNum() / 2 + 2, dispText2, strlen(dispText2));
		Ui_DispTextLineAlign(line++, DISPLAY_LEFT, disp, 0);
	} else {
		sprintf(disp, "1.%s", dispText1);
		Ui_DispTextLineAlign(line++, DISPLAY_LEFT, disp, 0);
		sprintf(disp, "0.%s", dispText2);
		Ui_DispTextLineAlign(line++, DISPLAY_LEFT, disp, 0);
	}
	
	while(1) {
		byte key = Ui_WaitKey(timeMS);
		switch(key)
		{
		case KB_NONE:
			return UI_RET_TIMEOUT;
		case KB_CANCEL:
		case KB_ENTER:
			return UI_RET_ABORT;
		case KB0:
			return 0;
		case KB1:
			return 1;
		}
	}
	
	return UI_RET_FAIL;
}

int Param_Printerprm(void * pxPARAM)
{
	int iRet;
	ST_PARAM * pxTerm = App_GetTermParam();
	
#if !defined(PRINTER)
	return APP_RET_OK;
#endif
	iRet = Ui_InputNum(PROMPT_PRINTERPRM, "print density(0-10):", pxTerm->printerdesity, 2, 1, APP_UI_TIMEOUT);
	if(iRet != UI_RET_SUCCESS) {
		goto FINISH;
	}	
	
	iRet = Ui_InputNum(PROMPT_PRINTERPRM, "prt line spacing(1-20):", pxTerm->printerline, 2, 1, APP_UI_TIMEOUT);
	if(iRet != UI_RET_SUCCESS) {
		goto FINISH;
	}
	Param_WriteParam();

FINISH:	
	return APP_RET_OK;
}

int Param_KeySound(void * pxPARAM)
{
	int iRet;
	ST_PARAM * pxTerm = App_GetTermParam();
	
#if !defined(KEYBOARD)
	return APP_RET_OK;
#endif
	Ui_Clear();
	Ui_DispTitle(PROMPT_SOUNDSWITCH);

	iRet = Param_SetParam(PROMPT_KEYTONE, PROMPT_OPEN, PROMPT_CLOSE, (pxTerm->keyTone == 1)? 1: 0, APP_UI_TIMEOUT);
	if(iRet >= 0) {
		if(iRet == 1) {
			pxTerm->keyTone = 1;
			Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "ENABLE");
		} else {
			pxTerm->keyTone = 0;
			Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "DISABLE");
		}
		Param_WriteParam();
	}
	
	return APP_RET_OK;
}

int Param_Reset(void * pxPARAM)
{
#if defined(LCDSHOW)
	Ui_Clear();
	Ui_DispTitle(PROMPT_RESET);
#if defined(KEYBOARD)	
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, PROMPT_WARN, 0);
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_CLRDATA, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_ENTER, 0);
	Sys_BeepFAIL();
	if(Ui_WaitKey(APP_UI_TIMEOUT) != KB_ENTER) {
		return 0;
	}
#endif
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_RESET, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_WAIT, 0);

	Param_RemoveFiles();
	//LvoslitSysReCover();

	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_END, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_REBOOT, 0);
	Sys_BeepOK();
	LvosSysPower(SYS_POWER_REBOOT);
	while(1);
#else
	Param_RemoveFiles();
	//LvoslitSysReCover();
#endif
	return 0;
}

static int Param_SetParams(TLVBuf * tlv, const ST_PARAMDEF * defs, int num)
{
	int iRet;
	int idx;
	
	for(idx = 0; idx < num; idx ++) {
		switch(defs[idx].type)
		{
		case T_BIN:
			iRet = Tlv_SetTag(tlv, defs[idx].tag, defs[idx].data, defs[idx].len);
			break;
		case T_STR:
			iRet = Tlv_SetTag(tlv, defs[idx].tag, defs[idx].data, strlen(defs[idx].data));
			break;
		case T_BYTE:
			iRet = Tlv_SetTag(tlv, defs[idx].tag, defs[idx].data, 1);
			break;
		case T_SHORT:
			iRet = Tlv_SetTag(tlv, defs[idx].tag, defs[idx].data, 2);
			break;
		case T_INT:
			iRet = Tlv_SetTag(tlv, defs[idx].tag, defs[idx].data, 4);
			break;
		default:
			return APP_RET_PARAM;
		}
		
		if(iRet < 0) {
			return APP_RET_FAIL;
		}
	}

	return APP_RET_OK;
}

int Param_WriteParam()
{
	int iRet;
	TLVBuf tlv;
	
	tlv.mlen = MAX_PARAM_SIZE;
	tlv.len = 0;
	tlv.buf = Sys_Malloc(tlv.mlen);
	if(tlv.buf == NULL) {
		return APP_RET_ERR_MEM;
	}
	if(Param_SetParams(&tlv, &m_ParamDefs[0], sizeof(m_ParamDefs) / sizeof(ST_PARAMDEF))) {
		Sys_Free(tlv.buf);
		return APP_RET_PARAM;
	}
	File_Remove(TERM_PARAM_FILE);
	iRet = File_Write(TERM_PARAM_FILE, 0, tlv.buf, tlv.len);
	Sys_Free(tlv.buf);
	if(iRet != tlv.len) {
		return APP_RET_ERR_MEM;
	}
	return APP_RET_OK;
}

int Param_IsLeapYear(int year)
{
    if ((year % 400) == 0) {
        return 1;
    } else if ((year % 100) == 0) {
        return 0;
    } else if ((year % 4) == 0) {
        return 1;
    } else {
        return 0;
    }
}

int Param_DaOfMon(int month, int year)
{
	const unsigned char g_day_per_mon[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (month != 2) {
        return g_day_per_mon[month - 1];
    } else {
        return g_day_per_mon[1] + Param_IsLeapYear(year);
    }
}

int Param_CheckTime(const char * time)
{
	int year;
	int mon;
	int day;
	char tmp[20];

	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time, 4);
	year = atoi(tmp);
	if(year < 1970) {
		return -1;
	}else if(year > 2099) {
		return -1;
	}

	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time+4, 2);
	mon = atoi(tmp);
	if(mon < 1 || mon > 12) {
		return -2;
	}
	
	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time+6, 2);
	day = atoi(tmp);
	if(day < 1 || day > Param_DaOfMon(mon, year)) {
		return -3;
	}
	
	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time+8, 2);
	day = atoi(tmp);
	if(day  < 0 || day  > 23) {
		return -4;
	}
	
	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time+10, 2);
	day = atoi(tmp);
	if(day < 0 || day > 59) {
		return -5;
	}
	
	memset(tmp, 0, sizeof(tmp));
	memcpy(tmp, time+12, 2);
	day = atoi(tmp);
	if(day < 0 || day > 59) {
		return -6;
	}
	
	return 0;
}

int Param_SetTime(void * pxPARAM)
{
	int iRet;
	char tmp[50];

RETRY:
	Ui_Clear();
	Ui_DispTitle(PROMPT_TIME);
	switch(Ui_GetLcdHeight())
	{
	case 64:
		memset(tmp, 0, sizeof(tmp));
		Sys_GetTime(tmp, tmp+8);
		Ui_DispTextLineAlign(1, DISPLAY_RIGHT, tmp, 0);
		iRet = Ui_Input(2, NULL, PROMPT_DTIME, tmp, INPUT_NUM, 14, 14, APP_UI_TIMEOUT);
		if(iRet != UI_RET_SUCCESS) {
			return APP_RET_OK;
		}
		break;
		
	default:
		Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_CTIME, 0);
		memset(tmp, 0, sizeof(tmp));
		Sys_GetTime(tmp, tmp+8);
		Ui_DispTextLineAlign(2, DISPLAY_RIGHT, tmp, 0);
		Ui_DispTextLineAlign(3, DISPLAY_LEFT, PROMPT_FORMAT, 0);
		iRet = Ui_Input(4, NULL, PROMPT_DTIME, tmp, INPUT_NUM, 14, 14, APP_UI_TIMEOUT);
		if(iRet != UI_RET_SUCCESS) {
			return APP_RET_OK;
		}
		break;
	}
	
	if(Param_CheckTime(tmp) != 0) {
		Ui_Clear();
		Ui_DispTitle(PROMPT_TIME);
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_TIMEERROR, 0);
		Sys_BeepFAIL();
		Ui_WaitKey(APP_PROMPT_TIMEOUT);
		goto RETRY;
	}
	
	Sys_SetTime(tmp, tmp+8);
	
	return APP_RET_OK;
}

static int Param_GetAPList(const char * buffer, ST_WIFIAP * apList, int total, int num)
{
	int iRet;
	int idx;
	byte * start;
	byte * end;
	
	// 解析成APLIST
	start = (byte *)buffer;
	while((num < total) && (*start != 0x00)) {
		ST_WIFIAP apInfo;
		char tmp[50 +1];
		// ssid;rssi;mode|ssid;rssi;mode|
		memset((char *) &apInfo, 0, sizeof(ST_WIFIAP));
		end = strstr(start, ";");
		if(end == NULL) {
			break;
		}
		iRet = end - start;
		if(iRet > 0) {
			memset(tmp, 0, sizeof(tmp));
			if(iRet > 50) {
				memcpy(tmp, start, 50);
			} else {
				memcpy(tmp, start, iRet);
			}
			strncpy(apInfo.apName, Param_DelSpace(tmp), 50);
		}
		start = end + 1;
		
		// rssi
		end = strstr(start, ";");
		if(end == NULL) {
			break;
		}
		iRet = end - start;
		if(iRet > 0) {
			memset(tmp, 0, sizeof(tmp));
			if(iRet > 5) {
				memcpy(tmp, start, 5);
			} else {
				memcpy(tmp, start, iRet);
			}
			strncpy(apInfo.apSignal, Param_DelSpace(tmp), 5);
		}
		start = end + 1;
		
		// mode
		end = strstr(start, "|");
		if(end == NULL) {
			end = start + strlen(start);
		}
		iRet = end - start;
		if(iRet > 0) {
			memset(tmp, 0, sizeof(tmp));
			if(iRet > 32) {
				memcpy(tmp, start, 32);
			} else {
				memcpy(tmp, start, iRet);
			}
			strncpy(apInfo.apMode, Param_DelSpace(tmp), 32);
		}
		start = end + 1;
		
		PARAM_STRLOG("SSID[%d] = %s, %s, %s, ", idx, apInfo.apName, apInfo.apSignal, apInfo.apMode);
		if( !strlen(apInfo.apName) || !strlen(apInfo.apSignal)) {
			continue;
		}
	
		for(idx = 0, iRet = 0; idx < num; idx ++) {
			//PARAM_STRLOG("apList[%d].apName = %s ", idx, apList[idx].apName);
			if( !strcmp(apInfo.apName, apList[idx].apName) ) {
				iRet = 1;
				break;
			}
		}
		
		// 没有重名,则添加到列表中
		if( !iRet ) {
			memcpy((char *)&apList[num], (char *)&apInfo, sizeof(ST_WIFIAP));
			num ++;
		}
		
		// 热点列表已满
		if(num >= total) {
			goto FINISH;
		}
	}
FINISH:

	return num;
}

int Param_ScanApList(ST_WIFIAP * apList, int total, uint timeMS)
{
#define BUFSIZE 4096
	int iRet;
	int idx;
	int isWifiEnable = 0;
	uint iLeft = 0;
	int num = 0;
	byte * start;
	byte * end;
	byte * buffer;
	uint  scanAp;
	uint 	now = 0;
	int handle = Sys_GetHandle("WIFI");
	uint startTime = LvosSysTick();
	
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, PROMPT_SCAN, 0);

	while(1) {
		uint wifiStat = 0;
		uint apStat = 0;
		
		Comm_GetWifiStatus(handle, &wifiStat, &apStat, NULL);
		
		if (!isWifiEnable && !wifiStat) {
			// 启动WIFI 模块
			Comm_WriteParam(handle, "ENABLE", "1");
			isWifiEnable = 1;
		}
		
		if (wifiStat) {
			PARAM_STRLOG("WIFI Ready!");
			break;
		}
		if(startTime + timeMS < LvosSysTick() ) {
			PARAM_STRLOG("wait WIFI timeout!");
			return 0;
		}
		
		if(Ui_GetKey() == KB_CANCEL) {
			return APP_RET_ERR_ESCED;
		}
		
		Sys_Delay(200);
	}
	
	buffer = Sys_Malloc(BUFSIZE);
	if(buffer == NULL) {
		return APP_RET_ERR_MEM;
	}
	
RESCAN:
	Comm_WriteParam(handle, "SCAN", "ENABLE");
	
	scanAp = LvosSysTick();
	while( 1 ) {
		now = LvosSysTick();
		
		if(startTime + timeMS < now) {
			goto FINISH;
		}
		
		if(Ui_GetKey() == KB_CANCEL) {
			goto FINISH;
		}
		
		memset(buffer, 0, BUFSIZE);
		iRet = Comm_ReadParam(handle, "SCAN", buffer, BUFSIZE);
		if(iRet < 0) {
			PARAM_STRLOG("Sys_ReadParam(WIFI, SCAN) = %d", iRet);
			goto FINISH;
		}
		if( strlen(buffer) == 0 ) {
			PARAM_STRLOG("WIFI SCAN is empty");
			if(scanAp + 5000 < now) {
				goto RESCAN;
			}
			Sys_Delay(100);
			continue;
		}
		
		strcat(buffer, "|");
		num = Param_GetAPList(buffer, &apList[0], total, num);
		if(num >= total) {
			goto FINISH;
		}
		
	}
	
FINISH:
	Sys_Free(buffer);
	
	return num;
}

int Param_WifiScan64(void * pxPARAM)
{
	int         num;
	int         pageNo;
	int         page;
	int         pageSize;
	int         selected;
	byte        tmp[100];
	int         isModifyed = 0;
	char * 		pxNAME = NULL;
	ST_WIFIAP * apList = NULL;
	ST_PARAM  * pxTerm = App_GetTermParam();

	LvosUiSetAttr(0, UI_ATTR_CLOSEICON, 1);

	Ui_Clear();
	Ui_DispTitle(PROMPT_SCAN);
	apList = (ST_WIFIAP *)Sys_Malloc(sizeof(ST_WIFIAP) * MAX_WIFIAP_NUM);
	if(apList == NULL) {
		goto FINISH;
	}
RESCAN:
	memset((char *)apList, 0, sizeof(ST_WIFIAP) * MAX_WIFIAP_NUM);
	num = Param_ScanApList(apList, MAX_WIFIAP_NUM, 16000);
	if(num <= 0) {
		Ui_ClearLine(1, Ui_GetLineNum()-1);
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_NOSID, 0);
		Sys_BeepFAIL();
		Ui_WaitKey(APP_PROMPT_TIMEOUT);
		goto FINISH;
	}

SELECT:
	pageSize = Ui_GetLineNum() -1;
	page = (num + pageSize - 1) / pageSize;
	selected = -1;
	Ui_Clear();
	Ui_DispTitle(PROMPT_SELSID);
	for(pageNo = 0; pageNo < page; ) {
		int line = 0;
		int lineNo;
		byte key;
		
		Ui_ClearLine(1, pageSize);
		line = num - pageNo * pageSize;
		if(line > pageSize) {
			line = pageSize;
		}
		for(lineNo = 0; lineNo < line; lineNo ++) {
			char   name[60];
			
			pxNAME = apList[pageNo * pageSize + lineNo].apName;
			memset(name, 0, sizeof(name));
			if( Utils_IsTextUTF8(pxNAME, strlen(pxNAME)) ) {
				char gbk[60];
				memset(gbk, 0, sizeof(gbk));
				Utils_UTF82GBK(pxNAME, gbk, strlen(pxNAME));
				sprintf(name, "%d.%s", 1+lineNo, gbk);
			} else {
				sprintf(name, "%d.%s", 1+lineNo, pxNAME);
			}
			Ui_DispTextLineAlign(1+lineNo, DISPLAY_LEFT, name, 0);
		}
		
WAITKEY:
		key = Ui_WaitKey(APP_UI_TIMEOUT);
		if(key >= KB1 && key <= (KB0 + line)) {
			selected = pageNo * pageSize + (key - KB0) - 1;
			break;
		}
		
		switch(key)
		{
		case KB_CANCEL:
		case KB_NONE:
			goto FINISH;
		case KB_ENTER:
			goto RESCAN;
		case KB_UP:
			if(pageNo > 0) {
				pageNo --;
			}
			break;
			
		case KB_DOWN:
			if(pageNo < page - 1) {
				pageNo ++;
			}
			break;
		default:
			goto WAITKEY;
		}
	}

	Ui_Clear();
	Ui_DispTitle(PROMPT_CONFIRMSID);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_SSID, 0);
	pxNAME = apList[selected].apName;
	if( Utils_IsTextUTF8(pxNAME, strlen(pxNAME)) ) {
		char gbk[60];
		memset(gbk, 0, sizeof(gbk));
		Utils_UTF82GBK(pxNAME, gbk, strlen(pxNAME));
		Ui_DispTextLineAlign(2, DISPLAY_LEFT, gbk, 0);
	} else {
		Ui_DispTextLineAlign(2, DISPLAY_LEFT, pxNAME, 0);
	}
	Ui_DispTextLineAlign(3, DISPLAY_LEFT, PROMPT_ENTERCAN, 0);

	while(1)
	{
		byte key;
		key = Ui_WaitKey(APP_UI_TIMEOUT);
		if(key == KB_ENTER) {
			memset(pxTerm->wifi.apName, 0, sizeof(pxTerm->wifi.apName));
			memset(pxTerm->wifi.apMode, 0, sizeof(pxTerm->wifi.apMode));
			strcpy(pxTerm->wifi.apName, apList[selected].apName);
			strcpy(pxTerm->wifi.apMode, apList[selected].apMode);
			break;
		}
		if(key == KB_NONE) {
			goto FINISH;
		}
		if(key == KB_CANCEL) {
			goto SELECT;
		}
	}

	Ui_Clear();
	Ui_DispTitle(PROMPT_SSIDPWD);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_CURSSIDPWD, 0);
	memset(tmp, 0, sizeof(tmp));
	Utils_Escape(pxTerm->wifi.apKey, tmp);
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, tmp, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_ENTERFIX, 0);
	while(1)
	{
		byte key;
		key = Ui_WaitKey(APP_UI_TIMEOUT);
		if(key == KB_NONE || key == KB_CANCEL) {
			break;
		}
		if(key == KB_ENTER) {
			Ui_ClearLine(1, Ui_GetLineNum() - 2);
			memset(tmp, 0, sizeof(tmp));
			strcpy(tmp, pxTerm->wifi.apKey);
			num = Ui_InputStrSelectMode(1, NULL, PROMPT_ISSIDPWD, tmp, INPUT_STR, 50, 0, APP_UI_TIMEOUT);
			if(num == UI_RET_SUCCESS) {
				memset(pxTerm->wifi.apKey, 0, sizeof(pxTerm->wifi.apKey));
				strcpy(pxTerm->wifi.apKey, tmp);
				Param_WriteParam();
				isModifyed = 1;
				break;
			}
		}
	}
	
FINISH:
	if(apList != NULL) {
		Sys_Free(apList);
	}
	if(isModifyed) {		
		Comm_Init();
		Comm_WriteParam(Sys_GetHandle("WIFI"), "CONNECT_AP", "1");
		Ui_Clear();
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_WIFISETOK, 0);
		Sys_BeepOK();
		Ui_WaitKey(APP_PROMPT_TIMEOUT);
	}
	
	LvosUiSetAttr(0, UI_ATTR_CLOSEICON, 0);
	
	return APP_RET_OK;
}

int Param_WifiInput64(void * pxPARAM)
{
	int         iRet;
	byte        tmp[100];
	int         isModifyed = 0;
	ST_PARAM  * pxTerm = App_GetTermParam();
	
	LvosUiSetAttr(0, UI_ATTR_CLOSEICON, 1);
	Ui_Clear();
	Ui_DispTitle(PROMPT_SSIDNAME);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_CURRENTSID, 0);
	memset(tmp, 0, sizeof(tmp));
	Utils_Escape(pxTerm->wifi.apName, tmp);
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, tmp, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_ENTERFIX, 0);
	while(1)
	{
		iRet = Ui_WaitKey(APP_UI_TIMEOUT);
		if(iRet == KB_NONE || iRet == KB_CANCEL) {
			break;
		}
		if(iRet == KB_ENTER) {
			Ui_ClearLine(1, Ui_GetLineNum() - 2);
			memset(tmp, 0, sizeof(tmp));
			strcpy(tmp, pxTerm->wifi.apName);
			iRet = Ui_InputStrSelectMode(1, NULL, PROMPT_ISSIDNAME, tmp, INPUT_STR, 50, 1, APP_UI_TIMEOUT);
			if(iRet == UI_RET_SUCCESS) {
				memset(pxTerm->wifi.apName, 0, sizeof(pxTerm->wifi.apName));
				strcpy(pxTerm->wifi.apName, tmp);
				Param_WriteParam();
				isModifyed = 1;
				break;
			}
		}
	}
	
	Ui_Clear();
	Ui_DispTitle(PROMPT_SSIDPWD);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_CURSSIDPWD, 0);
	memset(tmp, 0, sizeof(tmp));
	Utils_Escape(pxTerm->wifi.apKey, tmp);
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, tmp, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_ENTERFIX, 0);
	while(1)
	{
		iRet = Ui_WaitKey(APP_UI_TIMEOUT);
		if(iRet == KB_NONE || iRet == KB_CANCEL) {
			break;
		}
		if(iRet == KB_ENTER) {
			Ui_ClearLine(1, Ui_GetLineNum() - 2);
			memset(tmp, 0, sizeof(tmp));
			strcpy(tmp, pxTerm->wifi.apKey);
			iRet = Ui_InputStrSelectMode(1, NULL, PROMPT_ISSIDPWD, tmp, INPUT_STR, 50, 0, APP_UI_TIMEOUT);
			if(iRet == UI_RET_SUCCESS) {
				memset(pxTerm->wifi.apKey, 0, sizeof(pxTerm->wifi.apKey));
				strcpy(pxTerm->wifi.apKey, tmp);
				Param_WriteParam();
				isModifyed = 1;
				break;
			}
		}
	}
	
	Ui_Clear();
	Ui_DispTitle(PROMPT_SSIDMODE);
	sprintf(tmp, "%s: %s",PROMPT_CURMODE, pxTerm->wifi.apMode);
	Ui_DispTextLineAlign(1, DISPLAY_RIGHT, tmp, 0);
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, "1.PSK  2.WPA  3.WPA2", 0);
	Ui_DispTextLineAlign(3, DISPLAY_LEFT, "4.WPA/WPA2    5.WEP", 0);
	while(1)
	{
		iRet = Ui_WaitKey(APP_UI_TIMEOUT);
		if(iRet == KB1) {
			strcpy(pxTerm->wifi.apMode, "PSK");
			Param_WriteParam();
			isModifyed = 1;
			break;
		}
		if(iRet == KB2) {
			strcpy(pxTerm->wifi.apMode, "WPA");
			Param_WriteParam();
			isModifyed = 1;
			break;
		}
		if(iRet == KB3) {
			strcpy(pxTerm->wifi.apMode, "WPA2");
			Param_WriteParam();
			isModifyed = 1;
			break;
		}
		if(iRet == KB4) {
			strcpy(pxTerm->wifi.apMode, "WPA/WPA2");
			Param_WriteParam();
			isModifyed = 1;
			break;
		}
		if(iRet == KB5) {
			strcpy(pxTerm->wifi.apMode, "WEP");
			Param_WriteParam();
			isModifyed = 1;
			break;
		}
		if(iRet == KB_NONE || iRet == KB_CANCEL) {
			break;
		}
	}
	
	if(isModifyed) {
		Comm_Init();
		Comm_WriteParam((int)Sys_GetHandle("WIFI"), "CONNECT_AP", "1");
		Ui_Clear();
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_WIFISETOK, 0);
		Sys_BeepOK();
		Ui_WaitKey(APP_PROMPT_TIMEOUT);
	}
	
	LvosUiSetAttr(0, UI_ATTR_CLOSEICON, 0);
	
	return APP_RET_OK;
}

int Param_SetWIFI(void * pxPARAM)
{
    Ui_Clear();
	Ui_ClearKey();
	Ui_DispTitle(PROMPT_WIFISSID);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, PROMPT_SCANSSID, 0);
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, PROMPT_YES, 0);
	Ui_DispTextLineAlign(3, DISPLAY_LEFT, PROMPT_NO, 0);
	
	while(1)
	{
		switch(Ui_WaitKey(APP_UI_TIMEOUT))
		{
		case KB1:
			return Param_WifiScan64(NULL);
			
		case KB0:
			return Param_WifiInput64(NULL);
			
		case KB_CANCEL:
		case KB_NONE:
			return APP_RET_NO_DISP;
		}
	}
	
	return APP_RET_NO_DISP;
}

int Param_Prmset(void * pxPARAM)
{
	int iRet;
	ST_PARAM * pxTerm = App_GetTermParam();
#if defined(GPRSCOM) || defined(WIFICOM)
    Ui_Clear();
	Ui_DispTitle(PROMPT_PRMSET);

	if( App_HasWifi() ) {
		iRet = Param_SetParam(PROMPT_PRMSET, "GPRS", "WIFI", (pxTerm->commMode == COMM_GPRS)? 1: 0, APP_UI_TIMEOUT);
		if(iRet >= 0) {
			if(iRet == 1) {
				pxTerm->commMode = COMM_GPRS;
				Comm_WifiEnable(0);
				Param_WriteParam();
				Comm_Init();

				Ui_Clear();
				Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_COMMOK, 0);
				Sys_BeepOK();
				Comm_UpdateSignal(1);
				Ui_WaitKey(APP_PROMPT_TIMEOUT);
				
			} else {
				pxTerm->commMode = COMM_GPRS | COMM_WIFI;
				Comm_WifiEnable(1);
				Param_WriteParam();
				Comm_Init();
				Param_SetWIFI(NULL);
			}		
		}
	}
#endif
	
#if defined(PRINTER)
	iRet = Ui_InputNum(PROMPT_PRMSET, "print density(0-10):", pxTerm->printerdesity, 2, 1, APP_UI_TIMEOUT);
	if(iRet != UI_RET_SUCCESS) {
		return APP_RET_FAIL;
	}	
	
	iRet = Ui_InputNum(PROMPT_PRMSET, "prt line spacing(1-20):", pxTerm->printerline, 2, 1, APP_UI_TIMEOUT);
	if(iRet != UI_RET_SUCCESS) {
		return APP_RET_FAIL;
	}
	Param_WriteParam();
#endif
	
	
#if defined(KEYBOARD)
	Ui_Clear();
	Ui_DispTitle(PROMPT_PRMSET);

	iRet = Param_SetParam(PROMPT_KEYTONE, PROMPT_OPEN, PROMPT_CLOSE, (pxTerm->keyTone == 1)? 1: 0, APP_UI_TIMEOUT);
	if(iRet >= 0) {
		if(iRet == 1) {
			pxTerm->keyTone = 1;
			Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "ENABLE");
		} else {
			pxTerm->keyTone = 0;
			Sys_WriteParam(Sys_GetHandle("IOCTL"), "KBTONE", "DISABLE");
		}
		Param_WriteParam();
	}
#endif
	
	return APP_RET_OK;
}



#include <libapp.h>

#include "app.h"
#include "param.h"
#include "func.h"
#include "emvproc.h"


int Func_Menu(const char *title, const ST_MENUITEM * menu, int num, uint timeMS)
{
    int idx;
    int item;
    int iRet;
    int isTitle = 1; 
    int pageSize = 0;
	int pageIdx = 0;
	int pageCounter = 0;

    if (menu == NULL || num > 9 || num <= 0)
    {
        return UI_RET_PARAM;
    }
	
    isTitle = 1;
    if (title != NULL){
        isTitle += 1;
    }
	
	pageSize = Ui_GetLineNum() - isTitle;
	pageCounter = (num + pageSize - 1 ) / pageSize;
	pageIdx = 0;
	
MENU_DRAW_MENU:
    Ui_Clear();
    if (title != NULL) {
    	Ui_DispTitle(title);
    }
    for (idx = 0; idx < pageSize; idx++)
    {
        int line = idx + isTitle - 1;
		int item = pageIdx * pageSize + idx;
   		if(item >= num) {
			break;
   		}
		Ui_DispTextLineAlign(line, DISPLAY_LEFT, menu[item].text, 0);
    }
    
MENU_WAIT_KEY:
    Ui_ClearKey();
    iRet = Ui_WaitKey(timeMS);
    if (iRet == KB_NONE)
    {
        // return UI_RET_TIMEOUT;
        return 0;
    }
    if (iRet == KB_CANCEL)
    {
        // return UI_RET_ABORT;
        return 0;
    }
	
	if(iRet == KB_DOWN) {
		if(pageIdx < pageCounter - 1) {
			pageIdx ++;
		}
		goto MENU_DRAW_MENU;
	}
	
	if(iRet == KB_UP) {
		if(pageIdx > 0) {
			pageIdx --;
		}
		goto MENU_DRAW_MENU;
	}

    for (idx = 0; idx < pageSize; idx++)
    {
		int item = pageIdx * pageSize + idx;
   		if(item >= num) {
			break;
   		}
        if (menu[item].kb != iRet)
        {
            continue;
        }
        if (menu[item].func != NULL)
        {
            iRet = menu[item].func(menu[item].param);
#if 0
            if (iRet)
            { // exit the menu
                return iRet;
            }
#endif
            goto MENU_DRAW_MENU;
        }
        goto MENU_WAIT_KEY;
    }

    goto MENU_WAIT_KEY;
}

int Func_DispInfo(const char *pTitle, const char *pMsg, int timeOutMs)
{
	int   num=0;
	int   page;
	int   pageNo;
	int   pageSize;
	int   titleFlag = 0;
	char* pStr=NULL;
	char* pStart=NULL;
	char  apList[20][30]={0};
	char  Tem[128]={0};

	Ui_Clear();
	Ui_ClearKey();
	if(pTitle != NULL) {
		Ui_DispTitle(pTitle);
		titleFlag = 1;
	}
	pStart = (char *)pMsg;
	while(1)
	{
		pStr=strstr(pStart,"\n");
		if(pStr==NULL)
			break;
		memset(Tem,0,sizeof(Tem));
		memcpy(Tem,pStart,pStr-pStart);
        if(strlen(Tem) <= Ui_GetLineEnNum()) {
			strcpy(apList[num++],Tem);
        }
		else
		{
			int i=0,j=0;
			while(Tem[i])
			{
				if(j >= Ui_GetLineEnNum())
				{
					num++;
					j=0;
				}
				if(Tem[i] & 0x80)
				{
				   if((j + 2) > Ui_GetLineEnNum()) {
				   		num++;
						j = 0;
				   }
				   memcpy(apList[num]+j,Tem+i,2);	
				   i+=2;
				   j+=2;
				}
				else
				    apList[num][j++]=Tem[i++];	
			}
		    num++;
		}
		pStart=pStr+1;
	}
	pageSize = Ui_GetLineNum() - 1 - titleFlag;
	page = (num + pageSize - 1) / pageSize;
	for(pageNo = 0; pageNo < page; ) {
		int line = 0;
		int lineNo;
		byte key;
		Ui_ClearLine(titleFlag, pageSize);
		line = num - pageNo * pageSize;
		if(line > pageSize) {
			line = pageSize;
		}
		for(lineNo = 0; lineNo < line; lineNo ++) {
			Ui_DispTextLineAlign(titleFlag+lineNo, DISPLAY_LEFT, apList[pageNo * pageSize + lineNo], 0);
		}
		key = Ui_WaitKey(timeOutMs);
		switch(key)
		{
			case KB_CANCEL:
			case KB_NONE : 
				return KB_CANCEL;
			case KB_ENTER:
				return KB_ENTER;
			case KB_UP:
				if(pageNo > 0) {
					pageNo --;
				}
				break;
			case KB_DOWN:
				if(pageNo < page - 1) {
					pageNo ++;
				}
				break;
		}
	}
}

void Func_DispTransTitle(byte * title)
{
	Ui_Clear();
	Ui_ClearLineColor(0, DISPLAY_FONT_COLOR);
	Ui_DispTextLineAlign(0, DISPLAY_CENTER, title, 1);
}

void Func_DispRetCode(byte * title,const char * rspMsg)
{	
	int i;
	char buf[DISPLAY_LINE_EN_NUM + 1];
	
	Func_DispTransTitle(title);
	memset(buf, 0, sizeof(buf));
	i = Utils_StrCopy(buf, rspMsg, Ui_GetLineEnNum());
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, buf, 0);
	if(i < strlen(rspMsg)) {
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, rspMsg+ i, 0);
	}
	Ui_WaitKey(APP_PROMPT_TIMEOUT);
	return;
}

int Func_EnterPIN(int isRetry)
{
	int iRet;
	ST_TRANLOG *    pxLOG = App_GetTranLog();
	
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	if(strlen(pxLOG->amt) > 0) {
		char disp[20];
		memset(disp, 0, sizeof(disp));
		Utils_Asc2Amt(pxLOG->amt, disp, 20);
		//Ui_DispBigNumStr(1 * DISPLAY_CHAR_HEIGHT+4, DISPLAY_RIGHT, DISPLAY_COLOR_RED, disp);
		Ui_DispTextLineAlign(1, DISPLAY_RIGHT, disp, 0);
		Ui_DispTextLineAlign(2, DISPLAY_LEFT, "pls input online pin:", 0);
	} else {
		Ui_DispTextLineAlign(1, DISPLAY_LEFT, "pls input online pin:", 0);
	}
	if(Ui_GetLcdHeight() != 64) {
		Ui_DispTextLineAlign(Ui_GetLineNum() - 2, DISPLAY_CENTER, "no pin ENTER key", 0);
	}
	memset(pxLOG->pin, 0, sizeof(pxLOG->pin));
	iRet = Sec_EnterPIN("0,4,5,6,7,8,9,10,11,12", pxLOG->card, pxLOG->pin, APP_UI_TIMEOUT);
	switch(iRet)
	{
		case SEC_RET_OK: /*input PIN*/
			pxLOG->entryMode[2] = '1';
			return SEC_RET_OK;
		
		case SEC_RET_NO_PIN: /*no pwd*/
			pxLOG->entryMode[2] = '2';
			memset(pxLOG->pin, 0, sizeof(pxLOG->pin));
			return SEC_RET_NO_PIN;
			
		case SEC_ERR_PIN_TIMEOUT:
		case SEC_ERR_INPUT_CANCEL: /*cancel*/
			return SEC_RET_PIN_ABORT;
	}

	return iRet;
}

int Func_Detect(int handle, char * reset)
{
	if(LvosScPoll(handle) == 0 && LvosScActive(handle, reset) == 0) {
		return 0;
	}
	
	return -1;
}

int Func_Scan(const char * amt, char * payCode, uint timeMS)
{
	int iRet;
	uint start;
	uint left;
		
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, "show payment code", 0);
	Ui_DispTextLineAlign(Ui_GetLineNum()-2, DISPLAY_CENTER, "Angle 30,10-15CM", 0);
	
	switch( Ui_GetLcdHeight() ) {
	case 64:
		break;
	case 96:  
   		Ui_DispSimpleBMP(38, 26, "scan.bmp");// 52x40
		break;
	default:
		Ui_DispBMP(0, 48, 176, 120, "scan_1.bmp");
		break;
	}
	
	iRet = LvosScanStart();
	if(iRet < 0) {
		LvosScanStop();
		goto INPUT_CODE;
	}

	start = LvosSysTick();
	while(1) {
		if(timeMS) {
			iRet = LvosSysTick() - start;
			if(timeMS > iRet && left != (timeMS -iRet) / 1000) {
				char tmp[20];
				left = (timeMS -iRet) / 1000;
				sprintf(tmp, "    %d    ", left);
				Ui_Disp(176, 120, "            ", DISPLAY_COLOR_BLACK, DISPLAY_BG_COLOR, UI_FONT_NORMAL, 0);
			    Ui_Disp(176, 120, tmp, DISPLAY_COLOR_BLACK, DISPLAY_BG_COLOR, UI_FONT_NORMAL, 0);
			}
			if (timeMS < iRet) {
				LvosScanStop();
				return APP_RET_TIMEOUT;
			}
		}
		iRet = LvosScanGetDecode(payCode);
		if(iRet < 0) {
			LvosScanStop();
			break;
		}
		if(strlen(payCode)) {
			LvosScanStop();
			Sys_BeepOK();
			return APP_RET_OK;
		}
		switch (Ui_GetKey())
		{
			case KB_CANCEL:
				LvosScanStop();
				return APP_RET_CANCLE;
			
			case KB_UP:
				LvosScanStop();
				goto INPUT_CODE;
			
			default: //if 4G set delay
				if( App_IsLTE() ) {
					Sys_Delay(300);
				}
				break;			
		}
	}

INPUT_CODE:
	iRet = Ui_InputNum(NULL, "input payment code:", payCode, 32, 1, timeMS);
	if(iRet == UI_RET_SUCCESS) {
		return APP_RET_OK;
	}
	
	return APP_RET_CANCLE;
}


void Func_RemoveCard()
{
	char tmp[100];
	
	if(Func_Detect(App_GetIccHandle(), tmp) == 0) {
		Ui_ClearLine(1, Ui_GetLineNum()-2);
		Sys_BeepFAIL();
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_REMOVECARD, 0);
		while (Func_Detect(App_GetIccHandle(), tmp) == 0);
	}
	if(Func_Detect(App_GetPiccHandle(), tmp) == 0) {
		Ui_ClearLine(1, Ui_GetLineNum()-2);
		Sys_BeepFAIL();
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_REMOVECARD, 0);
		while (Func_Detect(App_GetPiccHandle(), tmp) == 0);
	}
	LvosScRemoval(App_GetIccHandle());
	LvosScRemoval(App_GetPiccHandle());
}



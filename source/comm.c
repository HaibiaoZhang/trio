#include <libapp.h>
#include "app.h"
#include "param.h"
#include "comm.h"

#ifdef COMM_DEBUG
#define COMM_STRLOG DBG_STR
#define COMM_HEXLOG DBG_HEX
#else
#define COMM_STRLOG  
#define COMM_HEXLOG
#endif

#define TMP_SIZE 1024

static int m_CurCommMode;
static int m_CommHandle = -1;
static uint m_uiCommSignal = 0;
static uint m_SignalFresh = 1000; // 信号刷新间隔从1秒刷新一次，开始到默认10秒刷新一次

int Comm_WifiEnable(int isEnable)
{
	int iRet;
	int handle = Sys_GetHandle("WIFI");
	
	if(handle < 0) {
		return COMM_RET_FAIL;
	}
	
	if(isEnable) {
		iRet = Comm_WriteParam(handle, "ENABLE", "1");
		if(iRet < 0) {
			return COMM_RET_FAIL;
		}
		return COMM_RET_OK;
	}
	
	iRet = Comm_WriteParam(handle, "ENABLE", "0");
	if(iRet < 0) {
		return COMM_RET_FAIL;
	}
	
	for(iRet = 0; iRet < 10; iRet++) {
		uint wifiStat = 0;
		uint apStat = 0;
		Comm_GetWifiStatus(handle, &wifiStat, &apStat, NULL);
		if(wifiStat == 0 && apStat == 0) {
			break;
		}
		Sys_Delay(200);
	}
	
	return COMM_RET_OK;
}

static void Comm_WaitGPRSReady(uint timeMS)
{
	int iRet;
	uint isPPP = 0;
	uint start = LvosSysTick();
	
	while(timeMS > (LvosSysTick() - start))
	{
		uint status = 0;
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet >= 0) {
			if(status & 0x08) {
				COMM_STRLOG("GPRS Ready!");
				return;
			}
		} else {
			COMM_STRLOG("LvosCommCheck(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
		}

		if(!isPPP) {
			iRet = LvosCommRequest((uint)m_CommHandle);
			if(iRet >= 0) {
				isPPP = 1;
			} else {
				COMM_STRLOG("LvosCommRequest(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
			}
		}
		
		Sys_Delay(200);
	}
}

int Comm_WriteParam(int handle, const char * param, const char * buffer)
{
	int iRet;
	
	iRet = Sys_WriteParam(handle, param, buffer);
	COMM_STRLOG("Sys_WriteParam(%d, %s, %s) = %d", handle, param, buffer, iRet);
	return iRet;
}

int Comm_ReadParam(int handle, const char * param, char * buffer, uint size)
{
	int iRet;
	
	iRet = Sys_ReadParam(handle, param, buffer, size);
	COMM_STRLOG("Sys_ReadParam(%d, %s, %s) = %d", handle, param, buffer, iRet);
	return iRet;
}

int Comm_GetWifiStatus(int handle, uint * wifiStat, uint * apStat, int *signal)
{
	int iRet;
	char * status;
	char * start;
	char * end;
	
	status = Sys_Malloc(200);
	if(status == NULL) {
		return -1;
	}
	
	memset(status, 0, 200);
	iRet = Comm_ReadParam(handle, "STATUS", status, 200);
	if(iRet < 0) {
		Sys_Free(status);
		return iRet;
	}
	if(wifiStat != NULL) {
		if(status[0] == '1') {
			*wifiStat = 1;
		}else {
			*wifiStat = 0;
		}
	}
	if(apStat != NULL) {
		if(status[2] == '1') {
			*apStat = 1;
		}else {
			*apStat = 0;
		}
	}
	if( *apStat == 1 && signal != NULL) {
		char * start = strstr(status, ";");
		if(start == NULL) {
			*signal = 0;
		}
		start = strstr(start+1, ";");
		if(start == NULL) {
			*signal = 0;
		}
		end = strstr(start, "|");
		if(end == NULL) {
			*signal = 0;
		}
		*end = 0;
		*signal = atoi(start+1);
	}
	
	Sys_Free(status);
	
	return 0;
}

static void Comm_WaitWIFIReady(uint timeMS)
{
	int iRet;
	uint isWifiEnable = 0;
	uint isConnectAp = 0;
	uint start = LvosSysTick();
	
	while(timeMS > (LvosSysTick() - start))
	{
		uint wifiStat = 0;
		uint apStat = 0;
		Comm_GetWifiStatus(m_CommHandle, &wifiStat, &apStat, NULL);
		if(!isWifiEnable && !wifiStat) {
			Comm_WriteParam(m_CommHandle, "ENABLE", "1");
			isWifiEnable = 1;
		}
		if(wifiStat && !apStat && !isConnectAp ) {
			Comm_WriteParam(m_CommHandle, "CONNECT_AP", "1");
			isConnectAp = 1;
		}
		if(wifiStat && apStat) {
			COMM_STRLOG("WIFI Ready!");
			return;
		}
		
		Sys_Delay(200);
	}
}

static void Comm_InitInner(uint mode)
{
	int iRet;
	uint status;
	ST_PARAM *pxTERM = App_GetTermParam();
	
RETRY:
	// close
	if(m_CommHandle >= 0) {
		uint start = LvosSysTick();
		LvosCommRelease((uint)m_CommHandle, 1);
		while(1) {
			status = 0;
			iRet = LvosCommCheck((uint)m_CommHandle, &status);
			if(iRet < 0) {
				COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
				break;
			}
			if( !(status & 0x08) ) {
				break;
			}
			iRet = (LvosSysTick() - start);
			if(20000 < iRet) {
				break;
			}
			
			LvosSysDelayMs(100);
		}
		m_CommHandle = -1;
	}
	
	// open
	COMM_STRLOG("mode = %d", mode);
	if((mode & COMM_WIFI) && (strlen(pxTERM->wifi.apName) > 0)) {
		COMM_STRLOG("pxTERM->wifi.apName = %s", pxTERM->wifi.apName);
		m_CurCommMode = COMM_WIFI;
		m_CommHandle = Sys_GetHandle("WIFI");
	}
	
	if(m_CommHandle >= 0) {
		iRet = Comm_WriteParam(HWND_APP, "ROUTE", "WIFI");
	} else {
		m_CurCommMode = COMM_GPRS;
		m_CommHandle = Sys_GetHandle("WLAN0");
		iRet = Comm_WriteParam(HWND_APP, "ROUTE", "GPRS");
	}

	switch (m_CurCommMode)
	{
	case COMM_GPRS:
		Comm_WriteParam(m_CommHandle, "PIN", "card");
		Comm_WriteParam(m_CommHandle, "APN", "CMNET");
		Comm_WriteParam(m_CommHandle, "PROTOCOL", "TCP");
		break;
		
	case COMM_WIFI:
		Comm_WriteParam(m_CommHandle, "SSID", pxTERM->wifi.apName);
		Comm_WriteParam(m_CommHandle, "KEY", pxTERM->wifi.apKey);
		Comm_WriteParam(m_CommHandle, "MODE", pxTERM->wifi.apMode);
		if(pxTERM->wifi.dhcp) {
			Comm_WriteParam(m_CommHandle, "DHCP", "ENABLE");
		} else {
			Comm_WriteParam(m_CommHandle, "DHCP", "DISABLE");
			Comm_WriteParam(m_CommHandle, "IP", pxTERM->wifi.ip.ip);
			Comm_WriteParam(m_CommHandle, "GATEWAY", pxTERM->wifi.ip.gateway);
			Comm_WriteParam(m_CommHandle, "MASK", pxTERM->wifi.ip.mask);
			Comm_WriteParam(m_CommHandle, "DNS1", pxTERM->wifi.ip.dns);
			Comm_WriteParam(m_CommHandle, "DNS2", pxTERM->wifi.ip.dns);
		}
		break;
	}
	
	Comm_WriteParam(m_CommHandle, "SERVER_IP", pxTERM->hostIP[0]);
	Comm_WriteParam(m_CommHandle, "SERVER_PORT", pxTERM->hostPort[0]);
}

int Comm_PreInit(uint timeMS)
{
	int iRet;
	uint isWifiEnable = 0;
	uint isConnectAp = 0;
	uint start = LvosSysTick();
	ST_PARAM * pxTERM = Param_GetTermParam();
		
	COMM_STRLOG("pxTERM->commMode = %d", pxTERM->commMode);
	COMM_STRLOG("pxTERM->wifi.apName = %s", pxTERM->wifi.apName);
	
	if(pxTERM->commMode & COMM_WIFI) {
		if(m_CommHandle >= 0) {
			LvosCommRelease((uint)m_CommHandle, 1);
			m_CommHandle= -1;
		}
		// WIFI
		m_CurCommMode = COMM_WIFI;
		m_CommHandle = Sys_GetHandle("WIFI");
		COMM_STRLOG("m_CommHandle = %d", m_CommHandle);
		if(m_CommHandle >= 0) {
			iRet = Sys_WriteParam(HWND_APP, "ROUTE", "WIFI");
		} else {
			m_CurCommMode = COMM_GPRS;
			m_CommHandle = Sys_GetHandle("WLAN0");
			iRet = Sys_WriteParam(HWND_APP, "ROUTE", "GPRS");
		}
	} else {
		// only for GPRS
		m_CurCommMode = COMM_GPRS;
		m_CommHandle = Sys_GetHandle("WLAN0");
		iRet = Sys_WriteParam(HWND_APP, "ROUTE", "GPRS");
	}

	switch (m_CurCommMode)
	{
	case COMM_GPRS:
		Sys_WriteParam(m_CommHandle, "PIN", "card");
		Sys_WriteParam(m_CommHandle, "APN", "CMNET");
		Sys_WriteParam(m_CommHandle, "PROTOCOL", "TCP");
		break;
		
	case COMM_WIFI:
		Sys_WriteParam(m_CommHandle, "SSID", pxTERM->wifi.apName);
		Sys_WriteParam(m_CommHandle, "KEY", pxTERM->wifi.apKey);
		Sys_WriteParam(m_CommHandle, "MODE", pxTERM->wifi.apMode);
		Sys_WriteParam(m_CommHandle, "DHCP", "ENABLE");
		break;
	}
	
	if(timeMS > 0) {
		if(m_CurCommMode == COMM_WIFI) {
			Comm_WaitWIFIReady(timeMS);
		} else {
			Comm_WaitGPRSReady(timeMS);
		}
	}
	
	return m_CurCommMode;
}

void Comm_Init()
{
	ST_PARAM *pxTERM = App_GetTermParam();

	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_INITCOM, 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, PROMPT_WAIT, 0);
	
	Comm_InitInner(pxTERM->commMode);
	if(m_CurCommMode == COMM_WIFI) {
		Comm_WaitWIFIReady(5000);
	} else {
		Comm_WaitGPRSReady(5000);
	}
	
	Comm_UpdateSignal(1);
}

void Comm_UpdateSignal(int isImmediately)
{
	int iRet;
	uint now = LvosSysTick();
	ST_PARAM * pxTERM = Param_GetTermParam();
	
	if(isImmediately || m_uiCommSignal == 0 || (now - m_uiCommSignal) >= m_SignalFresh) {
    	byte *buf = Sys_Malloc(TMP_SIZE);
		memset(buf, 0, TMP_SIZE);
		iRet = LvosSysCfgRead("GPRS_RSSI", buf, TMP_SIZE);
		COMM_STRLOG("LvosSysCfgRead(GPRS_RSSI) = %d, %s", iRet, buf);

		if(pxTERM->commMode & COMM_WIFI) {
			memset(buf, 0, TMP_SIZE);
			iRet = Sys_ReadParam(Sys_GetHandle("WIFI"), "STATUS", buf, TMP_SIZE);
			COMM_STRLOG("Sys_ReadParam(WIFI, STATUS) = %d, %s", iRet, buf);
		}
		Sys_Free(buf);
		
		m_uiCommSignal = LvosSysTick();
		if(m_SignalFresh < 10000) {
			m_SignalFresh += 1000;
		}
	}
}

int Comm_GPRSSignal()
{
	int iRet;
	byte buf[20];
	
	memset(buf, 0, sizeof(buf));
	iRet = LvosSysCfgRead("GPRS_RSSI", buf, sizeof(buf));
	// COMM_STRLOG("LvosSysCfgRead = %d, buf= %s\r\n", iRet, buf);
	iRet = atoi(buf);
	if(iRet == 90)                            return COMM_RET_SIM;
	else if((iRet>=32)||(iRet==0))		      return 5;
	else if(iRet<=3) 				  		  return 4;
	else if(iRet<=8) 				  		  return 3;
	else if(iRet<=13)				  		  return 2;
	else if(iRet<=17)				  		  return 1;
	else if(iRet<30)				  		  return 0;
	
	return 0;
}

int Comm_WifiSignal()
{
	int iRet;
	uint wifiStat = 0;
	uint apStat = 0;
	int  signal = 0;
	
	iRet = Comm_GetWifiStatus(m_CommHandle, &wifiStat, &apStat, &signal);
	if(iRet < 0) {
		return 5;
	}
	
	if(wifiStat != 1 || apStat != 1 || signal == 0) {
		return 5;
	}
	
	return 0;
}

int Comm_WaitSignal(uint timeMS, uint start, uint isCancel)
{
	int iRet;
	int  iLeft = 0;
	
	// wait for signal
	while(1)
	{
		uint status=0;
		if(isCancel && Ui_GetKey() == KB_CANCEL) {
			return COMM_RET_CANCEL;
		}
		
		iRet = (LvosSysTick() - start);
		if(timeMS > iRet && iLeft != (timeMS -iRet) / 1000) {
			char left[4];
			iLeft = (timeMS -iRet) / 1000;
			sprintf(left, "%02d", iLeft);
			Ui_ClearLine(2, 2);
			Ui_DispTextLineAlign(2, DISPLAY_CENTER, left, 0);
		}
		if(timeMS < iRet) {
			return COMM_RET_SIGNAL;
		}
		
		switch (m_CurCommMode)
		{
		case COMM_WIFI:
			if(Comm_WifiSignal() != 5) {
				return COMM_RET_OK;
			}
			break;
			
		default:
			iRet = Comm_GPRSSignal();
			if(iRet == COMM_RET_SIM) {
				return COMM_RET_SIM;
			}
			if(iRet != 5) {
				return COMM_RET_OK;
			}
			break;
		}
		
		LvosSysDelayMs(100);
	}
	
	return COMM_RET_SIGNAL;
}

int Comm_ConnectPPP(uint timeMS, uint start, uint isCancel)
{
	int iRet;
	int iLeft = 0;
	int pppStart = 0;
	int isWifiEnable = 0;
	int isConnectAp = 0;
	int isPPP = 0;
	ST_PARAM *pxTERM = App_GetTermParam();

RETRY:
	pppStart = LvosSysTick();
	iLeft = (timeMS + start - pppStart) / 1000;
	
	// wait PPP
	while(1)
	{
		uint status=0;
		if(isCancel && Ui_GetKey() == KB_CANCEL) {
			return COMM_RET_CANCEL;
		}

		iRet = (timeMS + start - LvosSysTick()) / 1000;
		if(timeMS > iRet && iLeft != iRet ) {
			char left[4];
			iLeft = iRet;
			sprintf(left, "%d", iLeft);
			Ui_ClearLine(2, 2);
			Ui_DispTextLineAlign(2, DISPLAY_CENTER, left, 0);
		}
		
		if(m_CurCommMode == COMM_WIFI) {
			uint wifiStat = 0;
			uint apStat = 0;
			Comm_GetWifiStatus(m_CommHandle, &wifiStat, &apStat, NULL);
			if(!isWifiEnable && !wifiStat) {
				// start WIFI model
				Comm_WriteParam(m_CommHandle, "ENABLE", "1");
				isWifiEnable = 1;
			}
			if(wifiStat && !apStat && !isConnectAp ) {
				// connect AP
				Comm_WriteParam(m_CommHandle, "CONNECT_AP", "1");
				isConnectAp = 1;
			}
			if(wifiStat && apStat) {
				COMM_STRLOG("WIFI Ready");
				return COMM_RET_OK;
			}
		} else {
			/*if(isSSL){
				iRet = gprs_net_is_ready();
		        if (iRet == 1) {
					COMM_STRLOG("gprs_net ready");
					return COMM_RET_OK;
		        }
			}else*/{
				iRet = LvosCommCheck((uint)m_CommHandle, &status);
				if(iRet < 0) {
					COMM_STRLOG("LvosCommCheck(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
					return COMM_RET_FAIL;
				}
				if(status & 0x08) {
					COMM_STRLOG("GPRS Ready");
					return COMM_RET_OK;
				}
				if(!isPPP) {
					iRet = LvosCommRequest((uint)m_CommHandle);
					COMM_STRLOG("LvosCommRequest(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
					if(iRet >= 0) {
						isPPP ++;
					}
				}
			}
		}
		iRet = LvosSysTick();
		if(((iRet - pppStart) > 20000) || (timeMS < (iRet -start))) {
			return COMM_RET_FAIL;
		}
		
		LvosSysDelayMs(200);
	}
	
FAILED:

	return COMM_RET_FAIL;
}

int Comm_ConnectHttpHost(uint timeMS, uint start, uint isCancel, const char * host, const char * port)
{
	int iRet;
	uint tcpStart;
	int  iLeft;
	ST_PARAM *pxTERM = App_GetTermParam();
	
	Sys_WriteParam(m_CommHandle, "SERVER_IP", host);
	COMM_STRLOG("SERVER_IP = %s", host);
	Sys_WriteParam(m_CommHandle, "SERVER_PORT", port);
	COMM_STRLOG("SERVER_PORT = %s", port);

	if(m_CurCommMode == COMM_WIFI) {
		iRet = LvosCommRequest((uint)m_CommHandle);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommRequest(%d) = %d", m_CommHandle, iRet);
			return COMM_RET_FAIL;
		}
	}
	
	iRet = LvosCommConnect(m_CommHandle);
	if(iRet < 0) {
		COMM_STRLOG("LvosCommConnect(%d) = %d", m_CommHandle, iRet);
		return COMM_RET_FAIL;
	}
	
	tcpStart = LvosSysTick();
	
	iLeft = (timeMS + start - tcpStart) / 1000;
	// 等待TCP
	while(1)
	{
		uint status=0;
		if(isCancel && Ui_GetKey() == KB_CANCEL) {
			return COMM_RET_CANCEL;
		}
		
		iRet = (timeMS + start - LvosSysTick()) / 1000;
		if(timeMS > iRet && iLeft != iRet) {
			char left[4];
			iLeft = iRet;
			sprintf(left, "%02d", iLeft);
			Ui_ClearLine(2, 2);
			Ui_DispTextLineAlign(2, DISPLAY_CENTER, left, 0);
		}
		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
			return COMM_RET_FAIL;
		}
		
		if(status & 0x01) {
			COMM_STRLOG("status = 0x%x,time=%d", status,LvosSysTick());
			return m_CommHandle;
		}		
		
		iRet = LvosSysTick();
		
		if(timeMS < (iRet - start)) {
			return COMM_RET_TIMEOUT;
		}
		
		if((iRet - tcpStart) > 10000) {
			return COMM_RET_RETRY;
		}
				
		LvosSysDelayMs(10);
	}
	
	return COMM_RET_FAIL;
}

int Comm_CloseTcp(uint timeMS, uint start, uint isCancel)
{
	int iRet;
	int  iLeft;
	
	iLeft = (timeMS + start - LvosSysTick()) / 1000;
	
	iRet = LvosCommDisconnect((uint)m_CommHandle);
	COMM_STRLOG("LvosCommDisconnect(%d) = %d", m_CommHandle, iRet);
	
	while(1) {
		uint status=0;
		
		if(isCancel && Ui_GetKey() == KB_CANCEL) {
			return COMM_RET_CANCEL;
		}
		
		iRet = (timeMS + start - LvosSysTick()) / 1000;
		if(timeMS > iRet && iLeft != iRet) {
			char left[4];
			iLeft = iRet;
			sprintf(left, "%02d", iLeft);
			Ui_ClearLine(2, 2);
			Ui_DispTextLineAlign(2, DISPLAY_CENTER, left, 0);
		}
		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
			return COMM_RET_FAIL;
		}
		
		if( !(status & 0x01) ) {
			return COMM_RET_OK;
		}
		
		iRet = LvosSysTick();
		if(timeMS < (iRet - start)) {
			return COMM_RET_TIMEOUT;
		}
		
		LvosSysDelayMs(100);
	}
	
	return COMM_RET_FAIL;
}

int Comm_ConnectHttp(uint timeMS, uint isCancel, const char * host, const char * port)
{
	int iRet = 0;
	int  retryPPP = 0;
	uint start = 0;
	uint status=0;
	uint idx = 0;
	ST_PARAM * pxTERM = App_GetTermParam();
	
	Ui_ClearKey();
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, PROMPT_CONNECTNET, 0);
	
RETRYPPP:
	start = LvosSysTick();
	if(m_CurCommMode == COMM_GPRS) {
		iRet = Comm_WaitSignal(timeMS, start, isCancel);
		if(iRet != COMM_RET_OK) {
			goto FAIL;
		}
	}
	// update signal
	Comm_UpdateSignal(0);
	
RETRYTCP:
	// PPP connect 
	iRet = Comm_ConnectPPP(timeMS, start, isCancel);
	if(iRet != COMM_RET_OK) {
		goto FAIL;
	}
	
	if(m_CurCommMode == COMM_WIFI) {
		iRet = Comm_WaitSignal(timeMS, start, isCancel);
		if(iRet != COMM_RET_OK) {
			goto FAIL;
		}
	}
	
	iRet = Comm_ConnectHttpHost(timeMS, start, isCancel, host, port);
	if(iRet >= 0) {
		return iRet;
	}	
FAIL:
	LvosCommCheck((uint)m_CommHandle, &status);
	COMM_STRLOG("Failed comm status = 0x%08X", status);	
	iRet = Comm_CloseTcp(timeMS, start, isCancel);	
	Sys_BeepFAIL();
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	switch(iRet)
	{
	case COMM_RET_SIM:
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_NOSIM, 0);
		break;
	
	case COMM_RET_SIGNAL:
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_SIGNALWEAK, 0);
		break;
	
	case COMM_RET_TIMEOUT:
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_CONTIMEOUT, 0);
		break;
	
	case COMM_RET_CANCEL:
		return iRet;
		
	default:
		Ui_DispTextLineAlign(2, DISPLAY_CENTER, PROMPT_CONFAIL, 0);
		break;
	}
	Ui_WaitKey(APP_PROMPT_TIMEOUT);
	
	return iRet;
}

int  Comm_SendHttp(byte * data, uint dataLen)
{
	return LvosCommSend((uint)m_CommHandle, data, dataLen);
}

int  Comm_RecvHttp(byte * data, uint dataSize)
{
	return LvosCommRecv((uint)m_CommHandle, data, dataSize);
}

void Comm_Close()
{	
	int iRet;
	uint start;
	
	iRet = LvosCommDisconnect((uint)m_CommHandle);
	COMM_STRLOG("LvosCommDisconnect(%d) = %d", m_CommHandle, iRet);
	start = LvosSysTick();
	
	while(1) {
		uint status=0;
		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
			return;
		}
		
		if( !(status & 0x01) ) {
			return;
		}
		
		iRet = (LvosSysTick() - start);
		if(2000 < iRet) {
			return;
		}
		
		LvosSysDelayMs(100);
	}
	
	return;
}
#if defined(BTCOM)	
int Commbt_checkstatus(){
	u32 status=0,ret=COMM_RET_FAIL;
	ret = LvosCommCheck(Sys_GetHandle("BT"),&status);
	if(ret != 0)
		return COMM_RET_FAIL;
	if(status & 0x01)
		return COMM_RET_OK;
	else
		return COMM_RET_FAIL;
}

int  Commbt_Send(byte * data, uint dataLen)
{
	int iRet;
	int sendLen;
	char left[4];

	if(Commbt_checkstatus()!=COMM_RET_OK){		
		COMM_STRLOG("comm not connect");
		return COMM_RET_FAIL;
	}
	for(sendLen = 0; sendLen < dataLen; ) {
		if((dataLen - sendLen) > 1024) {
			iRet = LvosCommSend(Sys_GetHandle("BT"), data + sendLen, 1024);
		} else {
			iRet = LvosCommSend(Sys_GetHandle("BT"), data + sendLen, dataLen - sendLen);
		}
		if(iRet < 0) {
			return COMM_RET_FAIL;
		}
		sendLen += iRet;
	}
	return dataLen;
}

int  Commbt_Recv(byte * data, uint dataSize)
{
	int iRet=COMM_RET_FAIL;
	int dataLen=0,i;
	byte lrc;
	char left[4];
	int offset=0;

	if(Commbt_checkstatus()!=COMM_RET_OK){		
		return COMM_RET_FAIL;
	}
	iRet = LvosCommRecv(Sys_GetHandle("BT"), data+offset, 3);
	if(iRet < 0 || data[0] != 0x4D || iRet != 3) {
		return COMM_RET_FAIL;
	}
	offset +=iRet;
	dataLen = Utils_Hex2Int(data+1, 2);
	iRet = LvosCommRecv(Sys_GetHandle("BT"), data+offset, dataLen+1);
	if(iRet !=  dataLen+1) {
		return COMM_RET_FAIL;
	}
	offset+=iRet;		
	for(i=0;i<(offset-1);i++){
		lrc ^= data[i];
	}
	
	if(lrc != data[offset-1]){
		return COMM_RET_FAIL;
	}
	return offset;
}
#endif

#if defined(USBCOM)
int  Commu_Recv(int handle,byte * data, uint dataSize, uint timeMS)
{
	int iRet;
	int recvLen;
	uint start;
	int iLeft;
	char left[4];
		
	iLeft = 0;
	start = LvosSysTick();
	for(recvLen = 0;  recvLen < dataSize; ) {		
		iRet = LvosSysTick() - start;
		if(timeMS < iRet) {
			return APP_RET_TIMEOUT;
		}
		if(Ui_GetKey() == KB_CANCEL) {
			return APP_RET_CANCLE;
		}
		iRet = LvosCommRecv(handle, data + recvLen, 1);
		if(iRet < 0) {
			continue;			
		}
		recvLen += iRet;
		
		if(recvLen > 0 && data[recvLen-1] == '!'){
			break;
		}
	}
	COMM_STRLOG("Commusb_Recv data:%s,len=%d",data,recvLen);
	return recvLen;
}

#endif


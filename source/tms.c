#include <libapp.h>
#include <ctype.h>
#include <cJSON.h>

#include "app.h"
#include "http.h"
#include "tms.h"
#include "param.h"
#include "comm.h"
#include "Func.h"
#include "limitapi.h"

#ifdef TMS_DEBUG
#define TMS_STRLOG DBG_STR
#define TMS_HEXLOG DBG_HEX
#else
#define TMS_STRLOG  
#define TMS_HEXLOG 
#endif

#define OS_TYPE 			"TMSAGENT"
#define OS_ID 			    "TMSAGENT"
#define OS_VERISON 			"1.0"
#define OS_VERISON_CODE		4
#define OS_OTA_TOKEN        "vwUsJLVF4R3on3ISgq6ACHdS1/jvul1P"

#define TMS_HOST_NAME		"ota.althico.com"
#define TMS_PORT			"8300"

#define PATH_URL_GET_DOWN_INFO	"/api/v1/device/ota/dev/%s/%s/%s/%s"
#define PATH_URL_UPLOAD_STATUS	"/api/v1/device/ota/task/%s/%s/%s/%s/%s"
#define PATH_URL_GET_WIFI_INFO	"/api/v1/device/ota/dev/wifi/%s/%s/%s/%s"

#define OTA_REQ_STR "{\"name\":\"TMSAGENT\",\"app_id\":\"TMSAGENT\",\"updater\":\"app\",\"os\": {\"app_version\":\"V1.0_01\"},\"apps\":[%s]}"
#define OTA_REQ_APPS "{\"app_id\": \"%s\",\"app_version\":\"%s\"}"
#define OTA_REQ_NOTICE "{\"code\":\"%d\",\"msg\":\"%s\"}"
#define OTA_REQ_WIFI "{\"n\":\"%s\"}"

static byte m_RecBufferr[TMS_FLASH_RECSIZE]; // 满1K写入一次
static int m_RecOffset=0;	// 当前不足一条记录的数据长度

static int Tms_GenToken(char *path, char *data, char *token)
{
	int len;
	byte hash[40];
    byte * buf;
	
	len = strlen(data) + strlen(path) + 200;
	buf = Sys_Malloc(len);
	if(buf == NULL) {
		return -1;
	}
	memset(buf, 0, len);
	len = sprintf(buf, "%s-%s-%s", path, data, OS_OTA_TOKEN);
	memset(hash, 0, sizeof(hash));
	len = LvosSha(SHA256, hash, len, buf);
	Sys_Free(buf);
	if(len < 0) {
		return -2;
	}
	
	sprintf(token, "%s-", OS_ID);
	Utils_Bcd2Asc(hash, 32, token+strlen(token));
	TMS_STRLOG("token: %s", token);
		
	return strlen(token);
}


// 清楚TMS标记
static void Tms_ClearFlag(ST_OSTMSFLAG * tmsFlag)
{
	// 清除tms更新成功的标志, 保留版本信息
	tmsFlag->tmsflag = 0;
	tmsFlag->count = 0;
	tmsFlag->offset = 0;
	LvoslitWriteTmsShare(tmsFlag);
}

// 检查系统是否支持应用层TMS
static int Tms_CheckSupport()
{
	ST_OSTMSFLAG tmsFlag;
	
	LvoslitReadTmsShare(&tmsFlag);
	if(tmsFlag.VerDate > 0) {
		return 1;
	}
	
	return 0;
}

static int Tms_ShowMsg(const char * title, const char * text, uint timeMS)
{
	int i=0;
	int j=0;
	int line=3;
	char Str[30];

	Ui_Clear();
	Ui_DispTitle(title);
	
	if(strlen(text) <= Ui_GetLineEnNum()) {
		Ui_DispTextLineAlign(line, DISPLAY_CENTER, text, 0);
		return Ui_WaitKey(timeMS);
 	}
	
	memset(Str,0,sizeof(Str));
	
	while(text[i] && line < (Ui_GetLineNum() - 1))
	{
		if(j >= Ui_GetLineEnNum()) {
			Ui_DispTextLineAlign(line++, DISPLAY_CENTER, Str, 0);
			memset(Str,0,sizeof(Str));
			j = 0;
		}
		if(text[i] == '\n') {
			Ui_DispTextLineAlign(line++, DISPLAY_CENTER, Str, 0);
			memset(Str, 0, sizeof(Str));
			j = 0;
			i ++;
		} else if(text[i] & 0x80) {
			if((j + 2) > Ui_GetLineEnNum()) { 
				// 最后一个是汉字超过可显长度需要先显示
				Ui_DispTextLineAlign(line++, DISPLAY_CENTER, Str, 0);
				memset(Str, 0, sizeof(Str));
				j = 0;
			}
			memcpy(Str+j, text+i, 2);
			i += 2;
			j += 2;
		} else {
			Str[j++]=text[i++];	
		}
	}
		
	if(strlen(Str)) {
		Ui_DispTextLineAlign(line, DISPLAY_CENTER, Str, 0);
	}
	
	return Ui_WaitKey(timeMS);
}

static void Tms_ShowProgress(int ratio)
{
	int Width = 0;
	int line;
	char Str[20];
	
	// 倒数第二行百分比，最后一行进度条
	memset(Str, 0, sizeof(Str));
	
	Width=Ui_GetLcdWidth();
	if(ratio < 100) {
		Width=Width*ratio/100;
	}
	itoa(ratio, Str, 10);
	strcat(Str, "%%");
	
	line = Ui_GetLineNum();
	switch( Ui_GetLcdHeight() ) {
	case 64:
		Ui_ClearLine(3, 3);
		Ui_DispTextLineAlign(3, DISPLAY_CENTER, Str, 0);
		break;
	
	case 96:
		Ui_ClearLine((line-3), (line-3));
		Ui_DispTextLineAlign((line-3), DISPLAY_CENTER, Str, 0);
		Ui_ClearBlockColor(0, (line-2)* Ui_GetCharHeight(), Ui_GetLcdWidth(), Ui_GetCharHeight(), DISPLAY_COLOR_WHITE);
		if(Width) {
			Ui_ClearBlockColor(0, (line-2)*Ui_GetCharHeight(), Width, Ui_GetCharHeight(), DISPLAY_COLOR_BLACK);
		}
		break;
		
	default:
		Ui_ClearLine((line-3), (line-3));
		Ui_DispTextLineAlign((line-3), DISPLAY_CENTER, Str, 0);
		Ui_ClearBlockColor(0, (line-2)* Ui_GetCharHeight(), Ui_GetLcdWidth(), Ui_GetCharHeight(), RGB(199,200,244));
		if(Width) {		
			Ui_ClearBlockColor(0, (line-2)*Ui_GetCharHeight(), Width, Ui_GetCharHeight(), RGB(0,255,255));
		}
		break;
	}
}


static int Tms_GetCode(cJSON * pxJson, int * code)
{
	cJSON * pxItem = NULL;

	pxItem = cJSON_GetObjectItem(pxJson, "rsp_code");
	if(pxItem == NULL) return -1;

		
	if(pxItem->type == cJSON_Number) {
		*code = pxItem->valueint;
		return 0;
	} 
	
	if(pxItem->type == cJSON_String) {
		*code = atoi(pxItem->valuestring);
		return 0;
	}

	return -2;
}

static int Tms_GetTaskId(cJSON * pxJson, char * taskId)
{
	cJSON * pxItem = NULL;

	pxItem = cJSON_GetObjectItem(pxJson, "task_id");
	if(pxItem == NULL || pxItem->type != cJSON_String) return -1;
	if(strlen(pxItem->valuestring) > 32) return -2;
	strcpy(taskId, pxItem->valuestring);
	
	return 0;
}

int Tms_GetStringData(cJSON * pxJson, char * keyid,char * value)
{
	cJSON * pxItem = NULL;

	pxItem = cJSON_GetObjectItem(pxJson, keyid);
	if(pxItem == NULL || pxItem->type != cJSON_String) return -1;
	strcpy(value, pxItem->valuestring);
	
	return 0;
}

static int Tms_GetTmsInfo(cJSON * pxJson, ST_TMSINFO * tmsInfo)
{
	cJSON * pxItem = NULL;
	cJSON * pxApp = NULL;
	
	pxItem = cJSON_GetObjectItem(pxJson, "rsp_data");
	if(pxItem == NULL || pxItem->type != cJSON_Object) return -1;
	
	pxApp = cJSON_GetObjectItem(pxItem, "apps");
	if(pxApp == NULL || pxApp->type != cJSON_Array) return -1;
	
	pxItem = cJSON_GetArrayItem(pxApp, 0);
	if(pxItem == NULL || pxItem->type != cJSON_Object) return -1;
	
	pxApp  = cJSON_GetObjectItem(pxItem, "bin");
	if(pxApp == NULL || pxApp->type != cJSON_Object) return -1;
	
	pxItem = cJSON_GetObjectItem(pxApp, "url");
	if(pxItem == NULL || pxItem->type != cJSON_String) return -1;
	if(strlen(pxItem->valuestring) > sizeof(tmsInfo->Url)) return -1;
	strcpy(tmsInfo->Url, pxItem->valuestring);
	
	pxItem = cJSON_GetObjectItem(pxApp, "size");
	if(pxItem == NULL) return -1;
	if(pxItem->type == cJSON_Number) {
		tmsInfo->uFileSize = (uint)pxItem->valueint;
	} else if(pxItem->type == cJSON_String) {
		tmsInfo->uFileSize = atoi(pxItem->valuestring);
	} else {
		return -1;
	}
	
	return 0;
}

#define URL_LEN 256
#define DATA_LEN 1024
static int Tms_CheckVersoin(const char * appId, const char * appVersion, ST_TMSINFO * tmsInfo)
{
	int iRet;
	int code = 0;
	char * tmp = NULL; 
	char * url =NULL;
	char * data = NULL;
	cJSON * pxJSON = NULL;
	HTTPHANDLE request;

	tmp = Sys_Malloc(URL_LEN);
	if(tmp == NULL) {
    	TMS_STRLOG("Sys_Malloc url failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}

	url = Sys_Malloc(URL_LEN);
	if(url == NULL) {
    	TMS_STRLOG("Sys_Malloc url failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	data = Sys_Malloc(DATA_LEN);
	if(data == NULL) {
    	TMS_STRLOG("Sys_Malloc data failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	iRet = Http_InitHandle(&request, TMS_DOWNLOAD_SIZE);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	memset(tmp, 0, URL_LEN);
#ifdef WP90GWCC
	strcpy(tmp, "ALTHICO");
	strcpy(tmp+33, "WP90");
	strcpy(tmp+66, "GWCC");
#else
	Sys_ReadVendor(tmp);
	Sys_ReadModel(tmp+33);
	Sys_ReadPN(tmp+66);
#endif
	Sys_ReadSN(tmp+99);
	memset(url, 0, URL_LEN);
	snprintf(url, URL_LEN-1, PATH_URL_GET_DOWN_INFO, tmp, tmp+33, tmp+66, tmp+99);
	iRet = Http_InitRequest(&request, "POST", TMS_HOST_NAME, TMS_PORT, url);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "close");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/json");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");
	// create requset body
	memset(tmp, 0, URL_LEN);
	snprintf(tmp, URL_LEN-1, OTA_REQ_APPS, appId, appVersion);
	memset(data, 0, DATA_LEN);
	snprintf(data, DATA_LEN-1, OTA_REQ_STR, tmp);
	if(strlen(data) > request.packSize - request.packLen) {
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	// calc token
	memset(tmp, 0, URL_LEN);
	Tms_GenToken(url, data, tmp);
	Http_SetHeader(&request, "Token", tmp);
	// set body
	Http_SetRequestBody(&request, data, strlen(data));
	
	//TMS_HEXLOG("HTTP requset:\r\n%s", request.pack, request.packLen);
	TMS_STRLOG("HTTP requset:\r\n%s", request.pack);
	
	Ui_Clear();
	Ui_DispTitle("TMS UPDATE");
	iRet = Http_TmsConnect(TMS_HOST_NAME, TMS_PORT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Connect = %d", iRet);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	request.socket = iRet;
	
	iRet = Http_TmsSend(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsSend = %d", iRet);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_TmsRecv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsRecv = %d", iRet);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	
   	// TMS_HEXLOG("response pack:\r\n", request.pack, request.packLen);
   	
	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	memset(data, 0, DATA_LEN);
	iRet = Http_GetResponseBody(&request, data, DATA_LEN-1);
	if(iRet < 0) {
	    TMS_STRLOG("Http_GetResponseBody = %d", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
   	// TMS_HEXLOG("response data:\r\n", data, strlen(data));
	pxJSON = cJSON_Parse(data);
	if(pxJSON == NULL) {
	    TMS_STRLOG("cJSON_Parse ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}

	// rsp_code: “0”-无需更新，”1”-强制更新，“2”-正常更新，其他-错误
	code = -1;
	iRet = Tms_GetCode(pxJSON, &code);
	if(iRet != 0) {
	    TMS_STRLOG("get rsp_code ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	TMS_STRLOG("code = %d", code);
	if(code > 0) {
		iRet = Tms_GetTaskId(pxJSON, tmsInfo->taskId);
		if(iRet != 0) {
		    TMS_STRLOG("get task id ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
		iRet = Tms_GetTmsInfo(pxJSON, tmsInfo);
		if(iRet != 0) {
		   	TMS_STRLOG("get app info ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
	}
	
	// 成功返回更新代码
	iRet = code;

FINISH:
	Http_Release(&request);
	
	if(pxJSON != NULL) {
		cJSON_Delete(pxJSON);
	}
	if(tmp != NULL) {
		Sys_Free(tmp);
	}
	
	if(url != NULL) {
		Sys_Free(url);
	}
	
	if(data != NULL) {
		Sys_Free(data);
	}
	
	return iRet;
}

int Tms_GetServerTime(void)
{
	int iRet;
	int code = 0;
	char * data = NULL;
	cJSON * pxJSON = NULL;
	HTTPHANDLE request;
	char url[50];
	char tmp[URL_LEN];

	memset(url,0,sizeof(url));
	strcpy(url,"/time/v1/device/ota/getSystemTime");
	data = Sys_Malloc(DATA_LEN);
	if(data == NULL) {
    	TMS_STRLOG("Sys_Malloc data failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	iRet = Http_InitHandle(&request, TMS_FLASH_PAGESIZE);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	iRet = Http_InitRequest(&request, "POST", TMS_HOST_NAME, TMS_PORT, url);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "keep-alive");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/json");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");	
	// calc token
	Http_SetHeader(&request, "Token", "fW9DMOn8TWLNoEb9SqXdEoBDPb47ysIX");
	// set body
	Http_SetRequestBody(&request, NULL, 0);
	TMS_STRLOG("HTTP requset:\r\n%s", request.pack);
	
	Ui_Clear();
	Ui_DispTitle("TMS-TIME UPDATE");
	iRet = Http_TmsConnect(TMS_HOST_NAME, TMS_PORT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Connect = %d", iRet);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	request.socket = iRet;
	
	iRet = Http_TmsSend(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsSend = %d", iRet);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_TmsRecv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsRecv = %d", iRet);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	   	
	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	memset(data, 0, DATA_LEN);
	iRet = Http_GetResponseBody(&request, data, DATA_LEN-1);
	if(iRet < 0) {
	    TMS_STRLOG("Http_GetResponseBody = %d", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
   	TMS_STRLOG("response data:\r\n%s", data);
	pxJSON = cJSON_Parse(data);
	if(pxJSON == NULL) {
	    TMS_STRLOG("cJSON_Parse ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	memset(tmp, 0, URL_LEN);
	iRet = Tms_GetStringData(pxJSON, "rsp_code",tmp);
	if(iRet != 0) {
	    TMS_STRLOG("get rsp_code ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	TMS_STRLOG("rsp_code = %s", tmp);
	if(strcmp(tmp,"SUCCESS") == 0) {
		
		memset(tmp, 0, URL_LEN);
		iRet = Tms_GetStringData(pxJSON, "rsp_data",tmp);
		if(iRet != 0) {
			TMS_STRLOG("get rsp_data ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
		TMS_STRLOG("rsp_data = %s", tmp);
		/*更新设备时间*/
		Sys_SetTime(tmp, tmp+8);
	}
FINISH:
	Http_Release(&request);
	
	if(pxJSON != NULL) {
		cJSON_Delete(pxJSON);
	}
	
	if(data != NULL) {
		Sys_Free(data);
	}
	
	return 0;
}

int Tms_GetServerWifiInfo(void)
{
	int iRet;
	int code = 0;
	char * tmp = NULL; 
	char * url =NULL;
	char * data = NULL;
	cJSON * pxJSON = NULL;
	cJSON * pxdataJSON = NULL;
	HTTPHANDLE request;
	ST_PARAM  * pxTerm = App_GetTermParam();
	
	tmp = Sys_Malloc(URL_LEN);
	if(tmp == NULL) {
    	TMS_STRLOG("Sys_Malloc url failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}

	url = Sys_Malloc(URL_LEN);
	if(url == NULL) {
    	TMS_STRLOG("Sys_Malloc url failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	data = Sys_Malloc(DATA_LEN);
	if(data == NULL) {
    	TMS_STRLOG("Sys_Malloc data failed");
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	iRet = Http_InitHandle(&request, TMS_DOWNLOAD_SIZE);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	memset(tmp, 0, URL_LEN);
	Sys_ReadVendor(tmp);
	Sys_ReadModel(tmp+33);
	Sys_ReadPN(tmp+66);
	Sys_ReadSN(tmp+99);
	memset(url, 0, URL_LEN);
	snprintf(url, URL_LEN-1, PATH_URL_GET_WIFI_INFO, tmp, tmp+33, tmp+66, tmp+99);
	iRet = Http_InitRequest(&request, "POST", TMS_HOST_NAME, TMS_PORT, url);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "close");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/json");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");
	// create requset body
	memset(tmp, 0, URL_LEN);
	App_ReadSN(tmp);
	memset(data, 0, DATA_LEN);
	snprintf(data, DATA_LEN-1, OTA_REQ_WIFI, tmp);
	
	// calc token
	memset(tmp, 0, URL_LEN);
	Tms_GenToken(url, data, tmp);
	Http_SetHeader(&request, "Token", tmp);
	// set body
	Http_SetRequestBody(&request, data, strlen(data));
	
	TMS_STRLOG("HTTP requset:\r\n%s", request.pack);
	
	Ui_Clear();
	Ui_DispTitle("WIFI SETTING");
	iRet = Http_TmsConnect(TMS_HOST_NAME, TMS_PORT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Connect = %d", iRet);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	request.socket = iRet;
	
	iRet = Http_TmsSend(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsSend = %d", iRet);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_TmsRecv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsRecv = %d", iRet);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	
   	TMS_STRLOG("response pack:\r\n%s", request.pack);
   	
	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	memset(data, 0, DATA_LEN);
	iRet = Http_GetResponseBody(&request, data, DATA_LEN-1);
	if(iRet < 0) {
	    TMS_STRLOG("Http_GetResponseBody = %d", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
   	TMS_HEXLOG("response data:\r\n", data, strlen(data));
	pxJSON = cJSON_Parse(data);
	if(pxJSON == NULL) {
	    TMS_STRLOG("cJSON_Parse ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	memset(tmp, 0, URL_LEN);
	iRet = Tms_GetStringData(pxJSON, "rsp_code",tmp);
	if(iRet != 0) {
	    TMS_STRLOG("get rsp_code ERROR");
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	TMS_STRLOG("rsp_code = %s", tmp);
	if(strcmp(tmp,"SUCCESS") == 0) {	
		pxdataJSON = cJSON_GetObjectItem(pxJSON, "rsp_data");
		if(pxdataJSON == NULL || pxdataJSON->type != cJSON_Object){
			iRet = APP_RET_ERR_JP;
			goto FINISH;
		}

		memset(tmp, 0, URL_LEN);
		iRet = Tms_GetStringData(pxdataJSON, "s",tmp);
		if(iRet != 0) {
		    TMS_STRLOG("get s ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
		memset(pxTerm->wifi.apMode, 0, sizeof(pxTerm->wifi.apMode));
		strcpy(pxTerm->wifi.apMode, tmp);

		memset(tmp, 0, URL_LEN);
		iRet = Tms_GetStringData(pxdataJSON, "u",tmp);
		if(iRet != 0) {
		    TMS_STRLOG("get u ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
		memset(pxTerm->wifi.apName, 0, sizeof(pxTerm->wifi.apName));
		strcpy(pxTerm->wifi.apName, tmp);
		
		memset(tmp, 0, URL_LEN);
		iRet = Tms_GetStringData(pxdataJSON, "p",tmp);
		if(iRet != 0) {
		    TMS_STRLOG("get p ERROR");
			iRet = HTTP_ERR_DATA;
			goto FINISH;
		}
		memset(pxTerm->wifi.apKey, 0, sizeof(pxTerm->wifi.apKey));
		strcpy(pxTerm->wifi.apKey, tmp);
		// 成功返回更新代码
		iRet = 0;
		Param_WriteParam();
	}else{
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
FINISH:
	Http_Release(&request);
	
	if(pxJSON != NULL) {
		cJSON_Delete(pxJSON);
	}
	if(pxdataJSON != NULL) {
		cJSON_Delete(pxdataJSON);
	}
	if(tmp != NULL) {
		Sys_Free(tmp);
	}
	
	if(url != NULL) {
		Sys_Free(url);
	}
	
	if(data != NULL) {
		Sys_Free(data);
	}
	
	return iRet;
}

static int Tms_DecodeUrl(const char *url, char * host, char * port, char *path)
{
	int iRet;
	char tmp[12];
	char * pUrl = NULL;
	char * pPort = NULL;
	char * pSplit = NULL;

	TMS_STRLOG("url: %s", url);
	pUrl = strstr(url, "http://");
	if(pUrl == NULL) {
		pUrl = strstr(url, "https://");
		if(pUrl == NULL) {
			pUrl = (char *)url;
		} else {
			pUrl += 8;
		}
	} else {
		pUrl += 7;
	}
	
	pSplit =  strstr(pUrl, "/");
	if(pSplit == NULL) {
		if(strlen(pUrl) > 256) {
			return -1;
		}
		strcpy(host, pUrl);
	} else {
		if(strlen(pSplit) > 256) {
			return -1;
		}
		iRet = (uint)pSplit - (uint)pUrl;
		if(iRet > 256 || iRet < 0) {
			return -1;
		}
		strcpy(path, pSplit);
		memcpy(host, pUrl, iRet);
	}
	pPort = strstr(host, ":");
	if(pPort == NULL) {
		strcpy(port, "80");
	} else {
		*pPort = 0x00;//截断IP地址20220321
		pPort++;
		if(strlen(pPort) > 5) {
			return -1;
		}
		strcpy(port, pPort);
	}
	
	TMS_STRLOG("host: %s", host);
	TMS_STRLOG("port: %s", port);
	TMS_STRLOG("path: %s", path);

	return 0;
}


static void Tms_ClearBuffer()
{
	memset(m_RecBufferr, 0, sizeof(m_RecBufferr));
	m_RecOffset=0;
}

// 保存数据到FLASH
static int Tms_WriteData(void *buf, int len, void * param)
{
	int ret;
	int writeLen = 0;
	ST_TMSINFO * pTms = (ST_TMSINFO *)param;
	
	// TMS_STRLOG("pTms->uFileSize = %d", pTms->uFileSize);
	// TMS_STRLOG("pTms->offset = %d", pTms->offset);
	// TMS_STRLOG("len = %d", len);
	
	// 收到的数据长度+缓冲的数据长度+已写入的数据长度
	Tms_ShowProgress(((len+m_RecOffset+pTms->offset) * 100) / pTms->uFileSize);
	if(pTms->offset == 0) {
		TMS_HEXLOG("first pack:\r\n", buf, 128);
	}
	if(pTms->offset + len >= pTms->uFileSize) {
		TMS_STRLOG("pTms->uFileSize = %d, pTms->offset = %d, len = %d", pTms->uFileSize, pTms->offset, len);
		TMS_HEXLOG("last pack:\r\n", buf+len-128, 128);
	}
	while (writeLen < len) {
		int freeSize;
		freeSize = TMS_FLASH_RECSIZE - m_RecOffset;
		if ((len - writeLen) < freeSize) {
			freeSize = len - writeLen;
		}
		memcpy(m_RecBufferr + m_RecOffset, buf + writeLen, freeSize);
		writeLen += freeSize;
		m_RecOffset += freeSize;
		// 满1K或最后一条写入FLASH
		if ((m_RecOffset == TMS_FLASH_RECSIZE) || ((pTms->offset + m_RecOffset) >= pTms->uFileSize)) {
			ret=LvoslitWriteTmsArea(pTms->offset, m_RecBufferr, TMS_FLASH_RECSIZE);	
			if (ret < 0) {
				TMS_STRLOG("Write Flash Error %d", ret);
				Tms_ShowMsg("TMS UPDATE", "write data fail", 3000);
			}
			pTms->offset += m_RecOffset;
			if (pTms->offset  >= pTms->uFileSize) {
				TMS_STRLOG("write last page, pTms->offset = %d, pTms->uFileSize = %d", pTms->offset, pTms->uFileSize);
			}
			
			Tms_ClearBuffer();
		}
	}
	
	// 4k对齐的地方位置保存断点, 下载完成保存断点
	if((pTms->offset % TMS_FLASH_PAGESIZE == 0) || (pTms->offset >= pTms->uFileSize))
	{
		// 实时保存断点的信息
		File_Write(TMS_BREAKFILE, 0, (char *)pTms, sizeof(ST_TMSINFO));
	}
	
  	 return 0;
}

static int Tms_Download()
{
	int iRet;
	int total = 0;
	int down_len = 0;
	char host[256];
	char port[10];
	char path[256];
	char tmp[100];
	ST_TMSINFO tmsInfo;
	HTTPHANDLE request;

RETRY:
	// 通讯中断从断点处开始下载
	memset((char *)&tmsInfo, 0, sizeof(ST_TMSINFO));
	File_Read(TMS_BREAKFILE, 0, (char *)&tmsInfo, sizeof(ST_TMSINFO));
	if(tmsInfo.offset == 0) {
		LvoslitClearTmsArea(tmsInfo.uFileSize);
	}
	total = tmsInfo.offset;
	if(total >= tmsInfo.uFileSize) {// 下载完成
		return 0;
	}
	tmsInfo.count++; // 重启的次数
	File_Write(TMS_BREAKFILE, 0, (char *)&tmsInfo, sizeof(ST_TMSINFO));
	Tms_ClearBuffer();
	
	TMS_STRLOG("URL:%s\n uFileSize:%d\n offset:%d",tmsInfo.Url, tmsInfo.uFileSize, tmsInfo.offset);
	
	memset(host, 0, sizeof(host));
	memset(port, 0, sizeof(port));
	memset(path, 0, sizeof(path));
	iRet = Tms_DecodeUrl(tmsInfo.Url, host, port, path);
	if(iRet < 0 || strlen(host) == 0 || strlen(port) == 0 || strlen(path) == 0) {
	    TMS_STRLOG("Http_Connect = %d", iRet);
		sprintf(tmp, "error parsing url[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	iRet = Http_InitHandle(&request, TMS_DOWNLOAD_SIZE + 512);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		sprintf(tmp, "init handle err[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_DATA;
		return iRet;
	}
	
	Ui_Clear();
	Ui_DispTitle("remote update");
	iRet = Http_TmsConnect(host, port);
	if(iRet < 0) {
	    TMS_STRLOG("Http_Connect = %d", iRet);
		sprintf(tmp, "connect host error[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	
	request.socket = iRet;
	
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(1, DISPLAY_CENTER, "downloading updates...", 0);
	Tms_ShowProgress((tmsInfo.offset * 100) / tmsInfo.uFileSize);
DOWNLOAD:
	request.dataLength = 0;
	request.headerLength = 0;
	request.isTrunked = 0;
	request.trunckedDataLen = 0;
	request.packLen = 0;
	memset(request.pack, 0, request.packSize);
	
	iRet = Http_InitRequest(&request, "GET", host, port, path);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		sprintf(tmp, "init request err[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "Keep-Alive");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");
	
	if ((tmsInfo.uFileSize - total) > TMS_DOWNLOAD_SIZE) {
		down_len = TMS_DOWNLOAD_SIZE;
	} else {
		down_len = tmsInfo.uFileSize - total;
	}
	
	memset(tmp, 0, sizeof(tmp));
	sprintf(tmp, "bytes=%d-%d", total, total + down_len - 1);
	TMS_STRLOG("Range: %s", tmp);
	
	Http_SetHeader(&request, "Range", tmp);
	
	Http_SetRequestBody(&request, "", 0);
	
	// TMS_HEXLOG("HTTP requset:\r\n%s", request.pack, request.packLen);
	TMS_STRLOG("HTTP request:\r\n%s",request.pack);
	iRet = Http_TmsSend(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Send = %d", iRet);
		sprintf(tmp, "send data err[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_TmsRecv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Recv = %d", iRet);
		sprintf(tmp, "receive data err[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	
	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		sprintf(tmp, "status err[%d]", request.status);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	iRet = Http_DecodeResponseBody(&request, &tmsInfo, Tms_WriteData);
	if(iRet < 0) {
    	TMS_STRLOG("Http_DecodeBody = %d", iRet);
		sprintf(tmp, "update data err[%d]", iRet);
		Tms_ShowMsg("TMS UPDATE", tmp, 3000);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	total += iRet;
	if(total < tmsInfo.uFileSize) {
		// 未下载完成
		goto DOWNLOAD;
	}
	
	iRet = 0;
	
FINISH:
	Http_Release(&request);
	
	if(iRet == HTTP_ERR_DATA) {
		// 数据错误删除断点信息
		Tms_ClearUpdate();
		return iRet;
	} 
	
	if(total < tmsInfo.uFileSize) {
		// 未完成且下载次数未超过5次
		if(tmsInfo.count < 5) {
			goto RETRY;
		} else {
			Tms_ClearUpdate();
			Tms_ShowMsg("TMS UPDATE", "breakpoints exceeds litmits", 3000);
		}
	}
	
	return iRet;
}

// 0-no update info 1-update sucess 2-update interrupt
int Tms_GetUpdateStaus()
{
	int iRet;
	ST_TMSINFO tmsInfo;
	ST_OSTMSFLAG tmsFlag;

	LvoslitReadTmsShare(&tmsFlag);
	
	if(tmsFlag.tmsflag == 1) {
		return 1; 
	}
	
	if(File_IsExist(TMS_BREAKFILE) != RET_FILE_OK) {
		return 0; 
	}
	
	return 2;
}

//clear data
int Tms_ClearUpdate()
{
	ST_OSTMSFLAG tmsFlag;
	LvoslitReadTmsShare(&tmsFlag);
	Tms_ClearFlag(&tmsFlag);
	File_Remove(TMS_BREAKFILE);
	
	return 0;
}

// 0-无需更新 1-更新失败,次数超限，更新成功会重启
int Tms_Update(const char * appId, const char * ver, const char * code)
{
	int iRet = 0;
	char appVersion[50];

	sprintf(appVersion, "%s_%s", ver, code);

	if(File_IsExist(TMS_BREAKFILE) == RET_FILE_OK) {
		goto DOWNLOAD;
	}
	
	// 获取版本信息
	{
		int page = 0;
		ST_TMSINFO tmsInfo;
		memset((char *)&tmsInfo, 0, sizeof(ST_TMSINFO));
		iRet = Tms_CheckVersoin(appId, appVersion, &tmsInfo);
		if(iRet < 0) {
			char text[100];
			sprintf(text, "get ota info fail\nerror[%d]", iRet);
			Tms_ShowMsg("TMS UPDATE", text, 10000);
			return iRet;
		}
		
		//“0”-无需更新，”1”-强制更新，“2”-正常更新，其他-错误
		if(iRet == 0) {
			Tms_ShowMsg("TMS UPDATE", "no update task", 3000);
			return 0;
		}
		
		if(iRet != 1) { 
		    Ui_Clear();
			Ui_ClearKey();
			Ui_DispTitle("TMS UPDATE");
			Ui_DispTextLineAlign(1, DISPLAY_LEFT, "new version detected", 0);
			Ui_DispTextLineAlign(2, DISPLAY_LEFT, "to update now?", 0);
			Ui_DispTextLineAlign(3, DISPLAY_LEFT, "1.yes    0.no", 0);
			
			while(1)
			{
				switch(Ui_WaitKey(APP_UI_TIMEOUT))
				{
				case KB1:
					goto UPDATE;
					
				case KB0:				
				case KB_CANCEL:
				case KB_NONE:
					return 0;
				}
			}
		}
		
UPDATE:
		File_Write(TMS_BREAKFILE, 0, (char *)&tmsInfo, sizeof(ST_TMSINFO));
	}

	// 下载程序文件
DOWNLOAD:
	Ui_Clear();
	Ui_DispTitle("TMS UPDATE");
	Ui_ClearLine(1, Ui_GetLineNum()-2);
	Ui_DispTextLineAlign(2, DISPLAY_CENTER, "NO turn off", 0);
	iRet = Tms_Download();
	if(iRet < 0) {
		return iRet;
	}
	
	// 发起更新,增加次数限制避免更新死循环
	{
		ST_OSTMSFLAG tmsFlag;
		LvoslitReadTmsShare(&tmsFlag);
		if(tmsFlag.count < 0 || tmsFlag.count > 0) {
			Tms_ClearUpdate();
			Tms_ShowMsg("TMS UPDATE", "retries exceeded", 3000);
			return 1;
		}
		tmsFlag.count ++;
		LvoslitWriteTmsShare(&tmsFlag);
	}
	
	Ui_Clear();
	Ui_DispTitle("TMS UPDATE");
	return LvoslitUpdateTmsApp((char *)appId, appVersion);
}

int Tms_Notice(const char * taskId, int code, const char * info)
{
	int iRet;
	char url[256];
	char reqStr[256];
	char token[100];
	HTTPHANDLE request;

	Ui_Clear();
	Ui_DispTitle("TMS UPDATE INFORM");
	iRet = Http_InitHandle(&request, TMS_DOWNLOAD_SIZE);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		return iRet;
	}
	memset(reqStr, 0, sizeof(reqStr));
	Sys_ReadVendor(reqStr);
	Sys_ReadModel(reqStr+33);
	Sys_ReadPN(reqStr+66);
	Sys_ReadSN(reqStr+99);
	memset(url, 0, sizeof(url));
	sprintf(url, PATH_URL_UPLOAD_STATUS, taskId, reqStr, reqStr+33, reqStr+66, reqStr+99);
	iRet = Http_InitRequest(&request, "POST", TMS_HOST_NAME, TMS_PORT, url);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		iRet = HTTP_ERR_SIZE;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "close");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/json");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");
	// create requset body
	memset(reqStr, 0, sizeof(reqStr));
	sprintf(reqStr, OTA_REQ_NOTICE, code, info);
	// calc token
	memset(token, 0, sizeof(token));
	Tms_GenToken(url, reqStr, token);
	Http_SetHeader(&request, "Token", token);
	// set body
	Http_SetRequestBody(&request, reqStr, strlen(reqStr));
	
	TMS_HEXLOG("HTTP request:\r\n%s", request.pack, request.packLen);
	
	iRet = Http_TmsConnect(TMS_HOST_NAME, TMS_PORT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Connect = %d", iRet);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	request.socket = iRet;
	
	iRet = Http_TmsSend(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsSend = %d", iRet);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_TmsRecv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_TmsRecv = %d", iRet);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	
   	TMS_HEXLOG("HTTP response：\r\n", request.pack, request.packLen);

	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	memset(reqStr, 0,sizeof(reqStr));
	iRet = Http_GetResponseBody(&request, reqStr, sizeof(reqStr));
	if(iRet < 0) {
	    TMS_STRLOG("Http_GetResponseBody = %d", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	TMS_STRLOG("response：\r\n%s", reqStr);
		
	iRet = 0;
	
FINISH:
	Http_Release(&request);
	
	return iRet;
	
}

int Tms_CheckTms(const char * appId, const char * ver, const char * code)
{
	int iRet;

	iRet = Tms_GetUpdateStaus();
	if(iRet == 1) {
		ST_TMSINFO tmsInfo;
		memset((char *)&tmsInfo, 0, sizeof(ST_TMSINFO));
		File_Read(TMS_BREAKFILE, 0, (char *)&tmsInfo, sizeof(ST_TMSINFO));
		Tms_Notice(tmsInfo.taskId, 0, "Update success!");
		Tms_ClearUpdate();
		Sys_BeepOK();
		Tms_ShowMsg("TMS UPDATE", "update success", 3000);
		return 1;
	}
	if(iRet == 2) {
		Sys_BeepFAIL();
		Tms_Update(appId, ver, code);
		return 1;
	}
	return 0;
}

